package com.manazel;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.manazel.Constant.AES_Helper;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.User;
import com.manazel.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

import static com.manazel.Registration.fileUri;

/**
 * Created by c119 on 15/12/16.
 */

public class EditProfile extends FragmentActivity implements View.OnClickListener, WsResponseListener {
    ImageView imgprofile, imgmenu, imgback, imgplus, imgedit, img_upload, img_uploadResult;
    TextView txtheader;
    //CircleImageView imgprofile;
    EditText edtfname, edtlname, edtaddress, edtmail, edtcontact;
    Button btnsave;

    public final int RESULT_LOAD_IMAGE = 395;
    public final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 456;
    private final int REQUEST_WRITE_STORAGE = 963;

    public static final String TAG = EditProfile.class.getName();
    String encodedImage;
    Bitmap bitmapPhoto = null;

    String whichpic;
    TinyDB tb;
    User userobj;
    String whichaction;
    String encodedcontrctImage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);

        tb = new TinyDB(this);
        bitmapPhoto = null;
        tb.putString(AppConstant.PREF_EDITPROFILE_IMAGE, "");

        Gson gson = new Gson();
        String json = tb.getString("userobj");
        userobj = gson.fromJson(json, User.class);

        imgmenu = (ImageView) findViewById(R.id.img_menu);
        imgmenu.setVisibility(View.GONE);

        imgback = (ImageView) findViewById(R.id.img_back);
        imgback.setOnClickListener(this);
        imgback.setVisibility(View.VISIBLE);

        imgplus = (ImageView) findViewById(R.id.img_plus);
        imgplus.setVisibility(View.GONE);

        imgedit = (ImageView) findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView) findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(this));
        txtheader.setText(R.string.edit_profile);

        imgprofile = (ImageView) findViewById(R.id.img_profile);
        imgprofile.setOnClickListener(this);

        edtfname = (EditText) findViewById(R.id.edt_fname);
        edtlname = (EditText) findViewById(R.id.edt_lname);

        edtaddress = (EditText) findViewById(R.id.edt_address);
        edtmail = (EditText) findViewById(R.id.edt_email);
        edtcontact = (EditText) findViewById(R.id.edt_contact);

        btnsave = (Button) findViewById(R.id.btn_save);
        btnsave.setOnClickListener(this);

        img_uploadResult = (ImageView) findViewById(R.id.img_uploadResult);
        img_uploadResult.setVisibility(View.GONE);

        img_upload = (ImageView) findViewById(R.id.img_upload);
        img_upload.setOnClickListener(this);

        if (userobj != null) {

            Picasso.with(this)
                    .load(userobj.getProfile_pic())
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.placeholder_profile)
                    .error(R.drawable.placeholder_profile).into(imgprofile);

            edtfname.setText(userobj.getFname());
            edtlname.setText(userobj.getLname());
            edtaddress.setText(userobj.getAddress().toString());
            edtmail.setText(userobj.getEmail().toString());
            edtcontact.setText(userobj.getContactno().toString());
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.img_profile:
                showImagedia("profile", "Choose Profile Picture");
                break;

            case R.id.btn_save:
                checkvalidation();
                break;
            case R.id.img_upload:
                showImagedia("contract", "Choose Contract Picture");
                break;
            default:
                break;
        }
    }

    private void showImagedia(final String action, String title) {
        try {
            LayoutInflater factory = LayoutInflater.from(this);
            final View DialogView = factory.inflate(R.layout.take_profile_pic, null);
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().getAttributes().windowAnimations = R.style.ProfilepicDialogAnimation;
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(DialogView);

            TextView idTVTitle = (TextView) DialogView.findViewById(R.id.idTVTitle);
            idTVTitle.setText(title);

            TextView idTvTakePic = (TextView) DialogView.findViewById(R.id.idTvTakePic);
            idTvTakePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    whichaction = "camera";
                    whichpic = action;
                    dialog.dismiss();
                    if (isStoragePermissionGranted()) {
                        captureImage();
                    }
                }
            });

            TextView idTvChooseFromGallery = (TextView) DialogView.findViewById(R.id.idTvChooseFromGallery);
            idTvChooseFromGallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    whichaction = "gallery";
                    whichpic = action;
                    dialog.dismiss();
                    if (isStoragePermissionGranted()) {
                        PickImage();
                    }
                }
            });

            Button idBtnCancel = (Button) DialogView.findViewById(R.id.idBtnCancel);
            idBtnCancel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    whichaction = "cancel";
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(EditProfile.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(EditProfile.this, getApplicationContext().getPackageName() + ".provider", f));

        if (whichpic.equalsIgnoreCase("profile")) {
            tb.putString(AppConstant.PREF_EDITPROFILE_IMAGE, getRealPathFromURI(EditProfile.this, f.getAbsolutePath()));
            Log.e(TAG, "captureImage:SELEct PROFILE PIC::: " + tb.getString(AppConstant.PREF_EDITPROFILE_IMAGE));
        } else {
            tb.putString(AppConstant.PREF_EDIT_PROFILE_CONTRACT_IMAGE, getRealPathFromURI(EditProfile.this, f.getAbsolutePath()));
        }

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private static String getRealPathFromURI(Activity activity, String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = activity.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    private void PickImage() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    public Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    private static File getOutputMediaFile() {
        File mediaFile = null;
        // External sdcard location
        try {
            File mediaStorageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d(TAG, "Oops! Failed create "
                            + AppConstant.IMAGE_DIRECTORY_NAME);
                    return null;
                }
            }
            // Create a media file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());

            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
            return mediaFile;

        } catch (Exception ex) {

            ex.printStackTrace();
            return mediaFile;
        }
    }

    private void checkvalidation() {
        Pattern pattern = Pattern.compile("^[a-zA-Z]+$");
        if (edtfname.getText().toString().length() > 0 && edtfname.getText().toString() != ""
                && edtfname.getText().toString() != " ") {
            Matcher fmatcher = pattern.matcher(edtfname.getText().toString());
            if (fmatcher.matches()) {
                if (edtlname.getText().toString().length() > 0 && edtlname.getText().toString() != ""
                        && edtlname.getText().toString() != " ") {
                    Matcher lmatcher = pattern.matcher(edtlname.getText().toString());
                    if (lmatcher.matches()) {
                        if (edtaddress.getText().toString().length() > 0 && edtaddress.getText().toString() != ""
                                && edtaddress.getText().toString() != " ") {
                            if (edtmail.getText().toString().length() > 0 && edtmail.getText().toString() != ""
                                    && edtmail.getText().toString() != " ") {
                                if (AppGlobal.checkEmail(edtmail.getText().toString())) {
                                    if (edtcontact.getText().toString().length() > 0 && edtcontact.getText().toString() != ""
                                            && edtcontact.getText().toString() != " ") {
                                        calleditprofilews();
                                    } else {
                                        AppGlobal.showToast(this, getString(R.string.plz_enter_contact));
                                    }
                                } else {
                                    AppGlobal.showToast(this, getString(R.string.str_enter_valid_email));
                                }

                            } else {
                                AppGlobal.showToast(this, getString(R.string.str_enter_email));
                            }
                        } else {
                            AppGlobal.showToast(this, getString(R.string.plz_enter_address));
                        }
                    } else {
                        AppGlobal.showToast(this, getString(R.string.plz_enter_alphabets));
                    }
                } else {
                    AppGlobal.showToast(this, getString(R.string.plz_enter_lname));
                }
            } else {
                AppGlobal.showToast(this, getString(R.string.plz_enter_alphabets));
            }
        } else {
            AppGlobal.showToast(this, getString(R.string.plz_enter_fname));
        }
    }

    private void calleditprofilews() {

        if (AppGlobal.isNetwork(this)) {
            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(this, AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setId(userobj.getId());
            cm.setFname(edtfname.getText().toString());
            cm.setLname(edtlname.getText().toString());
            cm.setEmail(edtmail.getText().toString());
            cm.setAddress(edtaddress.getText().toString());
            cm.setContactno(edtcontact.getText().toString());
            cm.setContract(encodedcontrctImage);
            cm.setProfile_pic(encodedImage);
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            try {
                new AsyncPostService(EditProfile.this, getString(R.string.Please_wait), WsConstant.Req_ManageProfile, cm, true, true)
                        .execute(WsConstant.WS_MANAGEPROFILE);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            AppGlobal.showToast(this, getString(R.string.str_no_internet));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                if (resultCode == Activity.RESULT_OK) {


//                        setBitMap(tb.getString(AppConstant.PREF_PROFILE_IMAGE));
                    File f = new File(Environment.getExternalStorageDirectory().toString());
                    for (File temp : f.listFiles()) {
                        if (temp.getName().equals("temp.jpg")) {
                            f = temp;
                            break;
                        }
                    }
                    try {
                        Bitmap bitmap;
                        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

                        bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                                bitmapOptions);


                        String path = android.os.Environment
                                .getExternalStorageDirectory()
                                + File.separator
                                + File.separator;
                        f.delete();
                        OutputStream outFile = null;
                        File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                        try {
                            outFile = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, outFile);
                            if (whichpic.equals("profile")) {
                                imgprofile.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 120, 120, false));
                                encodedImage = bitmapToString(bitmap);
                            } else {
                                tb.putString(AppConstant.PREF_EDIT_PROFILE_CONTRACT_IMAGE, getRealPathFromURI(EditProfile.this, file.getAbsolutePath()));

                                setContractData(getRealPathFromURI(EditProfile.this, file.getAbsolutePath()));
                            }

                            outFile.flush();
                            outFile.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // user cancelled Image capture
                    AppGlobal.showToast(this, "Image capture cancelled.");

                } else {
                    // failed to capture image
                    AppGlobal.showToast(this, "Sorry! Failed to capture image");
                }

            }

            if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != data) {
                Uri selectedImage = data.getData();

                if (selectedImage.toString().startsWith("file:///")) {
                    if (whichpic.equals("profile")) {
                        tb.putString(AppConstant.PREF_EDITPROFILE_IMAGE, selectedImage.getPath());
                        setBitMap(selectedImage.getPath());

                    } else {
                        setContractData(selectedImage.getPath());
                    }

                } else {
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getApplicationContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);

                    if (cursor != null) {

                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String picturePath = cursor.getString(columnIndex);
                        cursor.close();

                        if (whichpic.equals("profile")) {
                            tb.putString(AppConstant.PREF_EDITPROFILE_IMAGE, picturePath);

                        } else {
                            tb.putString(AppConstant.PREF_EDIT_PROFILE_CONTRACT_IMAGE, picturePath);
                        }
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
                        if (whichpic.equals("profile")) {
                            imgprofile.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 300, 300, false));
                            setBitMap(picturePath);
                        } else {
                            setContractData(picturePath);
                        }
                    } else {

                        super.onActivityResult(requestCode, resultCode, data);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setContractData(String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmapcontrctPhoto = BitmapFactory.decodeFile(getRealPathFromURI(EditProfile.this, path), options);
//                        edt_ContractImg.setText(picturePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmapcontrctPhoto.compress(Bitmap.CompressFormat.JPEG, 85, baos);
        img_uploadResult.setImageBitmap(Bitmap.createScaledBitmap(bitmapcontrctPhoto, 300, 300, false));
        img_uploadResult.setVisibility(View.VISIBLE);
        byte[] imageBytes = baos.toByteArray();
        encodedcontrctImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    public void setBitMap(String picturePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        //options.inSampleSize = 12;

        bitmapPhoto = BitmapFactory.decodeFile(picturePath, options);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmapPhoto.compress(Bitmap.CompressFormat.JPEG, 85, baos);
        byte[] imageBytes = baos.toByteArray();
        encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        imgprofile.setImageBitmap(bitmapPhoto);
        Picasso.with(EditProfile.this)
                .load(new File(picturePath))
                .transform(new CircleTransform())
                .into(imgprofile);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            if (whichaction.equalsIgnoreCase("camera")) {
                captureImage();
            } else if (whichaction.equalsIgnoreCase("gallery")) {
                PickImage();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error) {

        if (error == null) {

            if (serviceType.equalsIgnoreCase(WsConstant.Req_ManageProfile)) {
                String status = ((ResponseResult) data).getStatus();

                if (status.equalsIgnoreCase("SUCCESS")) {

                    Gson gson = new Gson();
                    String json = gson.toJson(((ResponseResult) data).getUser_info().get(0));
                    tb.putString("userobj", json);
                    Toast.makeText(this, "Profile updated", Toast.LENGTH_SHORT).show();
                    finish();

                    EventBus.getDefault().post("update_profile");

                } else {

                }
            }
        }
    }

    public String bitmapToString(Bitmap bitmap) {
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] image = stream.toByteArray();
            return Base64.encodeToString(image, Base64.DEFAULT);
        } catch (Exception e) {
            Log.e(TAG, "getBitmapString : ", e);
            return "";
        }
    }
}
