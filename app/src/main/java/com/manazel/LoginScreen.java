package com.manazel;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.User;

/**
 * Created by c119 on 23/11/16.
 */

public class LoginScreen extends FragmentActivity implements View.OnClickListener, WsResponseListener {
    ImageView imgmenu, imgback, imgplus, imgedit;
    TextView txtheader, txtforgotpass, txtnotmember;

    EditText edtemail, edtpass;
    Button btnlogin, btnregister;

    TinyDB tb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tb = new TinyDB(this);

        imgmenu = (ImageView) findViewById(R.id.img_menu);
        imgmenu.setVisibility(View.GONE);

        imgback = (ImageView) findViewById(R.id.img_back);
        imgback.setVisibility(View.GONE);

        imgplus = (ImageView) findViewById(R.id.img_plus);
        imgplus.setVisibility(View.GONE);

        imgedit = (ImageView) findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView) findViewById(R.id.txt_header);
        txtheader.setText(R.string.str_login);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(this));

        edtemail = (EditText) findViewById(R.id.edt_email);
        edtemail.setTypeface(AppGlobal.getHelveticaNuneLight(this));
        edtpass = (EditText) findViewById(R.id.edt_password);
        edtpass.setTypeface(AppGlobal.getHelveticaNuneLight(this));

        txtforgotpass = (TextView) findViewById(R.id.txt_forgotpass);
        txtforgotpass.setTypeface(AppGlobal.getLatoRegular(this));
        txtforgotpass.setOnClickListener(this);

        btnlogin = (Button) findViewById(R.id.btn_login);
        btnlogin.setTypeface(AppGlobal.getLatoBold(this));
        btnlogin.setOnClickListener(this);

        txtnotmember = (TextView) findViewById(R.id.txt_notmember);
        txtnotmember.setTypeface(AppGlobal.getHelveticaNuneRegular(this));

        btnregister = (Button) findViewById(R.id.btn_register_here);
        btnregister.setTypeface(AppGlobal.getHelveticaNuneMedium(this));
        btnregister.setOnClickListener(this);

        callrefteshtokenws();
    }

    private void callrefteshtokenws() {
        if (AppGlobal.isNetwork(this)) {
            Common cm = new Common();
            cm.setAccess_key("nousername");

            try {

                new AsyncPostService(this, getString(R.string.Please_wait), WsConstant.Req_refreshToken, cm, false, true).execute(WsConstant.WS_REFRESHTOKEN);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                checkvalidation();
                break;

            case R.id.txt_forgotpass:
                showforgotpassdia();
                break;

            case R.id.btn_register_here:
                callregister();
                break;
        }
    }

    private void checkvalidation() {
        if (edtemail.getText().toString().length() > 0 && edtemail.getText().toString() != ""
                && edtemail.getText().toString() != " ") {
            if (AppGlobal.checkEmail(edtemail.getText().toString())) {
                if (edtpass.getText().toString().length() > 0 && edtpass.getText().toString() != ""
                        && edtpass.getText().toString() != " ") {
                    callloginws();
                } else {
                    AppGlobal.showToast(this, getString(R.string.str_enter_password));
                }
            } else {

                AppGlobal.showToast(this, getString(R.string.str_enter_valid_email));
            }
        } else {

            AppGlobal.showToast(this, getString(R.string.str_enter_email));
        }
    }

    private void callloginws() {
        if (AppGlobal.isNetwork(this)) {
            String temptoken = null;
            if (AppGlobal.getStringPreference(LoginScreen.this, AppConstant.APP_PREF_TEMPTOKEN) != "") {
                temptoken = AppGlobal.getStringPreference(LoginScreen.this, AppConstant.APP_PREF_TEMPTOKEN);
            }

            Common cm = new Common();
            cm.setEmail(edtemail.getText().toString());
            cm.setPassword(edtpass.getText().toString());
            cm.setSecret_key(temptoken);
            cm.setAccess_key(AppConstant.No_user_name);

            try {
                new AsyncPostService(this, getString(R.string.Please_wait), WsConstant.Req_Login, cm, true, true)
                        .execute(WsConstant.WS_LOGIN);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            AppGlobal.showToast(this, getString(R.string.str_no_internet));
        }
    }

    private void callregister() {
        Intent i = new Intent(LoginScreen.this, Registration.class);
        startActivity(i);
        finish();
    }

    private void showforgotpassdia() {
        try {
            LayoutInflater factory = LayoutInflater.from(this);
            final View DialogView = factory.inflate(R.layout.forgot_pass_dialog, null);
            final Dialog dialogForgotPass = new Dialog(this);
            dialogForgotPass.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogForgotPass.getWindow().getAttributes().windowAnimations = R.style.ProfilepicDialogAnimation;
            dialogForgotPass.setCancelable(false);
            dialogForgotPass.setCanceledOnTouchOutside(false);
            dialogForgotPass.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialogForgotPass.setContentView(DialogView);

            final EditText emailAddress = (EditText) dialogForgotPass.findViewById(R.id.idEtEmailAddress);

            Button btnOk = (Button) dialogForgotPass.findViewById(R.id.btnOk);
            btnOk.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (emailAddress.getText().toString().length() > 0 && emailAddress.getText().toString() != ""
                            && emailAddress.getText().toString() != " ") {
                        if (AppGlobal.checkEmail(emailAddress.getText().toString().trim())) {
                            dialogForgotPass.dismiss();
                            callForgotPasswordws(emailAddress.getText().toString());
                        } else {
                            AppGlobal.showToast(LoginScreen.this, getString(R.string.str_enter_valid_email));
                        }

                    } else {
                        AppGlobal.showToast(LoginScreen.this, getString(R.string.str_enter_email));
                    }
                }
            });

            Button btnCancel = (Button) dialogForgotPass.findViewById(R.id.btnCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialogForgotPass.dismiss();
                }
            });

            dialogForgotPass.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void callForgotPasswordws(String email) {
        if (AppGlobal.isNetwork(this)) {
            String temptoken = null;
            if (AppGlobal.getStringPreference(LoginScreen.this, AppConstant.APP_PREF_TEMPTOKEN) != "") {
                temptoken = AppGlobal.getStringPreference(LoginScreen.this, AppConstant.APP_PREF_TEMPTOKEN);
            }

            Common cm = new Common();
            cm.setEmail(email);
            cm.setSecret_key(temptoken);
            cm.setAccess_key(AppConstant.No_user_name);

            try {
                new AsyncPostService(this, getString(R.string.Please_wait), WsConstant.Req_ForgotPassword, cm, true, true)
                        .execute(WsConstant.WS_FORGOTPASSWORD);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            AppGlobal.showToast(this, getString(R.string.str_no_internet));
        }
    }


    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error) {
        if (error == null) {
            if (serviceType.equalsIgnoreCase(WsConstant.Req_refreshToken)) {

                String message = ((ResponseResult) data).getResult().getStatus();
                if (message.equalsIgnoreCase("DONE")) {
                    AppGlobal.setStringPreference(LoginScreen.this, ((ResponseResult) data).getResult().getTempToken(), AppConstant.APP_PREF_TEMPTOKEN);
                    AppGlobal.setStringPreference(LoginScreen.this, "_$(MiGrind)!_ManazelApp_11_19_2016", AppConstant.APP_PREF_GLOBAL_PASSWORD);
                }
            }

            if (serviceType.equalsIgnoreCase(WsConstant.Req_Login)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    if (((ResponseResult) data).getArrlogindata().size() > 0) {
                        tb.remove("userobj");

                        String usertoken = ((ResponseResult) data).getArrlogindata().get(0).getUserToken();
                        String guid = ((ResponseResult) data).getArrlogindata().get(0).getGuid();

                        AppGlobal.setSecreteKey(this, usertoken, guid);

                        User userobj = new User();
                        userobj.setUserToken(((ResponseResult) data).getArrlogindata().get(0).getUserToken());
                        userobj.setId(((ResponseResult) data).getArrlogindata().get(0).getId());
                        userobj.setFname(((ResponseResult) data).getArrlogindata().get(0).getFname());
                        userobj.setLname(((ResponseResult) data).getArrlogindata().get(0).getLname());
                        userobj.setRole_id(((ResponseResult) data).getArrlogindata().get(0).getRole_id());
                        userobj.setEmail(((ResponseResult) data).getArrlogindata().get(0).getEmail());
                        userobj.setPassword(((ResponseResult) data).getArrlogindata().get(0).getPassword());
                        userobj.setAddress(((ResponseResult) data).getArrlogindata().get(0).getAddress());
                        userobj.setContactno(((ResponseResult) data).getArrlogindata().get(0).getContactno());
                        userobj.setContract(((ResponseResult) data).getArrlogindata().get(0).getContract());
                        userobj.setProfile_pic(((ResponseResult) data).getArrlogindata().get(0).getProfile_pic());
                        userobj.setStatus(((ResponseResult) data).getArrlogindata().get(0).getStatus());
                        userobj.setIs_verified(((ResponseResult) data).getArrlogindata().get(0).getIs_verified());
                        userobj.setDevice_token(((ResponseResult) data).getArrlogindata().get(0).getDevice_token());
                        userobj.setDevice_make(((ResponseResult) data).getArrlogindata().get(0).getDevice_make());
                        userobj.setGuid(((ResponseResult) data).getArrlogindata().get(0).getGuid());
                        userobj.setContractImages(((ResponseResult) data).getArrlogindata().get(0).getContractImages());


                        Gson gson = new Gson();
                        String json = gson.toJson(userobj);
                        tb.putString("userobj", json);

                        tb.putBoolean(AppConstant.PREF_ISLOGIN, true);

                        Intent i = new Intent(LoginScreen.this, MainActivity.class);
                        i.putExtra("property_intent_data", false);
                        startActivity(i);
                        finish();
                    }
                } else {

                    Toast.makeText(this, ((ResponseResult) data).getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            if (serviceType.equalsIgnoreCase(WsConstant.Req_ForgotPassword)) {
                String message = ((ResponseResult) data).getMessage();
                AppGlobal.showToast(this, message);
            }
        }
    }
}
