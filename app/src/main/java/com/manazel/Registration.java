package com.manazel;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.User;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.manazel.R.id.email;
import static com.manazel.R.id.profile_image;

/**
 * Created by c119 on 24/11/16.
 */

public class Registration extends FragmentActivity implements View.OnClickListener, WsResponseListener {

    ImageView imgmenu, imgback, imgplus, imgedit;
    TextView txtheader;
    Button btnregister;
    ListView listregistration;
    RegistrationAdapter registeradp;
    List<String> items = new ArrayList<String>();
    String whichaction;
    public static final String TAG = Registration.class.getName();
    public static Uri fileUri;
    public String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static final int RESULT_LOAD_IMAGE = 14;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 15;
    private static final int REQUEST_WRITE_STORAGE = 112;

    String encodedImage;
    Bitmap bitmapPhoto = null;

    String encodedcontrctImage;
    Bitmap bitmapcontrctPhoto = null;

    TinyDB tb;
    String whichpic;


    // VB CHANGES
    private EditText edt_Fname, edt_Lname, edt_Email, edt_Password,
            edt_ConPassword, edt_Number, edt_Address, edt_ContractImg,
            edt_StartDate, edt_EndDate;
    private CircleImageView profile_image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        tb = new TinyDB(this);

        tb.putString(AppConstant.PREF_PROFILE_IMAGE, "");
        tb.putString(AppConstant.PREF_CONTRACT_IMAGE, "");

        imgmenu = (ImageView) findViewById(R.id.img_menu);
        imgmenu.setVisibility(View.GONE);

        imgback = (ImageView) findViewById(R.id.img_back);
        imgback.setOnClickListener(this);
        imgback.setVisibility(View.VISIBLE);

        imgplus = (ImageView) findViewById(R.id.img_plus);
        imgplus.setVisibility(View.GONE);

        imgedit = (ImageView) findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView) findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(this));
        txtheader.setText(R.string.str_register);

        listregistration = (ListView) findViewById(R.id.list_register);
        registeradp = new RegistrationAdapter(Registration.this);
        listregistration.setAdapter(registeradp);

        btnregister = (Button) findViewById(R.id.btn_register);
        btnregister.setTypeface(AppGlobal.getLatoBold(this));
        btnregister.setOnClickListener(this);


        //VB CHANGES
        setupControls();
        initGlobal();


    }

    private void setupControls() {
        edt_Fname = (EditText) findViewById(R.id.edt_Fname);
        edt_Lname = (EditText) findViewById(R.id.edt_Lname);
        edt_Email = (EditText) findViewById(R.id.edt_Email);
        edt_Password = (EditText) findViewById(R.id.edt_Password);
        edt_ConPassword = (EditText) findViewById(R.id.edt_ConPassword);
        edt_Number = (EditText) findViewById(R.id.edt_Number);
        edt_Address = (EditText) findViewById(R.id.edt_Address);
        edt_ContractImg = (EditText) findViewById(R.id.edt_ContractImg);
        edt_StartDate = (EditText) findViewById(R.id.edt_StartDate);
        edt_EndDate = (EditText) findViewById(R.id.edt_EndDate);
        profile_image = (CircleImageView) findViewById(R.id.profile_image);
    }

    private void initGlobal() {
        edt_Fname.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_user, 0, 0, 0);
        edt_Lname.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_user, 0, 0, 0);
        edt_Email.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_email, 0, 0, 0);
        edt_Password.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_pass, 0, 0, 0);
        edt_ConPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_pass, 0, 0, 0);
        edt_Number.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_contact, 0, 0, 0);
        edt_Address.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_address, 0, 0, 0);
        edt_ContractImg.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_contract, 0, 0, 0);
        edt_StartDate.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_contact, 0, 0, 0);
        edt_EndDate.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_contact, 0, 0, 0);


        profile_image.setOnClickListener(this);
        edt_ContractImg.setOnClickListener(this);
        edt_StartDate.setOnClickListener(this);
        edt_EndDate.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.btn_register:
                checkAllValues();
                break;

            case R.id.profile_image:
                showDialogToSelectPhoto("profile", "Select Profile Picture");
                break;

            case R.id.edt_ContractImg:
                showDialogToSelectPhoto("contract", "Select Contract Picture");
                break;

            case R.id.edt_StartDate:
                AppGlobal.setDateTimeField(Registration.this, edt_StartDate, "dd MMM yyyy");
                break;

            case R.id.edt_EndDate:
                AppGlobal.setDateTimeField(Registration.this, edt_EndDate, "dd MMM yyyy");
                break;
        }
    }

    public class RegistrationAdapter extends BaseAdapter {
        Context mContext;
        private LayoutInflater layoutInflater;

        public RegistrationAdapter(Context context) {
            mContext = context;

            items.clear();
            layoutInflater = LayoutInflater.from(context);
            for (int i = 0; i < 10; i++) {
                items.add("");
            }
        }

        @Override
        public int getCount() {
            return 11;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return 4;
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return 0;
            } else if (position == 1) {
                return 1;
            } else if (position == 2) {
                return 1;
            } else if (position == 3) {
                return 1;
            } else if (position == 4) {
                return 1;
            } else if (position == 5) {
                return 1;
            } else if (position == 6) {
                return 1;
            } else if (position == 7) {
                return 2;
            } else if (position == 8) {
                return 3;
            } else if (position == 9) {
                return 1;
            } else if (position == 10) {
                return 1;
            } else {
                return -1;
            }

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            int type = getItemViewType(position);

            if (convertView == null) {
                holder = new ViewHolder();

                if (type == 0) {

                    convertView = layoutInflater.inflate(R.layout.row_register_profile, null);
//                    holder.imgprofile = (CircleImageView) convertView.findViewById(profile_image);
                    holder.imgprofile.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            showDialogToSelectPhoto("profile", "Select Profile Picture");
                        }
                    });
                } else if (type == 1) {
                    convertView = layoutInflater.inflate(R.layout.row_register_simpleedt, null);
                    holder.edtcommon = (EditText) convertView.findViewById(R.id.edt_common);
                    holder.edtcommon.setTypeface(AppGlobal.getHelveticaNuneLight(mContext));


                } else if (type == 2) {
                    convertView = layoutInflater.inflate(R.layout.row_register_bigedt, null);
                    holder.edtaddress = (EditText) convertView.findViewById(R.id.edt_address);
                    holder.edtaddress.setTypeface(AppGlobal.getHelveticaNuneLight(mContext));

                } else if (type == 3) {
                    convertView = layoutInflater.inflate(R.layout.row_register_contractedt, null);
                    holder.edtcontract = (EditText) convertView.findViewById(R.id.edt_contract);
                    holder.edtcontract.setTypeface(AppGlobal.getHelveticaNuneLight(mContext));

                    holder.imgadd = (ImageView) convertView.findViewById(R.id.img_add);

                    holder.imgadd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showDialogToSelectPhoto("contract", "Select Contract Picture");
                        }
                    });
                }
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (type == 0) {
                if (bitmapPhoto != null) {
                    holder.imgprofile.setImageBitmap(bitmapPhoto);
                } else {
                    holder.imgprofile.setImageResource(R.drawable.placeholder_profile);
                }

            }


            if (type == 1) {
                if (position == 1) {
                    holder.edtcommon.setHint(R.string.first_name);
                    holder.edtcommon.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_user, 0, 0, 0);
                    holder.edtcommon.setInputType(InputType.TYPE_CLASS_TEXT);
                    holder.edtcommon.setFocusable(false);
                    holder.edtcommon.setFocusableInTouchMode(true);
                    holder.edtcommon.setClickable(false);
                    holder.edtcommon.setTag(1);
                    holder.edtcommon.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() > 0) {
                                items.set(0, s.toString());
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                } else if (position == 2) {
                    holder.edtcommon.setHint(R.string.last_name);
                    holder.edtcommon.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_user, 0, 0, 0);
                    holder.edtcommon.setInputType(InputType.TYPE_CLASS_TEXT);
                    holder.edtcommon.setFocusable(false);
                    holder.edtcommon.setFocusableInTouchMode(true);
                    holder.edtcommon.setTag(2);
                    holder.edtcommon.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() > 0) {
                                items.set(1, s.toString());
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                } else if (position == 3) {

                    holder.edtcommon.setHint(R.string.email_address);
                    holder.edtcommon.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_email, 0, 0, 0);

                    holder.edtcommon.setTag(3);
                    holder.edtcommon.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                    holder.edtcommon.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                    holder.edtcommon.setFocusable(false);
                    holder.edtcommon.setFocusableInTouchMode(true);
                    holder.edtcommon.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() > 0) {
                                items.set(2, s.toString());

                                tb.putString(AppConstant.PREF_REG_EMAIL_VALUE, s.toString());
                                Log.e(TAG, "onTextChanged: THIS IS EMAIL ::::" + tb.getString(AppConstant.PREF_REG_EMAIL_VALUE));
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });

                } else if (position == 4) {

                    holder.edtcommon.setHint(R.string.choose_password);
                    holder.edtcommon.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_pass, 0, 0, 0);
                    holder.edtcommon.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    holder.edtcommon.setTag(4);
                    holder.edtcommon.setFocusable(false);
                    holder.edtcommon.setFocusableInTouchMode(true);
                    holder.edtcommon.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() > 0) {
                                items.set(3, s.toString());
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                } else if (position == 5) {

                    holder.edtcommon.setHint(R.string.confirm_password);
                    holder.edtcommon.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_pass, 0, 0, 0);
                    holder.edtcommon.setFocusable(true);
                    holder.edtcommon.setTag(5);
                    holder.edtcommon.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    holder.edtcommon.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() > 0) {
                                items.set(4, s.toString());
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                } else if (position == 6) {
                    holder.edtcommon.setHint(R.string.contact_number);
                    holder.edtcommon.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_contact, 0, 0, 0);
                    holder.edtcommon.setInputType(InputType.TYPE_CLASS_NUMBER);
                    holder.edtcommon.setFocusable(false);
                    holder.edtcommon.setFocusableInTouchMode(true);
                    holder.edtcommon.setTag(6);
                    holder.edtcommon.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() > 0) {
                                items.set(5, s.toString());
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                } else if (position == 9) {

                    holder.edtcommon.setHint("Start Date");
                    holder.edtcommon.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_contact, 0, 0, 0);
                    holder.edtcommon.setClickable(true);
                    holder.edtcommon.setFocusable(false);
                    holder.edtcommon.setFocusableInTouchMode(false);
                    holder.edtcommon.setTag(9);
                    holder.edtcommon.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() > 0) {
                                items.set(8, s.toString());
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                    holder.edtcommon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AppGlobal.setDateTimeField(Registration.this, holder.edtcommon, "dd MMM yyyy");
                        }
                    });


                } else if (position == 10) {

                    holder.edtcommon.setHint("End Date");
                    holder.edtcommon.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_contact, 0, 0, 0);
                    holder.edtcommon.setClickable(true);
                    holder.edtcommon.setFocusable(false);
                    holder.edtcommon.setFocusableInTouchMode(false);
                    //   holder.edtcommon.setPadding(0, 0, 0, 10);
                    holder.edtcommon.setTag(10);
                    holder.edtcommon.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() > 0) {
                                items.set(9, s.toString());
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    holder.edtcommon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AppGlobal.setDateTimeField(Registration.this, holder.edtcommon, "dd MMM yyyy");
                        }
                    });

                }
            }

            if (type == 2) {
                if (position == 7) {
                    holder.edtaddress.setHint(R.string.address);
                    Drawable innerDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.hint_address);
                    GravityCompoundDrawable gravityDrawable = new GravityCompoundDrawable(innerDrawable);
                    // NOTE: next 2 lines are important!
                    innerDrawable.setBounds(0, 0, innerDrawable.getIntrinsicWidth(), innerDrawable.getIntrinsicHeight());
                    gravityDrawable.setBounds(0, 0, innerDrawable.getIntrinsicWidth(), innerDrawable.getIntrinsicHeight());
                    holder.edtaddress.setCompoundDrawablesWithIntrinsicBounds(gravityDrawable, null, null, null);
                    holder.edtaddress.setTag(7);
                    holder.edtaddress.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() > 0) {
                                items.set(6, s.toString());
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    holder.edtaddress.setFocusable(false);
                    holder.edtaddress.setFocusableInTouchMode(true);
                }

            }

            if (type == 3) {

                if (position == 8) {

                    holder.edtcontract.setHint(R.string.contract);
                    holder.edtcontract.setFocusable(false);
                    holder.edtcontract.setFocusableInTouchMode(false);
                    holder.edtcontract.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showDialogToSelectPhoto("contract", "Select Contract Picture");
                        }
                    });
                    holder.edtcontract.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hint_contract, 0, 0, 0);
                    holder.edtcontract.setTag(8);
                    holder.edtcontract.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() > 0) {
                                items.set(7, s.toString());
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    if (!(tb.getString(AppConstant.PREF_CONTRACT_IMAGE).equals(""))) {
                        holder.edtcontract.setText(tb.getString(AppConstant.PREF_CONTRACT_IMAGE));
                    }
                }

            }

            return convertView;
        }


        private class EditTextWatcher implements TextWatcher {
            private int target;

            public void setTarget(int target) {
                this.target = target;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

        }

        public class ViewHolder {
            EditText edtcommon, edtaddress, edtcontract;
            //            EditTextWatcher watcher;
            CircleImageView imgprofile;
            ImageView imgadd;
        }
    }

    private void showDialogToSelectPhoto(final String action, String title) {
        try {
            LayoutInflater factory = LayoutInflater.from(Registration.this);
            final View DialogView = factory.inflate(R.layout.take_profile_pic, null);
            final Dialog dialog = new Dialog(Registration.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().getAttributes().windowAnimations = R.style.ProfilepicDialogAnimation;
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(DialogView);

            TextView idTVTitle = (TextView) DialogView.findViewById(R.id.idTVTitle);
            idTVTitle.setText(title);

            TextView idTvTakePic = (TextView) DialogView.findViewById(R.id.idTvTakePic);
            idTvTakePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    whichaction = "camera";
                    whichpic = action;
                    dialog.dismiss();
                    if (isStoragePermissionGranted()) {
                        captureImage();
                    }
                }
            });

            TextView idTvChooseFromGallery = (TextView) DialogView.findViewById(R.id.idTvChooseFromGallery);
            idTvChooseFromGallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    whichaction = "gallery";
                    whichpic = action;
                    dialog.dismiss();
                    if (isStoragePermissionGranted()) {
                        PickImage();
                    }
                }
            });

            Button idBtnCancel = (Button) DialogView.findViewById(R.id.idBtnCancel);
            idBtnCancel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    whichaction = "cancel";
                    whichpic = action;
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class GravityCompoundDrawable extends Drawable {
        // inner Drawable
        private final Drawable mDrawable;

        public GravityCompoundDrawable(Drawable drawable) {
            mDrawable = drawable;
        }

        @Override
        public int getIntrinsicWidth() {
            return mDrawable.getIntrinsicWidth();
        }

        @Override
        public int getIntrinsicHeight() {
            return mDrawable.getIntrinsicHeight();
        }

        @Override
        public void draw(Canvas canvas) {
            int halfCanvas = canvas.getHeight() / 2;
            int halfDrawable = mDrawable.getIntrinsicHeight() / 2;

            // align to top
            canvas.save();
            canvas.translate(0, -halfCanvas + halfDrawable);
            mDrawable.draw(canvas);
            canvas.restore();
        }

        @Override
        public void setAlpha(int alpha) {

        }

        @Override
        public void setColorFilter(ColorFilter colorFilter) {

        }

        @Override
        public int getOpacity() {
            return PixelFormat.TRANSPARENT;
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(Registration.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    private void captureImage() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//        String path = getOutputMediaFileUri();
//        String filePath = getRealPathFromURI(Registration.this, path);
//        Log.e("fileUri", "fileUri:::::::::" + filePath);
//
//        if (whichpic.equalsIgnoreCase("profile")) {
//            tb.putString(AppConstant.PREF_PROFILE_IMAGE, filePath);
//        } else {
//            tb.putString(AppConstant.PREF_CONTRACT_IMAGE, filePath);
//        }
//
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//
//        // start the image capture Intent
//        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(Registration.this, getApplicationContext().getPackageName() + ".provider", f));
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        if (whichpic.equalsIgnoreCase("profile")) {
            tb.putString(AppConstant.PREF_PROFILE_IMAGE, getRealPathFromURI(Registration.this, f.getAbsolutePath()));
            Log.e(TAG, "captureImage:SELEct PROFILE PIC::: " + tb.getString(AppConstant.PREF_PROFILE_IMAGE));
        } else {
            tb.putString(AppConstant.PREF_CONTRACT_IMAGE, getRealPathFromURI(Registration.this, f.getAbsolutePath()));
        }
    }

    public String getOutputMediaFileUri() {
//        return FileProvider.getUriForFile(Registration.this, getApplicationContext().getPackageName() + ".provider", getOutputMediaFile());
        return getOutputMediaFile().getAbsolutePath();

    }


    private static File getOutputMediaFile() {
        File mediaFile = null;
        // External sdcard location
        try {
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Manazeal");

            // Create the storage directory if it does not exist

            if (!mediaStorageDir.exists()) {
                mediaStorageDir.mkdir();
                Log.d(TAG, "Oops! Failed create " + AppConstant.IMAGE_DIRECTORY_NAME);
            }

            // Create a media file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());

            mediaFile = new File(mediaStorageDir.getAbsolutePath() + File.separator + "IMG_" + timeStamp + ".jpg");
            if (!mediaFile.exists()) {
                mediaFile.createNewFile();
            }
            return mediaFile;

        } catch (Exception ex) {

            ex.printStackTrace();
            return mediaFile;
        }
    }

    private void PickImage() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    private static String getRealPathFromURI(Activity activity, String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = activity.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                if (resultCode == Activity.RESULT_OK) {
                    if (whichpic.equals("profile")) {
//                        setBitMap(tb.getString(AppConstant.PREF_PROFILE_IMAGE));
                        File f = new File(Environment.getExternalStorageDirectory().toString());
                        for (File temp : f.listFiles()) {
                            if (temp.getName().equals("temp.jpg")) {
                                f = temp;
                                break;
                            }
                        }
                        try {
                            Bitmap bitmap;
                            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

                            bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                                    bitmapOptions);


                            String path = android.os.Environment
                                    .getExternalStorageDirectory()
                                    + File.separator
                                    + File.separator;
                            f.delete();
                            OutputStream outFile = null;
                            File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                            try {
                                outFile = new FileOutputStream(file);
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, outFile);
                                profile_image.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 300, 300, false));
                                encodedImage = bitmapToString(bitmap);
                                outFile.flush();
                                outFile.close();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        setBitMap(tb.getString(AppConstant.PREF_CONTRACT_IMAGE));
                    }

                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // user cancelled Image capture
                    AppGlobal.showToast(this, "Image capture cancelled.");
                } else {
                    // failed to capture image
                    AppGlobal.showToast(this, "Sorry! Failed to capture image");
                }

            }

            if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK) {
                Uri selectedImage = data.getData();
                if (selectedImage.toString().startsWith("file:///")) {
                    Log.e("selectedImage", "selectedImage=>" + selectedImage.getPath());
                    if (whichpic.equals("profile")) {
                        tb.putString(AppConstant.PREF_PROFILE_IMAGE, selectedImage.getPath());
                        profile_image.setImageURI(selectedImage);
                    } else {
                        tb.putString(AppConstant.PREF_CONTRACT_IMAGE, selectedImage.getPath());
                        setBitMap(selectedImage.getPath());
                    }

                } else {

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getApplicationContext().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String picturePath = cursor.getString(columnIndex);
                        cursor.close();

                        if (whichpic.equals("profile")) {
                            tb.putString(AppConstant.PREF_PROFILE_IMAGE, picturePath);
                        } else {
                            tb.putString(AppConstant.PREF_CONTRACT_IMAGE, picturePath);
                        }

                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
                        if (whichpic.equals("profile")) {
                            profile_image.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 300, 300, false));
                        } else {
                            setBitMap(picturePath);
                        }
                    } else {
                        super.onActivityResult(requestCode, resultCode, data);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setBitMap(String picturePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        //options.inSampleSize = 12;


//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
//        options.inJustDecodeBounds = true;
//        options.inSampleSize = 2;


        if (whichpic.equalsIgnoreCase("profile")) {
//            Log.e(TAG, "setBitMap: PIC PATH::::" + picturePath);
//            File imgFile = new File(picturePath);
//            if (imgFile.exists()) {
//
//                Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);
//                Log.e(TAG, "img path BEFORE COMPRESS ::::: " + bitmap.toString());
//                OutputStream stream = null;
//                try {
//                    stream = new FileOutputStream(imgFile);
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
//                Log.e(TAG, "img path AFTER COMPRESS ::::: " + bitmap.toString());
//                profile_image.setImageBitmap(bitmap);
//                encodedImage = bitmapToString(bitmap);
//            }

        } else {

            bitmapcontrctPhoto = BitmapFactory.decodeFile(picturePath, options);
            edt_ContractImg.setText(picturePath);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmapcontrctPhoto.compress(Bitmap.CompressFormat.JPEG, 85, baos);
            byte[] imageBytes = baos.toByteArray();
            encodedcontrctImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        }

        registeradp.notifyDataSetChanged();
    }

    public String bitmapToString(Bitmap bitmap) {
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] image = stream.toByteArray();
            return Base64.encodeToString(image, Base64.DEFAULT);
        } catch (Exception e) {
            Log.e(TAG, "getBitmapString : ", e);
            return "";
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            if (whichaction.equalsIgnoreCase("camera")) {
                captureImage();
            } else if (whichaction.equalsIgnoreCase("gallery")) {
                PickImage();
            }

        }
    }


    public void checkAllValues() {
        if (edt_Fname.getText().toString().trim().equals("")) {
            edt_Fname.requestFocus();
            edt_Fname.setError("Enter first name");
        } else if (edt_Lname.getText().toString().trim().equals("")) {
            edt_Lname.requestFocus();
            edt_Lname.setError("Enter last name");
        } else if (edt_Email.getText().toString().trim().equals("")) {
            edt_Email.requestFocus();
            edt_Email.setError("Enter email");
        } else if (edt_Password.getText().toString().trim().equals("")) {
            edt_Password.requestFocus();
            edt_Password.setError("Enter password");
        } else if (edt_ConPassword.getText().toString().trim().equals("")) {
            edt_ConPassword.requestFocus();
            edt_ConPassword.setError("Enter confirm password");
        } else if (edt_Number.getText().toString().trim().equals("")) {
            edt_Number.requestFocus();
            edt_Number.setError("Enter number");
        } else if (edt_Address.getText().toString().trim().equals("")) {
            edt_Address.requestFocus();
            edt_Address.setError("Enter address");
        } else if (edt_ContractImg.getText().toString().trim().equals("")) {
            edt_ContractImg.setError("Select contract image");
        } else if (edt_StartDate.getText().toString().trim().equals("")) {
            edt_StartDate.setError("Enter start date");
        } else if (edt_EndDate.getText().toString().trim().equals("")) {
            edt_EndDate.setError("Enter end date");
        } else if (!(edt_Email.getText().toString().trim().matches(emailPattern))) {
            edt_Email.setError("Enter valid email");
        } else if (!(edt_Password.getText().toString().trim().equals(edt_ConPassword.getText().toString().trim()))) {
            edt_ConPassword.setError("Confirm password not match!");
        } else {
            callregistrationws();
        }
    }


    private void callvalidation() {
        Log.e(TAG, "callvalidation: EMAIL VALUE::::::" + items.get(2));
        Pattern pattern = Pattern.compile("^[a-zA-Z]+$");
        //for firstname
        if (items.get(0).length() > 0) {
            Matcher fmatcher = pattern.matcher(items.get(0));
            if (fmatcher.matches()) {
                //for lastname
                if (items.get(1).length() > 0) {
                    Matcher lmatcher = pattern.matcher(items.get(1));
                    if (lmatcher.matches()) {
                        //for email
                        if (tb.getString(AppConstant.PREF_REG_EMAIL_VALUE).length() > 0) {
                            if (tb.getString(AppConstant.PREF_REG_EMAIL_VALUE).matches(emailPattern)) {
                                //for email
                                if (items.get(3).length() > 0) {
                                    //for conform password
                                    if (items.get(4).length() > 0) {
                                        if (items.get(3).equals(items.get(4))) {
                                            //for contact number
                                            if (items.get(5).length() > 0) {
                                                //for address
                                                if (items.get(6).length() > 0) {
                                                    if (encodedcontrctImage != null) {

                                                        if (items.get(9).trim().length() > 0) {

                                                            if (items.get(10).trim().length() > 0) {

                                                                callregistrationws();
                                                            } else {

                                                                AppGlobal.showToast(this, getString(R.string.plz_enter_enddate));
                                                            }
                                                        } else {

                                                            AppGlobal.showToast(this, getString(R.string.plz_enter_startdate));
                                                        }

                                                    } else {

                                                        callregistrationws();
                                                    }
                                                } else {
                                                    AppGlobal.showToast(this, getString(R.string.plz_enter_address));
                                                }
                                            } else {
                                                AppGlobal.showToast(this, getString(R.string.plz_enter_contact));
                                            }
                                        } else {
                                            AppGlobal.showToast(this, getString(R.string.cnf_pass_not_match));
                                        }
                                    } else {
                                        AppGlobal.showToast(this, getString(R.string.plz_enter_cnf_pass));
                                    }
                                } else {
                                    AppGlobal.showToast(this, getString(R.string.str_enter_password));
                                }
                            } else {
                                AppGlobal.showToast(this, getString(R.string.str_enter_valid_email));
                            }
                        } else {
                            AppGlobal.showToast(this, getString(R.string.str_enter_email));
                        }
                    } else {
                        AppGlobal.showToast(this, getString(R.string.plz_enter_alphabets) + " in last name");
                    }
                } else {
                    AppGlobal.showToast(this, getString(R.string.plz_enter_lname));
                }
            } else {
                AppGlobal.showToast(this, getString(R.string.plz_enter_alphabets) + " in first name");
            }
        } else {
            AppGlobal.showToast(this, getString(R.string.plz_enter_fname));
        }
    }

    private void callregistrationws() {
        if (AppGlobal.isNetwork(this)) {
            String temptoken = null;
            if (AppGlobal.getStringPreference(Registration.this, AppConstant.APP_PREF_TEMPTOKEN) != "") {
                temptoken = AppGlobal.getStringPreference(Registration.this, AppConstant.APP_PREF_TEMPTOKEN);
            }

            Common cm = new Common();
            cm.setFname(edt_Fname.getText().toString().trim());
            cm.setLname(edt_Lname.getText().toString().trim());
            cm.setRole_id("1");
            cm.setEmail(edt_Email.getText().toString().trim());
            cm.setPassword(edt_Password.getText().toString().trim());
            cm.setAddress(edt_Address.getText().toString().trim());
            cm.setProfile_pic(encodedImage);
            cm.setContactno(edt_Number.getText().toString().trim());
            cm.setContract(encodedcontrctImage);
            cm.setDevice_token("123456");
            cm.setDevice_make("dsdd");
            cm.setSecret_key(temptoken);
            cm.setAccess_key(AppConstant.No_user_name);

            try {
                new AsyncPostService(Registration.this, getString(R.string.Please_wait), WsConstant.Req_Registration, cm, true, true)
                        .execute(WsConstant.WS_REGISTRATION);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            AppGlobal.showToast(this, getString(R.string.str_no_internet));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        calllogin();
    }

    private void calllogin() {
        Intent i = new Intent(Registration.this, LoginScreen.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error) {
        if (error == null) {
            if (serviceType.equalsIgnoreCase(WsConstant.Req_Registration)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equals("SUCCESS")) {
                    AppGlobal.showToast(Registration.this, ((ResponseResult) data).getMessage());
                    if (((ResponseResult) data).getArrregisterdata().size() > 0) {
                        tb.remove("userobj");

                        String usertoken = ((ResponseResult) data).getArrregisterdata().get(0).getUserToken();
                        String guid = ((ResponseResult) data).getArrregisterdata().get(0).getGuid();

                        AppGlobal.setSecreteKey(this, usertoken, guid);

                        User userobj = new User();
                        userobj.setUserToken(((ResponseResult) data).getArrregisterdata().get(0).getUserToken());
                        userobj.setId(((ResponseResult) data).getArrregisterdata().get(0).getId());
                        userobj.setFname(((ResponseResult) data).getArrregisterdata().get(0).getFname());
                        userobj.setLname(((ResponseResult) data).getArrregisterdata().get(0).getLname());
                        userobj.setRole_id(((ResponseResult) data).getArrregisterdata().get(0).getRole_id());
                        userobj.setEmail(((ResponseResult) data).getArrregisterdata().get(0).getEmail());
                        userobj.setPassword(((ResponseResult) data).getArrregisterdata().get(0).getPassword());
                        userobj.setAddress(((ResponseResult) data).getArrregisterdata().get(0).getAddress());
                        userobj.setContactno(((ResponseResult) data).getArrregisterdata().get(0).getContactno());
                        userobj.setContract(((ResponseResult) data).getArrregisterdata().get(0).getContract());
                        userobj.setProfile_pic(((ResponseResult) data).getArrregisterdata().get(0).getProfile_pic());
                        userobj.setStatus(((ResponseResult) data).getArrregisterdata().get(0).getStatus());
                        userobj.setIs_verified(((ResponseResult) data).getArrregisterdata().get(0).getIs_verified());
                        userobj.setDevice_token(((ResponseResult) data).getArrregisterdata().get(0).getDevice_token());
                        userobj.setDevice_make(((ResponseResult) data).getArrregisterdata().get(0).getDevice_make());
                        userobj.setGuid(((ResponseResult) data).getArrregisterdata().get(0).getGuid());
                        userobj.setAccesscode(((ResponseResult) data).getArrregisterdata().get(0).getAccesscode());

                        Gson gson = new Gson();
                        String json = gson.toJson(userobj);
                        tb.putString("userobj", json);

                        //  tb.putBoolean(AppConstant.PREF_ISLOGIN, true);

                        //Intent i = new Intent(Registration.this, MainActivity.class);
                        Intent i = new Intent(Registration.this, LoginScreen.class);
                        startActivity(i);
                        setResult(0);
                        finish();
                    }

                } else {
                    AppGlobal.showToast(Registration.this, ((ResponseResult) data).getMessage());
                }
            }
        }
    }
}
