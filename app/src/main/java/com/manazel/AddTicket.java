package com.manazel;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.manazel.Constant.AES_Helper;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.TicketDeptData;
import com.manazel.modal.User;
import com.manazel.modal.departments;
import com.manazel.modal.ticketpriorities;
import com.manazel.modal.tickettypes;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.manazel.Registration.fileUri;

/**
 * Created by c119 on 25/11/16.
 */

public class AddTicket extends FragmentActivity implements View.OnClickListener,WsResponseListener
{
    ImageView imgmenu,imgback,imgplus,imgedit;
    TextView txtheader;

    Button btnsubmit;

    ListView listaddticket;
    AddticketAdapter addticketadp;

    RealmHelper realmhelp;
    List<String> items;

    List<TicketDeptData> arrticketdept;

    String finaldept,finaltickettype,finalticketprio;

    public static final int RESULT_LOAD_IMAGE = 195;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 162;
    private static final int REQUEST_WRITE_STORAGE = 180;

    TinyDB tb;
    User userobj;

    String descid = null;
    String tickettypeid = null;
    String tickerproiid = null;
    String whichaction;
    public static final String TAG = AddTicket.class.getName();

    String encodedImage;
    Bitmap bitmapPhoto = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addticket);

        tb = new TinyDB(this);

        bitmapPhoto = null;
        tb.putString(AppConstant.PREF_ADDTICKET_IMAGE,"");

        Gson gson = new Gson();
        String json = tb.getString("userobj");
        userobj = gson.fromJson(json,User.class);

        realmhelp = new RealmHelper();

        imgmenu = (ImageView)findViewById(R.id.img_menu);
        imgmenu.setVisibility(View.GONE);

        imgback = (ImageView)findViewById(R.id.img_back);
        imgback.setOnClickListener(this);
        imgback.setVisibility(View.VISIBLE);

        imgplus = (ImageView)findViewById(R.id.img_plus);
        imgplus.setOnClickListener(this);
        imgplus.setVisibility(View.GONE);

        imgedit = (ImageView)findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView)findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(this));
        txtheader.setText(R.string.str_add_ticket);

        btnsubmit = (Button)findViewById(R.id.btn_submit);
        btnsubmit.setTypeface(AppGlobal.getLatoBold(this));
        btnsubmit.setOnClickListener(this);

        arrticketdept =  realmhelp.getAllTicketDept();

        listaddticket = (ListView)findViewById(R.id.list_addticket);
        addticketadp = new AddticketAdapter(AddTicket.this,arrticketdept.get(0).getArrdepartment(),
                arrticketdept.get(0).getArrtickettypes(),arrticketdept.get(0).getArrticketpriorities());
        listaddticket.setAdapter(addticketadp);
    }

    public class AddticketAdapter extends BaseAdapter implements AdapterView.OnItemSelectedListener
    {
        Context mContext;

        List<departments> arrdepartment;
        List<tickettypes> arrtickettype;
        List<ticketpriorities> arrticketpriority;

        List<String> tempdepartment,temptickettype,tempticketpriority;

        private LayoutInflater layoutInflater;
        int count;

        public AddticketAdapter(Context context,List<departments> arrdepartment,List<tickettypes> arrtickettype,List<ticketpriorities> arrticketpriority)
        {
            mContext = context;
            layoutInflater = LayoutInflater.from(context);

            count = 0;

            finaldept = "Maintenance";
            finaltickettype = "Incident";
            finalticketprio = "High";

            this.arrdepartment = arrdepartment;
            this.arrtickettype = arrtickettype;
            this.arrticketpriority = arrticketpriority;

            items = new ArrayList<>();
            for (int i = 0; i < 5; i++)
            {
                items.add("");
            }

            tempdepartment = new ArrayList<>();
            for(int i=0 ;i < arrdepartment.size() ; i++)
            {
                tempdepartment.add(arrdepartment.get(i).getDepartmentName());
            }

            temptickettype = new ArrayList<>();
            for(int i=0 ;i < arrtickettype.size() ; i++)
            {
                temptickettype.add(arrtickettype.get(i).getTickettypeName());
            }

            tempticketpriority = new ArrayList<>();
            for(int i=0 ;i < arrticketpriority.size() ; i++)
            {
                tempticketpriority.add(arrticketpriority.get(i).getTicketPriority());
            }

        }

        @Override
        public int getCount() {
            return 6;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public int getViewTypeCount()
        {
            return 4;
        }

        @Override
        public int getItemViewType(int position)
        {
            if (position == 0)
            {
                return 0;
            }
            else if (position == 1)
            {
                return 1;
            }
            else if (position == 2)
            {
                return 1;
            }
            else if (position == 3)
            {
                return 1;
            }
            else if (position == 4)
            {
                return 2;
            }
            else if (position == 5)
            {
                return 3;
            }
            else
            {
                return -1;
            }

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder;

            int type = getItemViewType(position);

            if (convertView == null)
            {
                holder = new ViewHolder();

                if(type == 0)
                {
                    convertView = layoutInflater.inflate(R.layout.row_addticket_edt,null);
                    holder.edttitle = (EditText) convertView.findViewById(R.id.edt_title);
                    holder.edttitle.setTypeface(AppGlobal.getLatoBold(mContext));

                    holder.watcher = new EditTextWatcher();
                    holder.edttitle.addTextChangedListener(holder.watcher);
                }
                else if(type == 1)
                {
                    convertView = layoutInflater.inflate(R.layout.row_addticket_spin,null);
                    holder.spinner = (Spinner) convertView.findViewById(R.id.spi_department);
                    holder.spinner.setOnItemSelectedListener(this);
                }
                else if(type == 2)
                {
                    convertView = layoutInflater.inflate(R.layout.row_addticket_bigedt,null);
                    holder.edtwritedesc = (EditText) convertView.findViewById(R.id.edt_writedesc);
                    holder.edtwritedesc.setLines(5);
                    holder.edtwritedesc.setTypeface(AppGlobal.getLatoRegular(mContext));

                    holder.watcher = new EditTextWatcher();
                    holder.edtwritedesc.addTextChangedListener(holder.watcher);
                }
                else if(type == 3)
                {
                    convertView = layoutInflater.inflate(R.layout.row_addticket_upload,null);
                    holder.imgupload = (ImageView) convertView.findViewById(R.id.img_upload);

                    holder.reladisplay = (RelativeLayout)convertView.findViewById(R.id.rela_display);
                    holder.imgdisplay = (ImageView)convertView.findViewById(R.id.img_display);
                    holder.imgedit = (ImageView) convertView.findViewById(R.id.img_edit);
                    holder.imgdelete = (ImageView) convertView.findViewById(R.id.img_delete);

                    holder.imgupload.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            try
                            {
                                LayoutInflater factory = LayoutInflater.from(mContext);
                                final View DialogView = factory.inflate(R.layout.take_profile_pic,null);
                                final Dialog dialog = new Dialog(mContext);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.getWindow().getAttributes().windowAnimations = R.style.ProfilepicDialogAnimation;
                                dialog.setCancelable(false);
                                dialog.setCanceledOnTouchOutside(false);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                dialog.setContentView(DialogView);

                                TextView idTVTitle = (TextView) DialogView.findViewById(R.id.idTVTitle);
                                idTVTitle.setText("Add Ticket Picture");

                                TextView idTvTakePic = (TextView) DialogView.findViewById(R.id.idTvTakePic);
                                idTvTakePic.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        whichaction = "camera";
                                        dialog.dismiss();
                                        if (isStoragePermissionGranted())
                                        {
                                            captureImage();
                                        }
                                    }
                                });

                                TextView idTvChooseFromGallery = (TextView) DialogView.findViewById(R.id.idTvChooseFromGallery);
                                idTvChooseFromGallery.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        whichaction = "gallery";
                                        dialog.dismiss();
                                        if (isStoragePermissionGranted())
                                        {
                                            PickImage();
                                        }
                                    }
                                });

                                Button idBtnCancel = (Button) DialogView.findViewById(R.id.idBtnCancel);
                                idBtnCancel.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {

                                        whichaction = "cancel";
                                        dialog.dismiss();
                                    }
                                });

                                dialog.show();
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                    });

                    holder.imgedit.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            try
                            {
                                LayoutInflater factory = LayoutInflater.from(mContext);
                                final View DialogView = factory.inflate(R.layout.take_profile_pic,null);
                                final Dialog dialog = new Dialog(mContext);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.getWindow().getAttributes().windowAnimations = R.style.ProfilepicDialogAnimation;
                                dialog.setCancelable(false);
                                dialog.setCanceledOnTouchOutside(false);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                dialog.setContentView(DialogView);

                                TextView idTVTitle = (TextView) DialogView.findViewById(R.id.idTVTitle);
                                idTVTitle.setText("Edit Ticket Picture");

                                TextView idTvTakePic = (TextView) DialogView.findViewById(R.id.idTvTakePic);
                                idTvTakePic.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        whichaction = "camera";
                                        dialog.dismiss();
                                        if (isStoragePermissionGranted())
                                        {
                                            captureImage();
                                        }
                                    }
                                });

                                TextView idTvChooseFromGallery = (TextView) DialogView.findViewById(R.id.idTvChooseFromGallery);
                                idTvChooseFromGallery.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        whichaction = "gallery";
                                        dialog.dismiss();
                                        if (isStoragePermissionGranted())
                                        {
                                            PickImage();
                                        }
                                    }
                                });

                                Button idBtnCancel = (Button) DialogView.findViewById(R.id.idBtnCancel);
                                idBtnCancel.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {

                                        whichaction = "cancel";
                                        dialog.dismiss();
                                    }
                                });

                                dialog.show();
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                    });

                    holder.imgdelete.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            bitmapPhoto = null;
                            tb.putString(AppConstant.PREF_ADDTICKET_IMAGE,"");
                            notifyDataSetChanged();
                        }
                    });

                }
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            if(type == 0)
            {
                if(position == 0)
                {
                    holder.watcher.setTarget(position);
                }
            }

            if(type == 1)
            {
                if(position == 1)
                {
                    MySpinnerAdapter dataAdapter = new MySpinnerAdapter(mContext,R.layout.simple_spinner_item,tempdepartment);
                    dataAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    holder.spinner.setAdapter(dataAdapter);
                }
                else if(position == 2)
                {
                    MySpinnerAdapter dataAdapter = new MySpinnerAdapter(mContext,R.layout.simple_spinner_item,temptickettype);
                    dataAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    holder.spinner.setAdapter(dataAdapter);
                }
                else
                {
                    MySpinnerAdapter dataAdapter = new MySpinnerAdapter(mContext,R.layout.simple_spinner_item,tempticketpriority);
                    dataAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    holder.spinner.setAdapter(dataAdapter);
                }

            }

            if(type == 2)
            {
                if(position == 4)
                {
                    holder.watcher.setTarget(position);
                }
            }

            if(type == 3)
            {
                if(position == 5)
                {
                    if(tb.getString(AppConstant.PREF_ADDTICKET_IMAGE) != "")
                    {
                        holder.imgupload.setVisibility(View.GONE);
                        holder.reladisplay.setVisibility(View.VISIBLE);
                        if(bitmapPhoto != null)
                        {
                            holder.imgdisplay.setImageBitmap(bitmapPhoto);
                        }
                    }
                    else
                    {
                        holder.reladisplay.setVisibility(View.GONE);
                        holder.imgupload.setVisibility(View.VISIBLE);
                    }
                }
            }

            if(type == 1)
            {
                holder.spinner.setTag(position);
            }

            return convertView;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
        {
            if(count >= 3)
            {
                if(parent != null && parent.getTag() != null)
                {
                    int spid = (int)parent.getTag();
                    Log.e("whichsoinner::",""+spid);

                    if(spid == 1)
                    {
                        finaldept = parent.getItemAtPosition(position).toString();
                    }
                    else if(spid == 2)
                    {
                        finaltickettype = parent.getItemAtPosition(position).toString();
                    }
                    else
                    {
                        finalticketprio = parent.getItemAtPosition(position).toString();
                    }
                }
            }

            count++;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent)
        {

        }

        private class EditTextWatcher implements TextWatcher
        {
            private int target;

            public void setTarget(int target)
            {
                this.target = target;
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if(s.length() > 0 && s.toString() != "")
                {
                    items.set(target, s.toString());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

        }

        public class ViewHolder
        {
            EditTextWatcher watcher;
            EditText edttitle,edtwritedesc;
            Spinner spinner;
            RelativeLayout reladisplay;
            ImageView imgupload,imgdisplay,imgedit,imgdelete;
        }
    }

    public boolean isStoragePermissionGranted()
    {
        if (Build.VERSION.SDK_INT >= 23)
        {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            }
            else
            {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(AddTicket.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_WRITE_STORAGE);
                return false;
            }
        }
        else
        { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    private void captureImage()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri();
        Log.e("fileUri", "fileUri" + fileUri.toString());
        tb.putString(AppConstant.PREF_ADDTICKET_IMAGE,fileUri.getPath());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent,CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    public Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    private static File getOutputMediaFile() {
        File mediaFile = null;
        // External sdcard location
        try {
            File mediaStorageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d(TAG, "Oops! Failed create "
                            + AppConstant.IMAGE_DIRECTORY_NAME);
                    return null;
                }
            }
            // Create a media file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());


            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
            return mediaFile;
        } catch (Exception ex) {

            ex.printStackTrace();
            return mediaFile;
        }
    }

    private void PickImage()
    {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i,RESULT_LOAD_IMAGE);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.btn_submit:
                checkvalidation();
                break;
        }
    }

    private void checkvalidation()
    {
        if(items.get(0).length() > 0 && items.get(0).toString() != ""
                && items.get(0).toString() != " ")
        {
            if(items.get(4).length() > 0 && items.get(4).toString() != ""
                    && items.get(4).toString() != " ")
            {
                for(int i = 0 ; i < arrticketdept.get(0).getArrdepartment().size() ;i++)
                {
                    if(finaldept.equalsIgnoreCase(arrticketdept.get(0).getArrdepartment().get(i).getDepartmentName()))
                    {
                        descid = arrticketdept.get(0).getArrdepartment().get(i).getDepartmentId();
                        break;
                    }

                }

                for(int i = 0 ; i < arrticketdept.get(0).getArrtickettypes().size() ;i++)
                {
                    if(finaltickettype.equalsIgnoreCase(arrticketdept.get(0).getArrtickettypes().get(i).getTickettypeName()))
                    {
                        tickettypeid = arrticketdept.get(0).getArrtickettypes().get(i).getTickettypeId();
                        break;
                    }
                }

                for(int i = 0 ; i < arrticketdept.get(0).getArrticketpriorities().size() ;i++)
                {
                    if(finalticketprio.equalsIgnoreCase(arrticketdept.get(0).getArrticketpriorities().get(i).getTicketPriority()))
                    {
                        tickerproiid = arrticketdept.get(0).getArrticketpriorities().get(i).getTicketpriorityId();
                        break;
                    }
                }

                callcreateticketws(descid,tickettypeid,tickerproiid);
            }
            else
            {
                AppGlobal.showToast(this,"Please Enter Description!");
            }
        }
        else
        {
            AppGlobal.showToast(this,"Please Enter Title!");
        }
    }


    private void callcreateticketws(String descid,String tickettypeid,String tickerproiid)
    {
        if(AppGlobal.isNetwork(this))
        {
            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(this,AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setTitle(items.get(0).toString());
            cm.setDept_id(descid);
            cm.setTicket_type_id(tickettypeid);
            cm.setPriority_id(tickerproiid);
            cm.setUser_id(userobj.getId());
            cm.setDescription(items.get(4).toString());
            cm.setImage(encodedImage);
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            try
            {
                new AsyncPostService(AddTicket.this,getString(R.string.Please_wait),WsConstant.Req_CreateTicket,cm,true,true)
                        .execute(WsConstant.WS_CREATETICKET);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        else
        {
            AppGlobal.showToast(this,getString(R.string.str_no_internet));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        try
        {
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
            {
                if (resultCode == Activity.RESULT_OK)
                {
                    setBitMap(tb.getString(AppConstant.PREF_ADDTICKET_IMAGE));
                }
                else if (resultCode == Activity.RESULT_CANCELED)
                {
                    // user cancelled Image capture
                    AppGlobal.showToast(this,"Image capture cancelled.");
                }
                else
                {
                    // failed to capture image
                    AppGlobal.showToast(this,"Sorry! Failed to capture image");
                }

            }

            if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != data)
            {
                Uri selectedImage = data.getData();
                if (selectedImage.toString().startsWith("file:///"))
                {
                    tb.putString(AppConstant.PREF_ADDTICKET_IMAGE,selectedImage.getPath());
                    setBitMap(selectedImage.getPath());
                    Log.e("selectedImage", "selectedImage=>" + selectedImage.getPath());
                }
                else
                {

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getApplicationContext().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);

                    if (cursor != null)
                    {
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String picturePath = cursor.getString(columnIndex);
                        cursor.close();
                        Log.e("picturePath", "picturePath=>" + picturePath);

                        tb.putString(AppConstant.PREF_ADDTICKET_IMAGE,picturePath);

                        setBitMap(picturePath);

                    }
                    else
                    {
                        super.onActivityResult(requestCode, resultCode, data);
                    }
                }

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void setBitMap(String picturePath)
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        //options.inSampleSize = 12;

        bitmapPhoto = BitmapFactory.decodeFile(picturePath, options);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmapPhoto.compress(Bitmap.CompressFormat.JPEG,85, baos);
        byte[] imageBytes = baos.toByteArray();
        encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        addticketadp.notifyDataSetChanged();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            if(whichaction.equalsIgnoreCase("camera"))
            {
                captureImage();
            }
            else if(whichaction.equalsIgnoreCase("gallery"))
            {
                PickImage();
            }

        }
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error)
    {
        if(error == null)
        {
            if(serviceType.equalsIgnoreCase(WsConstant.Req_CreateTicket))
            {
                String status = ((ResponseResult) data).getStatus();
                if(status.equalsIgnoreCase("1"))
                {
                   if(((ResponseResult) data).getMessage().equalsIgnoreCase("Ticket Created Successfully."))
                   {
                       EventBus.getDefault().post("CreateTicket");
                       finish();
                   }
                }
            }

        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }
}
