package com.manazel;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.Window;
import android.view.WindowManager;

import com.manazel.Constant.AppConstant;
import com.manazel.Constant.TinyDB;

public class SplashScreen extends FragmentActivity {
    private static final int SPLASH_TIME_OUT = 500;

    TinyDB tb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        tb = new TinyDB(SplashScreen.this);

//        if(!tb.isKeyExist(AppConstant.PREF_ISLOGIN))
//        {
//            tb.putBoolean(AppConstant.PREF_ISLOGIN,false);
//        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startApp();
                    }
                }, SPLASH_TIME_OUT);
            }
        }, SPLASH_TIME_OUT);
    }

    private void startApp() {

        if (tb.getBoolean(AppConstant.PREF_ISLOGIN)) {
            Intent intent = new Intent(SplashScreen.this, MainActivity.class);
            intent.putExtra("property_intent_data", false);
            startActivity(intent);
            finish();

        } else {
            Intent intent = new Intent(SplashScreen.this, LoginScreen.class);
            startActivity(intent);
            finish();
        }

    }
}
