package com.manazel;

import com.manazel.modal.TicketDeptData;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by c120 on 05/07/16.
 */
public class RealmHelper
{

    private static final String TAG = RealmHelper.class.getSimpleName();
    //private Context mContext;
    public Realm realm;

    public RealmHelper()
    {
//        realm = RealmAdaptor.getInstance(context);
        realm = RealmAdaptor.getInstance();
        // mContext = context;
    }

    public void close() {
        realm.close();
    }

    public void refresh() {
        // realm.refresh();
    }

    public void InsertIntoticketdeptdata(List<TicketDeptData> arrticketdept)
    {
        for(TicketDeptData ticketdept:arrticketdept)
        {
            realm.beginTransaction();
            realm.insert(ticketdept);
            realm.commitTransaction();
        }
    }

    public void DeleteAllTicketDeptData()
    {
        int ticketdeptcount = realm.where(TicketDeptData.class).findAll().size();
        if(ticketdeptcount > 0)
        {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<TicketDeptData> result = realm.where(TicketDeptData.class).findAll();
                    result.deleteAllFromRealm();
                }
            });
        }
    }

    public List<TicketDeptData> getAllTicketDept()
    {
        RealmQuery<TicketDeptData> query = realm.where(TicketDeptData.class);
        return query.findAll();
    }

    /**
     * Method to get the total records of table.
     *
     * @param className
     */

}
