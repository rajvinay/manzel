package com.manazel;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.manazel.Constant.AES_Helper;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.Newsdata;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.User;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by c119 on 28/11/16.
 */

public class NewsDetail extends FragmentActivity implements View.OnClickListener, WsResponseListener {
    ImageView imgmenu, imgback, imgplus, imgedit;
    TextView txtheader;

    RecyclerView recyclerView;
    NewsDetailAdapter mAdapter;
    List<Newsdata> NewsdetailList;
    RecyclerView.LayoutManager mLayoutManager;

    TinyDB tb;
    User userobj;

    Newsdata newsdetailobj;
    EditText diaedtsubject, diaedtcomment;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newsdetail);

        tb = new TinyDB(this);

        Gson gson1 = new Gson();
        String json1 = tb.getString("userobj");
        userobj = gson1.fromJson(json1, User.class);

        imgmenu = (ImageView) findViewById(R.id.img_menu);
        imgmenu.setVisibility(View.GONE);

        imgback = (ImageView) findViewById(R.id.img_back);
        imgback.setOnClickListener(this);
        imgback.setVisibility(View.VISIBLE);

        imgplus = (ImageView) findViewById(R.id.img_plus);
        imgplus.setVisibility(View.GONE);

        imgedit = (ImageView) findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView) findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(this));
        txtheader.setText(R.string.str_news);

        Gson gson = new Gson();
        String json = tb.getString("newsdetailobj");
        newsdetailobj = gson.fromJson(json, Newsdata.class);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        NewsdetailList = new ArrayList<>();
        NewsdetailList.add(newsdetailobj);

        mAdapter = new NewsDetailAdapter(this, NewsdetailList);
        recyclerView.setAdapter(mAdapter);
    }

    public class NewsDetailAdapter extends RecyclerView.Adapter<NewsDetailAdapter.MyViewHolder> {
        List<Newsdata> newsdetailList;
        Context ctx;
        SimpleDateFormat sdf, formatter;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView imgnews;
            public TextView txtnewstitle, txtname, txtdatetime, lblleavecmt, txtinquiry;
            public WebView webnews;

            public MyViewHolder(View view) {
                super(view);
                imgnews = (ImageView) view.findViewById(R.id.img_news);
                txtnewstitle = (TextView) view.findViewById(R.id.txt_news_title);
                txtnewstitle.setTypeface(AppGlobal.getLatoLight(ctx));
                txtname = (TextView) view.findViewById(R.id.txt_name);
                txtname.setTypeface(AppGlobal.getLatoBold(ctx));
                txtdatetime = (TextView) view.findViewById(R.id.txt_datetime);
                txtdatetime.setTypeface(AppGlobal.getLatoRegular(ctx));

                webnews = (WebView) view.findViewById(R.id.web_news);
                webnews.getSettings().setJavaScriptEnabled(true);
                webnews.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
                webnews.getSettings().setDefaultFontSize(15);

                webnews.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                if (Build.VERSION.SDK_INT >= 19) {
                    webnews.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                } else {
                    webnews.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                }

                lblleavecmt = (TextView) view.findViewById(R.id.lbl_leavecmt);
                lblleavecmt.setTypeface(AppGlobal.getLatoRegular(ctx));
                txtinquiry = (TextView) view.findViewById(R.id.txt_inquiry);
                txtinquiry.setTypeface(AppGlobal.getLatoRegular(ctx));

                txtinquiry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LayoutInflater factory = LayoutInflater.from(ctx);
                        final View deleteDialogView = factory.inflate(R.layout.dia_news, null);
                        dialog = new Dialog(ctx);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
                        dialog.setCancelable(false);
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.setContentView(deleteDialogView);

                        TextView dialblleavecmt = (TextView) deleteDialogView.findViewById(R.id.dia_lbl_leavecmt);
                        dialblleavecmt.setTypeface(AppGlobal.getLatoRegular(ctx));
                        TextView dialblsubject = (TextView) deleteDialogView.findViewById(R.id.dia_lbl_subject);
                        dialblsubject.setTypeface(AppGlobal.getLatoRegular(ctx));
                        TextView dialblcomment = (TextView) deleteDialogView.findViewById(R.id.dia_lbl_comment);
                        dialblcomment.setTypeface(AppGlobal.getLatoRegular(ctx));

                        diaedtsubject = (EditText) deleteDialogView.findViewById(R.id.dia_edt_subject);
                        diaedtsubject.setTypeface(AppGlobal.getLatoRegular(ctx));
                        diaedtcomment = (EditText) deleteDialogView.findViewById(R.id.dia_edt_comment);
                        diaedtcomment.setTypeface(AppGlobal.getLatoRegular(ctx));

                        ImageView diaimgcancel = (ImageView) deleteDialogView.findViewById(R.id.dia_img_cancel);
                        diaimgcancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        Button diabtncancel = (Button) deleteDialogView.findViewById(R.id.dia_btn_cancel);
                        diabtncancel.setTypeface(AppGlobal.getLatoRegular(ctx));
                        diabtncancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        Button diabtnsend = (Button) deleteDialogView.findViewById(R.id.dia_btn_send);
                        diabtnsend.setTypeface(AppGlobal.getLatoRegular(ctx));
                        diabtnsend.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                checkvalidation();
                            }
                        });

                        dialog.show();
                    }
                });
            }
        }

        public NewsDetailAdapter(Context ctx, List<Newsdata> newsdetailList) {
            this.ctx = ctx;
            this.newsdetailList = newsdetailList;
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter = new SimpleDateFormat("MMMM, dd, hh:mm a");
        }

        @Override
        public NewsDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.newsdetail_list_row, parent, false);
            return new NewsDetailAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(NewsDetailAdapter.MyViewHolder holder, int position) {
            if (newsdetailList.get(position).getNewsannouncementsImages() != "") {
                holder.imgnews.setVisibility(View.VISIBLE);
                Picasso.with(ctx).load(newsdetailList.get(position).getNewsannouncementsImages())
                        .placeholder(R.drawable.appimage).error(R.drawable.appimage).into(holder.imgnews);
            } else {
                holder.imgnews.setVisibility(View.GONE);
            }

            String data = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/Lato-Regular.ttf\")}body {color: #5a6a72; background-color: #FFFFFF ;font-family: MyFont;}</style></head>" +
                    "<body style='margin:0;padding:0;'>"
                    + newsdetailList.get(position).getDescriptions() +
                    "</body></html>";

            holder.txtnewstitle.setText(newsdetailList.get(position).getTitle());

            Date testDate = null;
            try {
                testDate = sdf.parse(newsdetailList.get(position).getModified_date());
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            holder.txtdatetime.setText(formatter.format(testDate));

            holder.webnews.loadData(data, "text/html; charset=utf-8", "UTF-8");
        }

        @Override
        public int getItemCount() {
            return newsdetailList.size();
        }
    }

    private void checkvalidation() {
        if (diaedtsubject.getText().toString().length() > 0 && (!diaedtsubject.getText().toString().equals(""))) {
            if (diaedtcomment.getText().toString().length() > 0 && (!diaedtcomment.getText().toString().equals(""))) {
                calladdcommentws();
                dialog.dismiss();
            } else {
                diaedtcomment.setError("Please enter comment!");
            }
        } else {
            diaedtsubject.setError("Please enter subject!");
        }
    }

    public void calladdcommentws() {
        if (AppGlobal.isNetwork(this)) {
            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(this, AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setUser_id(userobj.getId());
            cm.setArticle_id("0");
            cm.setType("1");
            cm.setSubject(diaedtsubject.getText().toString().trim());
            cm.setComment(diaedtcomment.getText().toString().trim());
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            try {
                new AsyncPostService(NewsDetail.this, getString(R.string.Please_wait), WsConstant.Req_AddComments, cm, true, true)
                        .execute(WsConstant.WS_ADDCOMMENTS);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            AppGlobal.showToast(this, getString(R.string.str_no_internet));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error) {
        if (error == null) {
            if (serviceType.equalsIgnoreCase(WsConstant.Req_AddComments)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    String message = ((ResponseResult) data).getMessage();
                    AppGlobal.showToast(this, message);
                }
            }

        }
    }

}
