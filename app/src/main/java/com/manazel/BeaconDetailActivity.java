package com.manazel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.manazel.modal.BeaconDataModel;

import java.util.List;

import io.realm.Case;
import io.realm.Realm;

public class BeaconDetailActivity extends AppCompatActivity {

    TextView txt_title;
    ImageView img_back;
    WebView webview;
    Realm realm;
    List<BeaconDataModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon_detail);
        initview();

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        list = realm.where(BeaconDataModel.class).findAll();
        for (BeaconDataModel beaconObj : list) {
            Log.e("TAG", beaconObj.getUuid());
        }

        if (getIntent() != null) {

            if (getIntent().hasExtra("uuid")) {

                Log.e("detected uuid", getIntent().getStringExtra("uuid"));
                String uuid = getIntent().getStringExtra("uuid");
                String major = getIntent().getStringExtra("major");
                String minor = getIntent().getStringExtra("minor");

                if (uuid != null) {

                    BeaconDataModel beaconObj = realm.where(BeaconDataModel.class)
                            .beginGroup()
                            .contains("uuid", uuid, Case.INSENSITIVE)
                            .contains("major", major, Case.INSENSITIVE)
                            .contains("minior", minor, Case.INSENSITIVE)
                            .endGroup()
                            .findFirst();


                    if (beaconObj != null) {

                        txt_title.setText(beaconObj.getBeaconName());
                        webview.getSettings().setJavaScriptEnabled(true);
                        webview.loadDataWithBaseURL("", beaconObj.getText(), "text/html", "UTF-8", "");
                    }

                }

            } else {

                Toast.makeText(this, "not found", Toast.LENGTH_SHORT).show();
            }
        }


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }

    public void initview() {

        txt_title = (TextView) findViewById(R.id.txt_title);
        img_back = (ImageView) findViewById(R.id.img_back);
        webview = (WebView) findViewById(R.id.webview);
    }
}
