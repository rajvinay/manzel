/**
 * @author c61
 * 
 *Single Listener for All web services call..
 *This is Interface, used for All the web services Response.
 *We can impliment this to get response of web services with any activity or fragment.
 */

package com.manazel.ws;

import android.os.Bundle;

public interface DelieverResponse {


	abstract void onDelieverResponse(int request_id, int status, Object data, String error, Bundle payload);

}
