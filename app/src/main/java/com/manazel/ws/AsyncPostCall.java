package com.manazel.ws;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by c136 on 11/05/16.
 */
public class AsyncPostCall extends AsyncTask<Void, Void, Void> {

    String url;
    int id;
    JSONObject jobj;
    JSONArray jarray;
    boolean isprogressVislble, deliverResult = true;
    String progress_msg = "";
    Context context;
    DelieverResponse wsRepsonse;
    ProgressDialog pd;
    InputStream inputStream;
    HttpURLConnection urlConnection;
    String result = null;
    Fragment f;
    int statusCode;
    String error = null;
    Bundle payload = null;

    public AsyncPostCall(Context context, String url, int id, JSONObject jobj, boolean isprogressVislble, String progress_msg) {
        this.url = url;
        this.id = id;
        this.jobj = jobj;
        this.isprogressVislble = isprogressVislble;
        this.progress_msg = progress_msg;
        this.context = context;
        //this.deliverResult = deliverResult; //must be true to get response callback

        if (deliverResult)
            wsRepsonse = (DelieverResponse) context;
    }


    public AsyncPostCall(Fragment f, String url, int id, JSONObject jobj, boolean isprogressVislble, String progress_msg) {
        this.url = url;
        this.id = id;
        this.jobj = jobj;
        this.isprogressVislble = isprogressVislble;
        this.progress_msg = progress_msg;
        this.f = f;
        //this.deliverResult = deliverResult; //must be true to get response callback
        this.context = f.getActivity();

        if (deliverResult)
            wsRepsonse = (DelieverResponse) f;
    }

    public AsyncPostCall(Context context, String url, int id, JSONArray jarray, boolean isprogressVislble, String progress_msg) {
        this.url = url;
        this.id = id;
        this.isprogressVislble = isprogressVislble;
        this.jarray = jarray;
        //this.deliverResult = deliverResult;
        this.progress_msg = progress_msg;
        this.context = context;

        if (deliverResult)
            wsRepsonse = (DelieverResponse) context;
    }

    public AsyncPostCall(Fragment f, String url, int id, JSONArray jarray, boolean isprogressVislble, String progress_msg) {
        this.url = url;
        this.id = id;
        this.isprogressVislble = isprogressVislble;
        this.jarray = jarray;
        //this.deliverResult = deliverResult;
        this.progress_msg = progress_msg;
        this.context = f.getActivity();

        if (deliverResult)
            wsRepsonse = (DelieverResponse) f;
    }

    public AsyncPostCall(Context context, String url, int id, JSONObject jobj, boolean isprogressVislble, String progress_msg, Bundle payload) {
        this.url = url;
        this.id = id;
        this.jobj = jobj;
        this.isprogressVislble = isprogressVislble;
        this.progress_msg = progress_msg;
        this.context = context;
        this.payload = payload;
        //this.deliverResult = deliverResult; //must be true to get response callback

        if (deliverResult)
            wsRepsonse = (DelieverResponse) context;
    }

    public AsyncPostCall(Fragment f, String url, int id, JSONObject jobj, boolean isprogressVislble, String progress_msg, Bundle payload) {
        this.url = url;
        this.id = id;
        this.jobj = jobj;
        this.isprogressVislble = isprogressVislble;
        this.progress_msg = progress_msg;
        this.f = f;
        this.payload = payload;
        //this.deliverResult = deliverResult; //must be true to get response callback
        this.context = f.getActivity();

        if (deliverResult)
            wsRepsonse = (DelieverResponse) f;
    }

    public AsyncPostCall(Context context, String url, int id, boolean isprogressVislble, String progress_msg) {
        this.url = url;
        this.id = id;
        this.isprogressVislble = isprogressVislble;
        this.progress_msg = progress_msg;
        this.context = context;
        //this.deliverResult = deliverResult; //must be true to get response callback

        if (deliverResult)
            wsRepsonse = (DelieverResponse) context;
    }


    public AsyncPostCall(Fragment f, String url, int id, boolean isprogressVislble, String progress_msg) {
        this.url = url;
        this.id = id;
        this.isprogressVislble = isprogressVislble;
        this.progress_msg = progress_msg;
        this.f = f;
        //this.deliverResult = deliverResult; //must be true to get response callback
        this.context = f.getActivity();

        if (deliverResult)
            wsRepsonse = (DelieverResponse) f;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (isprogressVislble) {
            pd = new ProgressDialog(context);
            pd.setMessage(progress_msg == null ? "" : progress_msg);
            pd.setCancelable(false);
            pd.show();
        }

    }


    @Override
    protected Void doInBackground(Void... params) {


        // Send data
        try {


            URL url = new URL(this.url);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Accept", "application/json");

            if (jobj != null || jarray != null) {

                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.connect();

                byte[] outputBytes;
               /* pass post data */
                if (jobj != null) {
                    outputBytes = jobj.toString().getBytes("UTF-8");

                } else {

                    outputBytes = jarray.toString().getBytes("UTF-8");
                }
                OutputStream os = urlConnection.getOutputStream();
                os.write(outputBytes);
                os.close();


            } else {

                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
            }

            try {

                /* Get Response and execute WebService request*/
                statusCode = urlConnection.getResponseCode();
            } catch (IOException e) {
                statusCode = urlConnection.getResponseCode();
            }
            Log.e("status code", statusCode + "");


            /* 200 represents HTTP OK */
            if (statusCode == HttpsURLConnection.HTTP_OK || statusCode == HttpURLConnection.HTTP_ACCEPTED) {

                inputStream = new BufferedInputStream(urlConnection.getInputStream());
                result = convertStreamToString(inputStream);

            } else if (statusCode == HttpURLConnection.HTTP_NO_CONTENT) {

                JSONObject jobj = new JSONObject();
                jobj.put("status", "1");
                result = jobj.toString();

            } else {

                //inputStream = new BufferedInputStream(urlConnection.getInputStream());
                inputStream = new BufferedInputStream(urlConnection.getErrorStream());
                error = convertStreamToString(inputStream);
                Log.e("Error", error);
                result = null;
            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        //  Log.e("Async result",result);
        if (isprogressVislble) {
            if (pd != null && pd.isShowing())
                pd.dismiss();
        }

        if (deliverResult) // if deliver call back enable then only deliver response
            wsRepsonse.onDelieverResponse(id, statusCode, result, error, payload);

    }


    public static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append((line + "\n"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
