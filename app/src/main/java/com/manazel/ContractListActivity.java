package com.manazel;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.modal.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ContractListActivity extends AppCompatActivity {

    RecyclerView recycleview;
    CustomRecycleview adapter;
    User userobj;
    TinyDB tb;
    Gson gson;
    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contract_list);

        tb = new TinyDB(this);
        gson = new Gson();

        String json = tb.getString("userobj");
        userobj = gson.fromJson(json, User.class);

        initview();

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        adapter = new CustomRecycleview(userobj.getContractImages());
        recycleview.setAdapter(adapter);
    }

    public void initview() {

        recycleview = (RecyclerView) findViewById(R.id.recycleview);
        img_back = (ImageView) findViewById(R.id.img_back);

        recycleview.setLayoutManager(new LinearLayoutManager(this));
    }


    public void showContractDialog(String url) {

        Dialog d = new Dialog(this);
        d.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        d.setContentView(R.layout.dialog_layout_contract);
        ImageView imgview = (ImageView) d.findViewById(R.id.imgview);

        Picasso.with(this).load(WsConstant.URL_CONTRACT_IMAGE + url).into(imgview);
        d.show();
    }

    class CustomRecycleview extends RecyclerView.Adapter {

        ArrayList<String> contract_list;

        public CustomRecycleview(ArrayList<String> contract_list) {
            this.contract_list = contract_list;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_contract, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {

            ViewHolder holder = (ViewHolder) viewholder;
            holder.txtview.setText("Contract " + position + ":" + contract_list.get(position));
        }

        @Override
        public int getItemCount() {
            return contract_list.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            TextView txtview;

            public ViewHolder(View itemView) {
                super(itemView);
                txtview = (TextView) itemView.findViewById(R.id.txt_contract);
                txtview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        showContractDialog(contract_list.get(getAdapterPosition()));

                    }
                });
            }

        }
    }
}
