package com.manazel;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.manazel.Constant.AES_Helper;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.User;
import com.manazel.modal.articlesarr;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by c119 on 09/12/16.
 */

public class ArticleDetail extends FragmentActivity implements View.OnClickListener,WsResponseListener
{
    ImageView imgmenu,imgback,imgplus,imgedit;
    TextView txtheader;

    RecyclerView recyclerView;
    ArticleDetailAdapter mAdapter;
    List<articlesarr> ArticledetailList;
    RecyclerView.LayoutManager mLayoutManager;

    TinyDB tb;
    User userobj;

    articlesarr articledetailobj;
    EditText diaedtsubject,diaedtcomment;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articledetail);

        tb = new TinyDB(this);

        Gson gson1 = new Gson();
        String json1 = tb.getString("userobj");
        userobj = gson1.fromJson(json1,User.class);

        imgmenu = (ImageView)findViewById(R.id.img_menu);
        imgmenu.setVisibility(View.GONE);

        imgback = (ImageView)findViewById(R.id.img_back);
        imgback.setOnClickListener(this);
        imgback.setVisibility(View.VISIBLE);

        imgplus = (ImageView)findViewById(R.id.img_plus);
        imgplus.setVisibility(View.GONE);

        imgedit = (ImageView)findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView)findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(this));
        txtheader.setText("Article");

        Gson gson = new Gson();
        String json = tb.getString("articledetailobj");
        articledetailobj = gson.fromJson(json,articlesarr.class);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);

        mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        ArticledetailList = new ArrayList<>();
        ArticledetailList.add(articledetailobj);

        mAdapter = new ArticleDetailAdapter(this,ArticledetailList);
        recyclerView.setAdapter(mAdapter);
    }

    public class ArticleDetailAdapter extends RecyclerView.Adapter<ArticleDetailAdapter.MyViewHolder>
    {
        List<articlesarr> articledetailList;
        Context ctx;
        SimpleDateFormat sdf,formatter;

        public class MyViewHolder extends RecyclerView.ViewHolder
        {
            public ImageView imgarticle;
            public TextView txtarticletitle,txtname,txtdatetime,lblleavecmt,txtinquiry;
            public WebView webarticle;

            public MyViewHolder(View view)
            {
                super(view);
                imgarticle = (ImageView) view.findViewById(R.id.img_article);
                txtarticletitle = (TextView) view.findViewById(R.id.txt_article_title);
                txtarticletitle.setTypeface(AppGlobal.getLatoLight(ctx));
                txtname = (TextView) view.findViewById(R.id.txt_name);
                txtname.setTypeface(AppGlobal.getLatoBold(ctx));
                txtdatetime = (TextView) view.findViewById(R.id.txt_datetime);
                txtdatetime.setTypeface(AppGlobal.getLatoRegular(ctx));

                webarticle = (WebView) view.findViewById(R.id.web_article);
                webarticle.getSettings().setJavaScriptEnabled(true);
                webarticle.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
                webarticle.getSettings().setDefaultFontSize(15);

                webarticle.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                if (Build.VERSION.SDK_INT >= 19)
                {
                    webarticle.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                }
                else
                {
                    webarticle.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                }

                lblleavecmt = (TextView) view.findViewById(R.id.lbl_leavecmt);
                lblleavecmt.setTypeface(AppGlobal.getLatoRegular(ctx));
                txtinquiry = (TextView) view.findViewById(R.id.txt_inquiry);
                txtinquiry.setTypeface(AppGlobal.getLatoRegular(ctx));

                txtinquiry.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        LayoutInflater factory = LayoutInflater.from(ctx);
                        final View deleteDialogView = factory.inflate(R.layout.dia_articles,null);
                        dialog = new Dialog(ctx);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
                        dialog.setCancelable(false);
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.setContentView(deleteDialogView);

                        TextView dialblleavecmt = (TextView)deleteDialogView.findViewById(R.id.dia_lbl_leavecmt);
                        dialblleavecmt.setTypeface(AppGlobal.getLatoRegular(ctx));
                        TextView dialblsubject = (TextView)deleteDialogView.findViewById(R.id.dia_lbl_subject);
                        dialblsubject.setTypeface(AppGlobal.getLatoRegular(ctx));
                        TextView dialblcomment = (TextView)deleteDialogView.findViewById(R.id.dia_lbl_comment);
                        dialblcomment.setTypeface(AppGlobal.getLatoRegular(ctx));

                        diaedtsubject = (EditText) deleteDialogView.findViewById(R.id.dia_edt_subject);
                        diaedtsubject.setTypeface(AppGlobal.getLatoRegular(ctx));
                        diaedtcomment = (EditText) deleteDialogView.findViewById(R.id.dia_edt_comment);
                        diaedtcomment.setTypeface(AppGlobal.getLatoRegular(ctx));

                        ImageView diaimgcancel = (ImageView) deleteDialogView.findViewById(R.id.dia_img_cancel);
                        diaimgcancel.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                dialog.dismiss();
                            }
                        });

                        Button diabtncancel = (Button) deleteDialogView.findViewById(R.id.dia_btn_cancel);
                        diabtncancel.setTypeface(AppGlobal.getLatoRegular(ctx));
                        diabtncancel.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                dialog.dismiss();
                            }
                        });

                        Button diabtnsend = (Button) deleteDialogView.findViewById(R.id.dia_btn_send);
                        diabtnsend.setTypeface(AppGlobal.getLatoRegular(ctx));
                        diabtnsend.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                checkvalidation();
                            }
                        });

                        dialog.show();
                    }
                });
            }
        }

        public ArticleDetailAdapter(Context ctx,List<articlesarr> articledetailList)
        {
            this.ctx = ctx;
            this.articledetailList = articledetailList;
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter = new SimpleDateFormat("MMMM, dd, hh:mm a");
        }

        @Override
        public ArticleDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.articledetail_list_row, parent, false);
            return new ArticleDetailAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ArticleDetailAdapter.MyViewHolder holder, int position)
        {
            if(articledetailList.get(position).getArticleImages() != "")
            {
                holder.imgarticle.setVisibility(View.VISIBLE);
                Picasso.with(ctx).load(articledetailList.get(position).getArticleImages())
                        .placeholder(R.drawable.appimage).error(R.drawable.appimage).into(holder.imgarticle);
            }
            else
            {
                holder.imgarticle.setVisibility(View.GONE);
            }

            String data = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/Lato-Regular.ttf\")}body {color: #5a6a72; background-color: #FFFFFF ;font-family: MyFont;}</style></head>"+
                    "<body style='margin:0;padding:0;'>"
                    +articledetailList.get(position).getDescriptions()+
                    "</body></html>";

            holder.txtarticletitle.setText(articledetailList.get(position).getTitle());

            Date testDate = null;
            try
            {
                testDate = sdf.parse(articledetailList.get(position).getModified_date());
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }

            holder.txtdatetime.setText(formatter.format(testDate));

            holder.webarticle.loadData(data,"text/html; charset=utf-8", "UTF-8");
        }

        @Override
        public int getItemCount()
        {
            return articledetailList.size();
        }
    }

    private void checkvalidation()
    {
        if (diaedtsubject.getText().toString().length() > 0 && (!diaedtsubject.getText().toString().equals(""))) {
            if (diaedtcomment.getText().toString().length() > 0 && (!diaedtcomment.getText().toString().equals(""))) {
                calladdcommentws();
                dialog.dismiss();
            } else {
                diaedtcomment.setError("Please enter comment!");
            }
        } else {
            diaedtsubject.setError( "Please enter subject!");
        }
    }


    public void calladdcommentws()
    {
        if(AppGlobal.isNetwork(this))
        {
            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(this,AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setUser_id(userobj.getId());
            cm.setArticle_id(articledetailobj.getArticleId());
            cm.setType("0");
            cm.setSubject(diaedtsubject.getText().toString().trim());
            cm.setComment(diaedtcomment.getText().toString().trim());
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            try
            {
                new AsyncPostService(ArticleDetail.this,getString(R.string.Please_wait),WsConstant.Req_AddComments,cm,true,true)
                        .execute(WsConstant.WS_ADDCOMMENTS);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        else
        {
            AppGlobal.showToast(this,getString(R.string.str_no_internet));
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error)
    {
        if(error == null)
        {
            if(serviceType.equalsIgnoreCase(WsConstant.Req_AddComments))
            {
                String status = ((ResponseResult) data).getStatus();
                if(status.equalsIgnoreCase("SUCCESS"))
                {
                    String message = ((ResponseResult) data).getMessage();
                    AppGlobal.showToast(this,message);
                }
            }

        }
    }
}
