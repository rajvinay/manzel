package com.manazel;

import android.*;
import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.manazel.Constant.AES_Helper;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.Fragment.AnnouncementFragment;
import com.manazel.Fragment.ArticleFragment;
import com.manazel.Fragment.CommunityFragment;
import com.manazel.Fragment.FAQFragment;
import com.manazel.Fragment.GalleryFragment;
import com.manazel.Fragment.MyProfileFragment;
import com.manazel.Fragment.NewsFragment;
import com.manazel.Fragment.PortfolioFragment;
import com.manazel.Fragment.PropertyFragment;
import com.manazel.Fragment.TicketsFragment;
import com.manazel.adapter.DrawerExpandableListAdp;
import com.manazel.app.Config;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.beacon.MyBeaconsService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.BeaconDataModel;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.User;
import com.manazel.utils.NotificationUtils;
import com.manazel.ws.AsyncPostCall;
import com.manazel.ws.DelieverResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.annotations.RealmClass;

import static com.manazel.Constant.WsConstant.Req_UpdateTokenforUser;

/**
 * Created by c119 on 23/11/16.
 */

public class MainActivity extends FragmentActivity implements WsResponseListener, DelieverResponse {

    DrawerLayout mDrawerLayout;
    LinearLayout drawer_Linear_layout;
    TextView lblmamazel;
    ExpandableListView mDrawerListView;

    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    DrawerExpandableListAdp listAdapter;
    int drawerheadpos, drawerchildpos;
    final int delay = 500;
    private static final String TAG = MainActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    TinyDB tb;
    RealmHelper realmhelp;
    FragmentTransaction transaction;
    Realm realm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Realm.init(this);
        realm = Realm.getDefaultInstance();
        RealmAdaptor.getInstance();

        realmhelp = new RealmHelper();
        tb = new TinyDB(this);

        if (!tb.isKeyExist(AppConstant.PREF_REGID)) {
            tb.putString(AppConstant.PREF_REGID, "");
        }

        checkMarshMellowPermissions();

        drawerheadpos = 0;
        drawerchildpos = -1;

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        //mDrawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        drawer_Linear_layout = (LinearLayout) findViewById(R.id.drawer_Linear_layout);
        int width = getResources().getDisplayMetrics().widthPixels
                - (getResources().getDisplayMetrics().widthPixels / 100) * 20;
        DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) drawer_Linear_layout.getLayoutParams();
        params.width = width;
        drawer_Linear_layout.setLayoutParams(params);

        lblmamazel = (TextView) findViewById(R.id.lbl_mamazel);
        lblmamazel.setTypeface(AppGlobal.getLatoLight(this));

        mDrawerListView = (ExpandableListView) findViewById(R.id.drExplist);
        mDrawerListView.setGroupIndicator(null);

        listDataHeader = new ArrayList<String>();
        listDataHeader.clear();
        listDataChild = new HashMap<String, List<String>>();
        listDataChild.clear();

        ArrayList<String> arrbase = new ArrayList<String>();
        arrbase.add(getString(R.string.drawer_faq));
        arrbase.add(getString(R.string.drawer_article));

        ArrayList<String> arrGallery = new ArrayList<String>();
        arrGallery.add(getString(R.string.drawer_reef_com));
        arrGallery.add(getString(R.string.drawer_reef));

        ArrayList<String> arrtemp = new ArrayList<String>();

        listDataHeader.add(getString(R.string.drawer_properties));
        listDataHeader.add(getString(R.string.drawer_news));
        listDataHeader.add(getString(R.string.drawer_tickets));
        listDataHeader.add(getString(R.string.drawer_gallery));
        listDataHeader.add(getString(R.string.drawer_portfolio));
        listDataHeader.add(getString(R.string.drawer_announcements));
        listDataHeader.add(getString(R.string.drawer_knowlwdge_base));
        listDataHeader.add(getString(R.string.drawer_my_profile));
        listDataHeader.add(getString(R.string.drawer_logout));

        listDataChild.put(listDataHeader.get(0), arrtemp);
        listDataChild.put(listDataHeader.get(1), arrtemp);
        listDataChild.put(listDataHeader.get(2), arrtemp);
        listDataChild.put(listDataHeader.get(3), arrGallery);
        listDataChild.put(listDataHeader.get(4), arrtemp);
        listDataChild.put(listDataHeader.get(5), arrtemp);
        listDataChild.put(listDataHeader.get(6), arrbase);
        listDataChild.put(listDataHeader.get(7), arrtemp);
        listDataChild.put(listDataHeader.get(8), arrtemp);

        listAdapter = new DrawerExpandableListAdp(this, listDataHeader, listDataChild);

        mDrawerListView.setAdapter(listAdapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                callgetallticketdeptws();
            }
        }, 500);

        callpropertyfrag();

        mDrawerListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                listAdapter.setparentselection(groupPosition);
                listAdapter.notifyDataSetChanged();

                switch (groupPosition) {
                    case 2:
                        closedrawer();
                        if (drawerheadpos != 2) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    callticketsfrag();
                                    drawerheadpos = 2;
                                    drawerchildpos = -1;
                                }
                            }, delay);

                        }
                        break;

                    case 1:
                        closedrawer();
                        if (drawerheadpos != 1) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    callnewsfrag();
                                    drawerheadpos = 1;
                                    drawerchildpos = -1;
                                }
                            }, delay);
                        }
                        break;

                    case 0:
                        closedrawer();
                        if (drawerheadpos != 0) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    callpropertyfrag();
                                    drawerheadpos = 0;
                                    drawerchildpos = -1;
                                }
                            }, delay);
                        }

                        break;

                    case 3:
//                        closedrawer();
                        if (drawerheadpos != 3) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    callgalleryfrag();
                                    mDrawerListView.expandGroup(3);
                                    drawerheadpos = 3;
                                    drawerchildpos = -1;
                                }
                            }, delay);
                        }
                        break;

                    case 4:
                        closedrawer();
                        if (drawerheadpos != 4) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    callportfoliofrag();
                                    drawerheadpos = 4;
                                    drawerchildpos = -1;
                                }
                            }, delay);
                        }
                        break;

                    case 5:
                        closedrawer();
                        if (drawerheadpos != 5) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    callannouncementfrag();
                                    drawerheadpos = 5;
                                    drawerchildpos = -1;
                                }
                            }, delay);
                        }
                        break;
                    case 6:
                        if (drawerheadpos != 6) {
                            drawerheadpos = 6;
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mDrawerListView.expandGroup(6);
                                    drawerheadpos = 6;
                                    drawerchildpos = -1;
                                }
                            }, delay);

                        }
                        break;
                    case 7:
                        closedrawer();
                        if (drawerheadpos != 7) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    callmyprofilefrag();
                                    drawerheadpos = 7;
                                    drawerchildpos = -1;
                                }
                            }, delay);
                        }
                        break;
                    case 8:
                        closedrawer();
                        if (drawerheadpos != 8) {
                            drawerheadpos = 8;
                            drawerchildpos = -1;
                            tb.putBoolean(AppConstant.PREF_ISLOGIN, false);
                            tb.clear();
                            Intent i = new Intent(MainActivity.this, LoginScreen.class);
                            startActivity(i);
                            finish();
                        }
                        break;

                }
                return false;
            }
        });

        mDrawerListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                listAdapter.setchildselection(childPosition);
                listAdapter.notifyDataSetChanged();

                switch (groupPosition) {
                    case 6:
                        switch (childPosition) {
                            case 0:
                                closedrawer();
                                if (drawerchildpos != 0) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            callfaqscreen();
                                            drawerchildpos = 0;
                                        }
                                    }, delay);
                                }
                                drawerheadpos = 5;
                                break;

                            case 1:
                                closedrawer();
                                if (drawerchildpos != 1) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            callarticlescreen();
                                            drawerchildpos = 1;
                                        }
                                    }, delay);
                                }
                                drawerheadpos = 5;
                                break;
                        }
                        break;
                    case 3:
                        switch (childPosition) {
                            case 0:
                                closedrawer();
                                if (drawerchildpos != 0) {
                                    tb.putString(AppConstant.PREF_GALLERY_SECTION_VALUE, "1");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            callgalleryfrag();
                                            drawerchildpos = 0;
                                        }
                                    }, delay);
                                }
                                drawerheadpos = 3;
                                break;

                            case 1:
                                closedrawer();
                                tb.putString(AppConstant.PREF_GALLERY_SECTION_VALUE, "2");
                                if (drawerchildpos != 1) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            callgalleryfrag();
                                            drawerchildpos = 1;
                                        }
                                    }, delay);
                                }
                                drawerheadpos = 3;
                                break;
                        }

                        break;
                }

                return false;
            }
        });

        mDrawerListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousItem = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousItem) {
                    mDrawerListView.collapseGroup(previousItem);
                }

                previousItem = groupPosition;
            }
        });

        //for fcm

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications

                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                    //txtMessage.setText(message);
                }
            }
        };

        displayFirebaseRegId();

        // TODO: 22/12/16 uncomment this to start beacon service
        Intent background = new Intent(MainActivity.this, MyBeaconsService.class);
        startService(background);

        callBeaconApi();

//        b = getIntent().getExtras();
//        if (b.getBoolean("property_intent_data")) {
//            callpropertyfrag();
//        }
    }

    public void checkMarshMellowPermissions() {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, AppConstant.MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

        } else {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, AppConstant.MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
            }
        }

    }

    private void callgetallticketdeptws() {
        if (AppGlobal.isNetwork(this)) {
            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(this, AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            try {
                new AsyncPostService(MainActivity.this, getString(R.string.Please_wait), WsConstant.Req_GetAllTicketsdept, cm, false, true)
                        .execute(WsConstant.WS_GETALLTICKETSDEPT);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            AppGlobal.showToast(this, getString(R.string.str_no_internet));
        }
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error) {
        if (error == null) {
            if (serviceType.equalsIgnoreCase(WsConstant.Req_GetAllTicketsdept)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    realmhelp.DeleteAllTicketDeptData();

                    int size = ((ResponseResult) data).getArrticketdept().size();
                    if (size > 0) {
                        realmhelp.InsertIntoticketdeptdata(((ResponseResult) data).getArrticketdept());
                    }
                }
            }

            if (serviceType.equalsIgnoreCase(WsConstant.Req_UpdateTokenforUser)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("1")) {
                    tb.putString(AppConstant.PREF_SECRETE_KEY, ((ResponseResult) data).getUserToken());
                }
            }
        }
    }

    private void displayFirebaseRegId() {
        String regId = tb.getString(AppConstant.PREF_REGID);
        Log.e(TAG, "Firebase reg id: " + regId);
    }

    @Override
    protected void onResume() {
        super.onResume();


        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());


        //check for update key for websevice
        try {
            if (tb.getBoolean(AppConstant.PREF_ISLOGIN)) {
                String secret_key = tb.getString(AppConstant.PREF_SECRETE_KEY);
                if (secret_key.equals("") || AppGlobal.isSecurityTokenExpire(this)) {
                    callupdateTokenforUser();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callupdateTokenforUser() {
        if (AppGlobal.isNetwork(this)) {
            Gson gson = new Gson();
            String json = tb.getString("userobj");
            User userobj = gson.fromJson(json, User.class);

            Common cm = new Common();
            cm.setUser_id(userobj.getId());

            try {
                new AsyncPostService(MainActivity.this, getString(R.string.Please_wait), Req_UpdateTokenforUser, cm, false, true)
                        .execute(WsConstant.WS_UPDATETOKENFORUSER);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            AppGlobal.showToast(this, getString(R.string.str_no_internet));
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    public void opendrawer() {
        mDrawerLayout.openDrawer(drawer_Linear_layout);
    }

    public void closedrawer() {
        mDrawerLayout.closeDrawer(drawer_Linear_layout);
    }

    private void callticketsfrag() {
        Fragment fragment = new TicketsFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment, "TicketsFragment");
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    private void callfaqscreen() {
        Fragment fragment = new FAQFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment, "FAQFragment");
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    private void callarticlescreen() {
        Fragment fragment = new ArticleFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment, "ArticleFragment");
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    private void callnewsfrag() {
        Fragment fragment = new NewsFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment, "NewsFragment");
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    public void callpropertyfrag() {
        Fragment fragment = new PropertyFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment, "PropertyFragment");
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    private void callgalleryfrag() {
        Fragment fragment = new GalleryFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment, "GalleryFragment");
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    private void callportfoliofrag() {
        Fragment fragment = new PortfolioFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment, "PortfolioFragment");
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    private void callannouncementfrag() {

        Fragment fragment = new AnnouncementFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment, "AnnouncementFragment");
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    private void callcommunityfrag() {
        Fragment fragment = new CommunityFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment, "CommunityFragment");
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    private void callmyprofilefrag() {
        Fragment fragment = new MyProfileFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment, "MyProfileFragment");
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    public void handlePush(String pushtype) {

        if (pushtype == AppConstant.PUSH_TYPE_NEWS) {

        } else if (pushtype == AppConstant.PUSH_TYPE_ARTICLE) {

            Fragment f = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        }

    }

    public void callBeaconApi() {

        new AsyncPostCall(this, WsConstant.WS_GET_ALL_BEACONS, WsConstant.Req_GET_ALL_BEACONS,
                false, null).execute();
    }

    @Override
    public void onDelieverResponse(int request_id, int status, Object data, String error, Bundle payload) {

        switch (request_id) {
            case WsConstant.Req_GET_ALL_BEACONS: {

                if (data != null) {

                    try {

                        JSONObject jobj = new JSONObject(data.toString());
                        if (jobj.optString("status").equalsIgnoreCase("SUCCESS")) {
                            JSONArray jarray = jobj.getJSONArray("Beaconsdata");

                            realm.beginTransaction();
                            realm.createOrUpdateAllFromJson(BeaconDataModel.class, jarray);
                            realm.commitTransaction();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case AppConstant.MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, AppConstant.MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
                }
            }
            break;
            case AppConstant.MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION: {

            }
            break;
            default:
                break;

        }
    }
}
