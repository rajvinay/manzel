package com.manazel;


import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by c85 on 19/11/15.
 * This class is use to create Realm instance with database and migration functionality.
 * it is working as a base class for Realm.
 */
public class RealmAdaptor
{

    public static Realm getInstance()
    {
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("Manazel.realm")
                .schemaVersion(0)
               // .migration(new Migration())
                .build();

        return Realm.getInstance(configuration);
    }

    public static boolean clearRealm()
    {
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("Manazel.realm")
                .schemaVersion(0)
               // .migration(new Migration())
                .build();

        return Realm.deleteRealm(configuration);
    }
}
