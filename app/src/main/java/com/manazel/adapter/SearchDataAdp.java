package com.manazel.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.WsConstant;
import com.manazel.PropertyDetailWithMap;
import com.manazel.R;
import com.manazel.modal.SearchProperty;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by c217 on 23/05/17.
 */

public class SearchDataAdp extends RecyclerView.Adapter<SearchDataAdp.MyViewHolder> {
    Context ctx;
    int w, h;
    List<SearchProperty> searchProperties;
    String image;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout main_Relative;
        private CardView main_Card;
        public ImageView iv_Property;
        public TextView txt_Price, txt_Bathroom, txt_BedroomCount, txt_PropertySize, txt_PropertyName,
                txt_Title, txt_Desp;

        public MyViewHolder(View view) {
            super(view);

            main_Relative = (RelativeLayout) view.findViewById(R.id.main_Relative);
            main_Card = (CardView) view.findViewById(R.id.main_Card);
            txt_Price = (TextView) view.findViewById(R.id.txt_Price);
            txt_Bathroom = (TextView) view.findViewById(R.id.txt_Bathroom);
            txt_BedroomCount = (TextView) view.findViewById(R.id.txt_BedroomCount);
            txt_PropertySize = (TextView) view.findViewById(R.id.txt_PropertySize);
            txt_PropertyName = (TextView) view.findViewById(R.id.txt_PropertyName);
            txt_Title = (TextView) view.findViewById(R.id.txt_Title);
            txt_Desp = (TextView) view.findViewById(R.id.txt_Desp);
            iv_Property = (ImageView) view.findViewById(R.id.iv_Property);
//            txttitle.setTypeface(AppGlobal.getLatoBold(ctx));


            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(w, h);
            main_Relative.setLayoutParams(lp);

            main_Card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callProertyDetail(searchProperties.get(getAdapterPosition()));
                }
            });

        }
    }


    public SearchDataAdp(Context ctx, int w, int h, List<SearchProperty> searchProperties) {
        this.ctx = ctx;
        this.w = w;
        this.h = h;
        this.searchProperties = searchProperties;

    }

    @Override
    public SearchDataAdp.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_search_properties, parent, false);
        return new SearchDataAdp.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchDataAdp.MyViewHolder holder, int position) {

        holder.txt_Title.setText(searchProperties.get(position).getType_name() + " for " + searchProperties.get(position).getCategoryName());
        holder.txt_Desp.setText(Html.fromHtml(searchProperties.get(position).getDescription()));
        holder.txt_Bathroom.setText(searchProperties.get(position).getBathroom_no());
        holder.txt_BedroomCount.setText(searchProperties.get(position).getBedroomNo());
        holder.txt_Price.setText(AppGlobal.addCommaInPrice(searchProperties.get(position).getPrice()));
        holder.txt_PropertyName.setText(searchProperties.get(position).getTitle());
        holder.txt_PropertySize.setText(searchProperties.get(position).getArea() + " SqFt");


        if (!(searchProperties.get(position).getImages().equals(""))) {
            image = searchProperties.get(position).getImages().split(",")[0];
            Picasso.with(ctx).load(WsConstant.IMAGE_HOST_URL + image).fit().into(holder.iv_Property);
        } else {
            holder.iv_Property.setImageResource(R.drawable.appimage);
        }
    }

    @Override
    public int getItemCount() {
        return searchProperties.size();
    }

    public void callProertyDetail(SearchProperty recent) {
        Gson gson = new Gson();
        String json = gson.toJson(recent);


        Intent i = new Intent(ctx, PropertyDetailWithMap.class);
        i.putExtra("property_detail", json);
        i.putExtra("coming_from", "search_result");
        ctx.startActivity(i);
    }
}


