package com.manazel.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.manazel.R;
import com.manazel.modal.Contract;
import com.manazel.modal.Location;

import java.util.ArrayList;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;

/**
 * Created by c217 on 23/05/17.
 */

public class LocationAdpForSearchData extends BaseAdapter {

    Context c;
    ArrayList<Location> objects;

    public LocationAdpForSearchData(Context context, ArrayList<Location> objects) {
        super();
        this.c = context;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        View row = inflater.inflate(R.layout.row_spinner, parent, false);
        TextView label = (TextView) row.findViewById(R.id.tag);
        if (position == 0) {
            label.setText(objects.get(position).getCity());
        } else {
            label.setText(objects.get(position).getCity() + "(" + objects.get(position).getCount() + ")");
        }

        return row;
    }

    public String getValueforPosition(int position) {
        return objects.get(position).getCity();
    }


    public int getIndexByProperty(String value) {
        int pos = 0;
        for (int i = 0; i < objects.size(); i++) {
            if (objects.get(i) != null && objects.get(i).getCity().equals(value)) {
                pos = i;
            }
        }
        return pos;
    }
}


