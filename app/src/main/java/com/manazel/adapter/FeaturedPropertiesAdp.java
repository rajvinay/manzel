package com.manazel.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.WsConstant;
import com.manazel.PropertyDetailWithMap;
import com.manazel.R;
import com.manazel.modal.Featured;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by c217 on 19/05/17.
 */

public class FeaturedPropertiesAdp extends RecyclerView.Adapter<FeaturedPropertiesAdp.MyViewHolder> {
    Activity ctx;
    int w, h;
    List<Featured> featureds;

    String image;
    double multiplier;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout main_Relative;
        private CardView main_Card;
        public ImageView iv_Property;
        public TextView txt_Price, txt_Bathroom, txt_BedroomCount, txt_PropertySize, txt_PropertyName;

        public MyViewHolder(View view) {
            super(view);

            main_Relative = (RelativeLayout) view.findViewById(R.id.main_Relative);
            main_Card = (CardView) view.findViewById(R.id.main_Card);
            txt_Price = (TextView) view.findViewById(R.id.txt_Price);
            txt_Bathroom = (TextView) view.findViewById(R.id.txt_Bathroom);
            txt_BedroomCount = (TextView) view.findViewById(R.id.txt_BedroomCount);
            txt_PropertySize = (TextView) view.findViewById(R.id.txt_PropertySize);
            txt_PropertyName = (TextView) view.findViewById(R.id.txt_PropertyName);
            iv_Property = (ImageView) view.findViewById(R.id.iv_Property);
//            txttitle.setTypeface(AppGlobal.getLatoBold(ctx));


            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(w, h);
            main_Relative.setLayoutParams(lp);

            main_Card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callProertyDetail(featureds.get(getAdapterPosition()));
                }
            });

        }
    }


    public FeaturedPropertiesAdp(Activity ctx, int w, int h, List<Featured> featureds) {
        this.ctx = ctx;
        this.w = w;
        this.h = h;
        this.featureds = featureds;
        multiplier = ((double) h / (double) w);

    }

    @Override
    public FeaturedPropertiesAdp.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recent_properties, parent, false);
        return new FeaturedPropertiesAdp.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FeaturedPropertiesAdp.MyViewHolder holder, int position) {


        holder.txt_Bathroom.setText(featureds.get(position).getBathroomNo());
        holder.txt_BedroomCount.setText(featureds.get(position).getBedroomNo());
        holder.txt_Price.setText(AppGlobal.addCommaInPrice(featureds.get(position).getPrice()));
        holder.txt_PropertyName.setText(featureds.get(position).getTitle());
        holder.txt_PropertySize.setText(featureds.get(position).getArea() + " sqft");


        if (!(featureds.get(position).getImages().equals(""))) {
            image = featureds.get(position).getImages().split(",")[0];
            Picasso.with(ctx).load(WsConstant.IMAGE_HOST_URL + image).resize((int) (multiplier * (double) h), (int) (multiplier * (double) h)).into(holder.iv_Property);
        } else {
            holder.iv_Property.setImageResource(R.drawable.appimage);
        }


    }

    @Override
    public int getItemCount() {
        return featureds.size();
    }

    public void callProertyDetail(Featured featured) {
        Gson gson = new Gson();
        String json = gson.toJson(featured);


        Intent i = new Intent(ctx, PropertyDetailWithMap.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("property_detail", json);
        i.putExtra("coming_from", "featured");
        ctx.startActivity(i);
//        ctx.finish();
    }
}


