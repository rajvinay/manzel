package com.manazel.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.manazel.R;
import com.manazel.modal.Category;
import com.manazel.modal.Contract;

import java.util.ArrayList;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;

/**
 * Created by c217 on 23/05/17.
 */

public class CategoryAdpForSearchData extends BaseAdapter {

    Context c;
    ArrayList<Category> objects;

    public CategoryAdpForSearchData(Context context, ArrayList<Category> objects) {
        super();
        this.c = context;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        View row = inflater.inflate(R.layout.row_spinner, parent, false);
        TextView label = (TextView) row.findViewById(R.id.tag);
        label.setText(objects.get(position).getPropertyTypeName());

        return row;
    }

    public String getValueforPosition(int position) {
        return objects.get(position).getPropertyTypeName();
    }

    public String getSelectedDataId(String value) {
        int a = getIndexByProperty(value);
        Log.e(TAG, "getSelectedDataId: POSITION VALUE A:::::" + a);
        if (a == -1 || a == 0) {
            return "";
        } else {
            return objects.get(a).getPropertyTypeId();
        }
    }

    public int getIndexByProperty(String value) {
        int pos = 0;
        for (int i = 0; i < objects.size(); i++) {
            if (objects.get(i) != null && objects.get(i).getPropertyTypeName().equals(value)) {
                pos = i;
            }
        }
        return pos;
    }

}


