package com.manazel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.manazel.Constant.AppGlobal;
import com.manazel.Fragment.ArticleFragment;
import com.manazel.R;
import com.manazel.modal.Articledata;
import com.manazel.modal.articlesarr;

import java.util.List;

/**
 * Created by c119 on 09/12/16.
 */

public class ArticleExpandableListAdp extends BaseExpandableListAdapter
{
    private Context _context;

    private ChildViewHolder childViewHolder;
    private GroupViewHolder groupViewHolder;
    List<Articledata> articledatas;
    ArticleFragment frag;

    public ArticleExpandableListAdp(Context _context, List<Articledata> articledatas,ArticleFragment frag)
    {
        this._context = _context;
        this.articledatas = articledatas;
        this.frag = frag;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon)
    {
        return this.articledatas.get(groupPosition).getArrarticlechild().get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final articlesarr childText = (articlesarr) getChild(groupPosition, childPosition);

        if (convertView == null)
        {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.article_list_item, null);

            childViewHolder = new ChildViewHolder();
            childViewHolder.relachild = (RelativeLayout) convertView.findViewById(R.id.rela_child);
            childViewHolder.mChildText = (TextView) convertView.findViewById(R.id.lblListItem);
            childViewHolder.mChildText.setTypeface(AppGlobal.getLatoRegular(_context));

            convertView.setTag(childViewHolder);

            childViewHolder.relachild.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    String tag = (String) v.getTag();
                    String[] arr = tag.split(",");
                    int grp = Integer.parseInt(arr[0]);
                    int childp = Integer.parseInt(arr[1]);

                    frag.callarticledetail(articledatas.get(grp).getArrarticlechild().get(childp));
                }
            });
        }
        else
        {
            childViewHolder = (ChildViewHolder) convertView.getTag();
        }

        childViewHolder.mChildText.setText(childText.getTitle());

        childViewHolder.relachild.setTag(groupPosition + "," + childPosition);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition)
    {
        return this.articledatas.get(groupPosition).getArrarticlechild().size();
    }

    @Override
    public Object getGroup(int groupPosition)
    {
        return this.articledatas.get(groupPosition).getCategoryName();
    }

    @Override
    public int getGroupCount()
    {
        return this.articledatas.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
    {
        String headerTitle = (String) getGroup(groupPosition);

        if (convertView == null)
        {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.article_list_group, null);

            groupViewHolder = new GroupViewHolder();
            groupViewHolder.mGroupText = (TextView) convertView.findViewById(R.id.lblListHeader);
            groupViewHolder.mGroupText.setTypeface(AppGlobal.getLatoBold(_context));
            groupViewHolder.mimagearrow = (ImageView) convertView.findViewById(R.id.img_arrow);

            convertView.setTag(groupViewHolder);
        }
        else
        {
            groupViewHolder = (GroupViewHolder) convertView.getTag();
        }

        groupViewHolder.mGroupText.setText(headerTitle);

        int imageResourceId = isExpanded ? R.drawable.arrow_article_expand : R.drawable.arrow_article_collapse;
        groupViewHolder.mimagearrow.setImageResource(imageResourceId);

        return convertView;
    }

    public final class ChildViewHolder
    {
        TextView mChildText;
        RelativeLayout relachild;
    }

    public final class GroupViewHolder {
        TextView mGroupText;
        ImageView mimagearrow;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
