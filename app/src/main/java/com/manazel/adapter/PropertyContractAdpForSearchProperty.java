package com.manazel.adapter;

import android.app.Activity;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.manazel.R;
import com.manazel.modal.Contract;

import java.util.ArrayList;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;

/**
 * Created by c217 on 22/05/17.
 */

public class PropertyContractAdpForSearchProperty extends ArrayAdapter<Contract> {

    Context c;
    ArrayList<Contract> objects;
    String value;


    private static class ViewHolder {
        CheckedTextView tag;
    }

    public PropertyContractAdpForSearchProperty(Context context, ArrayList<Contract> objects) {
        super(context, android.R.layout.simple_list_item_single_choice, objects);
        this.c = context;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater) c
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

//        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_spinner, null);
//            convertView = mInflater.inflate(android.R.layout.simple_list_item_single_choice, null);
            holder = new ViewHolder();
            holder.tag = (CheckedTextView) convertView.findViewById(R.id.tag);
            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
        holder.tag.setText(objects.get(position).getPropertyCategoryName());
        holder.tag.setTag(position);
        return convertView;
    }


    public String getValueforPosition(int position) {
        return objects.get(position).getPropertyCategoryName();
    }


    public String getSelectedDataId(String value) {
        int a = getIndexByProperty(value);
        if (a == -1 || a == 0) {
            return "";
        } else {
            return objects.get(a).getPropertyCategoryId();
        }
    }

    public int getIndexByProperty(String value) {
        int pos = 0;
        for (int i = 0; i < objects.size(); i++) {
            if (objects.get(i) != null && objects.get(i).getPropertyCategoryName().equals(value)) {
                pos = i;
            }
        }
        return pos;
    }
}


