package com.manazel.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.manazel.R;

import java.util.ArrayList;

/**
 * Created by c217 on 27/05/17.
 */

public class FilterOptionSearchResultAdp extends BaseAdapter {

    Context c;
    ArrayList<String> objects;

    public FilterOptionSearchResultAdp(Context context, ArrayList<String> objects) {
        super();
        this.c = context;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        View row = inflater.inflate(R.layout.row_spinner, parent, false);
        TextView label = (TextView) row.findViewById(R.id.tag);
        label.setText(objects.get(position));

        return row;
    }



    public int getIndexByProperty(String value) {
        int pos = 0;
        for (int i = 0; i < objects.size(); i++) {
            if (objects.get(i) != null && objects.get(i).equals(value)) {
                pos = i;
            }
        }
        return pos;
    }
}


