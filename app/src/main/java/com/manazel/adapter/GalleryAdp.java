package com.manazel.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.WsConstant;
import com.manazel.Fragment.NewsFragment;
import com.manazel.R;
import com.manazel.SingleGalleryImageActivity;
import com.manazel.modal.Newsdata;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by c217 on 29/05/17.
 */

public class GalleryAdp extends RecyclerView.Adapter<GalleryAdp.MyViewHolder> {

    Context ctx;
    ArrayList<String> imgs;
    int w, h;
    double multiplier;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout main_Relative;
        ImageView iv_GalleryImg;

        public MyViewHolder(View view) {
            super(view);
            main_Relative = (RelativeLayout) view.findViewById(R.id.main_Relative);
            iv_GalleryImg = (ImageView) view.findViewById(R.id.iv_GalleryImg);

            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(w, h);
            main_Relative.setLayoutParams(lp);

            iv_GalleryImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(ctx,SingleGalleryImageActivity.class);
                    intent.putExtra("selected_pos",getAdapterPosition());
                    intent.putExtra("full_array",imgs);
                    ctx.startActivity(intent);
                }
            });

        }
    }

    public GalleryAdp(Context ctx, ArrayList<String> imgs, int w, int h) {
        this.ctx = ctx;
        this.imgs = imgs;
        this.w = w;
        this.h = h;
        multiplier = ((double) h / (double) w);
    }

    @Override
    public GalleryAdp.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_gallery, parent, false);
        return new GalleryAdp.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GalleryAdp.MyViewHolder holder, int position) {

        Picasso.with(ctx)
                .load(WsConstant.GALLERY_IMAGE_HOST_URL + imgs.get(position))
                .priority(Picasso.Priority.HIGH)
                .fit()
                .centerCrop()
                .into(holder.iv_GalleryImg);

        //.resize((int) (multiplier * (double) w), (int) (multiplier * (double) w))

    }

    @Override
    public int getItemCount() {
        return imgs.size();
    }
}

