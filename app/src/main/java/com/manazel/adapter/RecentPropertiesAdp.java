package com.manazel.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.WsConstant;
import com.manazel.Fragment.PropertyFragment;
import com.manazel.PropertyDetailWithMap;
import com.manazel.R;
import com.manazel.modal.Recent;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.TAG;

/**
 * Created by c217 on 19/05/17.
 */

public class RecentPropertiesAdp extends RecyclerView.Adapter<RecentPropertiesAdp.MyViewHolder> {
    Activity ctx;
    int w, h;
    List<Recent> recents;
    String image;
    double multiplier;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout main_Relative;
        private CardView main_Card;
        public ImageView iv_Property;
        public TextView txt_Price, txt_Bathroom, txt_BedroomCount, txt_PropertySize, txt_PropertyName;

        public MyViewHolder(View view) {
            super(view);

            main_Relative = (RelativeLayout) view.findViewById(R.id.main_Relative);
            main_Card = (CardView) view.findViewById(R.id.main_Card);
            txt_Price = (TextView) view.findViewById(R.id.txt_Price);
            txt_Bathroom = (TextView) view.findViewById(R.id.txt_Bathroom);
            txt_BedroomCount = (TextView) view.findViewById(R.id.txt_BedroomCount);
            txt_PropertySize = (TextView) view.findViewById(R.id.txt_PropertySize);
            txt_PropertyName = (TextView) view.findViewById(R.id.txt_PropertyName);
            iv_Property = (ImageView) view.findViewById(R.id.iv_Property);
//            txttitle.setTypeface(AppGlobal.getLatoBold(ctx));


            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(w, h);
            main_Relative.setLayoutParams(lp);

            main_Card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callProertyDetail(recents.get(getAdapterPosition()));
                }
            });

        }
    }


    public RecentPropertiesAdp(Activity ctx, int w, int h, List<Recent> recents) {
        this.ctx = ctx;
        this.w = w;
        this.h = h;
        this.recents = recents;
        multiplier = ((double) h / (double) w);
    }

    @Override
    public RecentPropertiesAdp.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recent_properties, parent, false);
        return new RecentPropertiesAdp.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecentPropertiesAdp.MyViewHolder holder, int position) {


        holder.txt_Bathroom.setText(recents.get(position).getBathroomNo());
        holder.txt_BedroomCount.setText(recents.get(position).getBedroomNo());
        holder.txt_Price.setText(AppGlobal.addCommaInPrice(recents.get(position).getPrice()));
        holder.txt_PropertyName.setText(recents.get(position).getTitle());
        holder.txt_PropertySize.setText(recents.get(position).getArea() + " sqft");

        if (!(recents.get(position).getImages().equals(""))) {
            image = recents.get(position).getImages().split(",")[0];
            Picasso.with(ctx).load(WsConstant.IMAGE_HOST_URL + image).resize((int) (multiplier * (double) h), (int) (multiplier * (double) h)).into(holder.iv_Property);
        } else {
            holder.iv_Property.setImageResource(R.drawable.appimage);
        }


    }

    @Override
    public int getItemCount() {
        return recents.size();
    }

    public void callProertyDetail(Recent recent) {
        Gson gson = new Gson();
        String json = gson.toJson(recent);
        Intent i = new Intent(ctx, PropertyDetailWithMap.class);
        i.putExtra("property_detail", json);
        i.putExtra("coming_from", "recent");
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
        ctx.startActivity(i);
//        ctx.finish();
    }
}


