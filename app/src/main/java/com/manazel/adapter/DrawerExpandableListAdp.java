package com.manazel.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.manazel.Constant.AppGlobal;
import com.manazel.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by c119 on 09/11/16.
 */

public class DrawerExpandableListAdp extends BaseExpandableListAdapter {
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;
    private ChildViewHolder childViewHolder;
    private GroupViewHolder groupViewHolder;
    int selected_grp = -1, selected_child = -1, selected_off;

    public Integer[] report = {
            R.drawable.drawer_faq,
            R.drawable.drawer_article
    };

    public Integer[] reportde = {
            R.drawable.drawer_faq_wt,
            R.drawable.drawer_article_wt
    };

    public Integer[] groupicons = {
            R.drawable.drawer_ticket
            , R.drawable.drawer_news
            , R.drawable.pro1
            , R.drawable.gallery1
            , R.drawable.pro1
            , R.drawable.drawer_community
            , R.drawable.drawer_knowledge
            , R.drawable.drawer_profile
            , R.drawable.drawer_logout};

    public Integer[] groupdeicons = {
            R.drawable.drawer_ticket_wt
            , R.drawable.drawer_news_wt
            , R.drawable.pro2
            , R.drawable.gallery2
            , R.drawable.pro2
            , R.drawable.drawer_community_wt
            , R.drawable.drawer_knowledge_wt
            , R.drawable.drawer_profile_wt
            , R.drawable.drawer_logout_wt};


    public DrawerExpandableListAdp(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.drawer_list_item, null);
            childViewHolder = new ChildViewHolder();
            childViewHolder.drchildicon = (ImageView) convertView.findViewById(R.id.dr_child_icon);
            childViewHolder.drchildtxt = (TextView) convertView.findViewById(R.id.dr_childtxt);
            childViewHolder.drchildtxt.setTypeface(AppGlobal.getLatoRegular(_context));

            convertView.setTag(childViewHolder);
        } else {
            childViewHolder = (ChildViewHolder) convertView.getTag();
        }

        if (groupPosition == 6) {
            childViewHolder.drchildicon.setImageResource(report[childPosition]);

            if (selected_child == childPosition) {
                childViewHolder.drchildicon.setImageResource(reportde[childPosition]);
                childViewHolder.drchildtxt.setTextColor(ContextCompat.getColor(_context, R.color.drawer_text_selected));
            } else {
                childViewHolder.drchildicon.setImageResource(report[childPosition]);
                childViewHolder.drchildtxt.setTextColor(ContextCompat.getColor(_context, R.color.drawer_text_deselected));
            }
        } else if (groupPosition == 3) {
            if (selected_child == childPosition) {
                childViewHolder.drchildicon.setImageResource(0);
                childViewHolder.drchildtxt.setTextColor(ContextCompat.getColor(_context, R.color.drawer_text_selected));
            } else {
                childViewHolder.drchildicon.setImageResource(0);
                childViewHolder.drchildtxt.setTextColor(ContextCompat.getColor(_context, R.color.drawer_text_deselected));
            }
        }

        childViewHolder.drchildtxt.setText(childText);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.drawer_list_group, null);

            groupViewHolder = new GroupViewHolder();
            groupViewHolder.relagrpmain = (RelativeLayout) convertView.findViewById(R.id.rela_grp_main);
            groupViewHolder.dricon = (ImageView) convertView.findViewById(R.id.dr_icon);
            groupViewHolder.drheadertxt = (TextView) convertView.findViewById(R.id.dr_headertxt);
            groupViewHolder.drheadertxt.setTypeface(AppGlobal.getLatoRegular(_context));
            groupViewHolder.mimagearrow = (ImageView) convertView.findViewById(R.id.img_arrow);

            convertView.setTag(groupViewHolder);
        } else {
            groupViewHolder = (GroupViewHolder) convertView.getTag();
        }

        groupViewHolder.dricon.setImageResource(groupicons[groupPosition]);
        groupViewHolder.drheadertxt.setText(headerTitle);

        if (groupPosition == 6 || groupPosition == 3) {
            groupViewHolder.mimagearrow.setVisibility(View.VISIBLE);
        } else {
            groupViewHolder.mimagearrow.setVisibility(View.GONE);
        }
        groupViewHolder.dricon.setScaleType(ImageView.ScaleType.FIT_CENTER);
        if (selected_grp == groupPosition) {
            groupViewHolder.relagrpmain.setBackgroundColor(Color.parseColor("#3f9dd9"));
            groupViewHolder.dricon.setImageResource(groupdeicons[groupPosition]);
            groupViewHolder.drheadertxt.setTextColor(ContextCompat.getColor(_context, R.color.drawer_text_selected));
        } else {
            groupViewHolder.relagrpmain.setBackgroundColor(Color.parseColor("#26292d"));
            groupViewHolder.dricon.setImageResource(groupicons[groupPosition]);
            groupViewHolder.drheadertxt.setTextColor(ContextCompat.getColor(_context, R.color.drawer_text_deselected));
        }

        int imageResourceId = isExpanded ? R.drawable.arrow_expand : R.drawable.arrow_collapse;
        groupViewHolder.mimagearrow.setImageResource(imageResourceId);

        return convertView;
    }

    public final class ChildViewHolder {
        TextView drchildtxt;
        ImageView drchildicon;
    }

    public final class GroupViewHolder {
        TextView drheadertxt;
        ImageView dricon, mimagearrow;
        RelativeLayout relagrpmain;
    }

    public void setparentselection(int i) {
        selected_grp = i;
    }

    public void setchildselection(int i) {
        selected_child = i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
