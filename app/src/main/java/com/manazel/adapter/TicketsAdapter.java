package com.manazel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.manazel.Constant.AppGlobal;
import com.manazel.Fragment.TicketsFragment;
import com.manazel.R;
import com.manazel.modal.Ticketsdata;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by c119 on 25/11/16.
 */

public class TicketsAdapter extends BaseAdapter
{
    public Context _context;
    public ArrayList<Ticketsdata> _listtickets;
    public LayoutInflater layoutInflater;
    public int selected_grp=-1;
    public TicketsFragment frag;
    SimpleDateFormat sdf,formatter;

    public TicketsAdapter(Context context, ArrayList<Ticketsdata> _listtickets, TicketsFragment frag)
    {
        this._context = context;
        this._listtickets = _listtickets;
        this.frag = frag;
        layoutInflater = LayoutInflater.from(context);
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter = new SimpleDateFormat("h:mm a dd MMMM yyyy");
    }

    public ArrayList<Ticketsdata> get_listtickets()
    {
        return _listtickets;
    }

    public void set_listtickets(ArrayList<Ticketsdata> _listtickets)
    {
        this._listtickets = _listtickets;
    }

    @Override
    public int getCount()
    {
        return _listtickets.size();
    }

    @Override
    public Object getItem(int position)
    {
        return _listtickets.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;
        if (convertView == null)
        {
            convertView = layoutInflater.inflate(R.layout.row_list_ticket, null);
            holder = new ViewHolder();

            holder.relamain = (RelativeLayout) convertView.findViewById(R.id.rela_main);
            holder.imgticket = (ImageView) convertView.findViewById(R.id.img_ticket);

            holder.txttitle = (TextView) convertView.findViewById(R.id.txt_title);
            holder.txttitle.setTypeface(AppGlobal.getLatoBlack(_context));

            holder.txtdept = (TextView) convertView.findViewById(R.id.txt_department);
            holder.txtdept.setTypeface(AppGlobal.getLatoBold(_context));

            holder.txtseriesno = (TextView) convertView.findViewById(R.id.txt_seriesno);
            holder.txtseriesno.setTypeface(AppGlobal.getLatoBold(_context));

            holder.txtdatetime = (TextView) convertView.findViewById(R.id.txt_datetime);
            holder.txtdatetime.setTypeface(AppGlobal.getLatoRegular(_context));

            holder.txtassignto = (TextView) convertView.findViewById(R.id.txt_assignto);
            holder.txtassignto.setTypeface(AppGlobal.getLatoRegular(_context));

            holder.lblpriority = (TextView) convertView.findViewById(R.id.lbl_priority);
            holder.lblpriority.setTypeface(AppGlobal.getLatoRegular(_context));

            holder.txtpriority = (TextView) convertView.findViewById(R.id.txt_priority);
            holder.txtpriority.setTypeface(AppGlobal.getLatoBold(_context));

            holder.lblstatus = (TextView) convertView.findViewById(R.id.lbl_status);
            holder.lblstatus.setTypeface(AppGlobal.getLatoRegular(_context));

            holder.txtstatus = (TextView) convertView.findViewById(R.id.txt_status);
            holder.txtstatus.setTypeface(AppGlobal.getLatoBold(_context));

            convertView.setTag(holder);

            holder.relamain.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    int pos = (int)v.getTag();
                    //AppGlobal.showToast(_context,_listtickets.get(pos).getSeries_no());
                    frag.callticketdetail(_listtickets.get(pos));
                }
            });
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        if(_listtickets.get(position).getTicketImages() != "")
        {
            Picasso.with(_context).load(_listtickets.get(position).getTicketImages())
                    .placeholder(R.drawable.ticket_list_placeholder)
                    .error(R.drawable.ticket_list_placeholder).into(holder.imgticket);
        }
        else
        {
            holder.imgticket.setImageResource(R.drawable.ticket_list_placeholder);
        }

        holder.txttitle.setText(_listtickets.get(position).getTitle());
        holder.txtdept.setText(_listtickets.get(position).getDepartmentName());
        holder.txtseriesno.setText(_listtickets.get(position).getSeries_no());

        Date testDate = null;
        try
        {
            testDate = sdf.parse(_listtickets.get(position).getModified());
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

        holder.txtdatetime.setText("Updated : "+formatter.format(testDate));
        holder.txtassignto.setText("Assign To : "+_listtickets.get(position).getAssignTofname()
                +" "+_listtickets.get(position).getAssignTolname());
        holder.txtpriority.setText(_listtickets.get(position).getTicketPriority());
        holder.txtstatus.setText(_listtickets.get(position).getTicketStatus());

        holder.relamain.setTag(position);

        return convertView;
    }

    public class ViewHolder
    {
        TextView txttitle,txtdept,txtseriesno,txtdatetime,txtassignto,txtpriority,txtstatus,lblpriority,lblstatus;
        ImageView imgticket;
        RelativeLayout relamain;
    }
}

