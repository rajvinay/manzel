package com.manazel.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.manazel.Constant.AppGlobal;
import com.manazel.Fragment.NewsFragment;
import com.manazel.R;
import com.manazel.modal.Newsdata;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by c119 on 28/11/16.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder>
{
    List<Newsdata> newsList;
    Context ctx;
    NewsFragment frag;
    int selected = -1;
    SimpleDateFormat sdf,formatter;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public RelativeLayout relamain;
        public ImageView imgnews;
        public TextView txtdatetime,txttitle;

        public MyViewHolder(View view) {
            super(view);
            relamain = (RelativeLayout) view.findViewById(R.id.rela_main);
            imgnews = (ImageView) view.findViewById(R.id.img_news);
            txtdatetime = (TextView) view.findViewById(R.id.txt_datetime);
            txtdatetime.setTypeface(AppGlobal.getLatoRegular(ctx));
            txttitle = (TextView) view.findViewById(R.id.txt_title);
            txttitle.setTypeface(AppGlobal.getLatoBold(ctx));

            relamain.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    selected = getAdapterPosition();
                    frag.callnewsdetail(newsList.get(getAdapterPosition()));
                    notifyDataSetChanged();
                }
            });
        }
    }

    public List<Newsdata> getNewsList() {
        return newsList;
    }

    public void setNewsList(List<Newsdata> newsList) {
        this.newsList = newsList;
    }

    public NewsAdapter(Context ctx, List<Newsdata> newsList, NewsFragment frag)
    {
        this.ctx = ctx;
        this.newsList = newsList;
        this.frag = frag;
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter = new SimpleDateFormat("MMMM, dd, hh:mm a");
    }

    @Override
    public NewsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_row, parent, false);
        return new NewsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NewsAdapter.MyViewHolder holder, int position)
    {
        if(position == selected)
        {
            holder.relamain.setBackgroundColor(Color.parseColor("#F0F2F4"));
        }
        else
        {
            holder.relamain.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }

        if(newsList.get(position).getNewsannouncementsImages() != "")
        {
            Picasso.with(ctx).load(newsList.get(position).getNewsannouncementsImages())
                    .placeholder(R.drawable.appimage).error(R.drawable.appimage).into(holder.imgnews);
        }
        else
        {
            holder.imgnews.setImageResource(R.drawable.appimage);
        }

        Date testDate = null;
        try
        {
            testDate = sdf.parse(newsList.get(position).getModified_date());
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

        holder.txtdatetime.setText(formatter.format(testDate));

        holder.txttitle.setText(newsList.get(position).getTitle());
    }

    @Override
    public int getItemCount()
    {
        return newsList.size();
    }
}

