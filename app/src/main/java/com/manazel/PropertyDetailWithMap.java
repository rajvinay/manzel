package com.manazel;

import android.*;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.Fragment.SearchFragment;
import com.manazel.Views.CirclePageIndicator;
import com.manazel.Views.ExpandableGridView;
import com.manazel.adapter.AmenitiesAdpForDetail;
import com.manazel.adapter.SlidingImage_Adapter;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.Featured;
import com.manazel.modal.Offer;
import com.manazel.modal.Property;
import com.manazel.modal.Recent;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.SearchProperty;
import com.manazel.modal.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;

public class PropertyDetailWithMap extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, View.OnTouchListener,
        WsResponseListener {

    private GoogleMap mMap;
    private ExpandableGridView gv_Amenities;
    AmenitiesAdpForDetail adapterAmenities;
    SlidingImage_Adapter pager_Adp;
    ArrayList<String> listAmenities = new ArrayList<>();
    ImageView img_back, img_menu, iv_Share;
    private TextView txt_header, txt_Price, txt_Title, txt_Desp, txt_RefId, txt_PriceTopTag,
            txt_Area, txt_BedroomCount, txt_Bathroom, txt_AgentName, txt_PhoneNo, txt_Email, txt_Address,
            txt_Save, txt_Share, txt_Print, txt_SatelliteView, txt_NormalView;
    private ScrollView mainScrollView;
    private ImageView transparentImageView, iv_Agent, iv_Save;
    private ViewPager mPager;
    private ArrayList<String> ImagesArray = new ArrayList<String>();
    private CirclePageIndicator indicator;
    private Bundle b;
    private Recent recent;
    private Offer offer;
    private Featured featured;
    private SearchProperty searchProperty;
    private String latitude = "", longitude = "";
    private SupportMapFragment mapFragment;
    private int width, height;
    private String propertyId;
    private TinyDB tb;
    private User userobj;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_detail_with_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        setupControls();
        initGlobal();
    }

    private void setupControls() {
        b = getIntent().getExtras();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;
        height = displaymetrics.heightPixels;

        img_back = (ImageView) findViewById(R.id.img_back);
        mainScrollView = (ScrollView) findViewById(R.id.main_scrollview);
        img_menu = (ImageView) findViewById(R.id.img_menu);
        iv_Share = (ImageView) findViewById(R.id.iv_Share);
        txt_header = (TextView) findViewById(R.id.txt_header);
        txt_Price = (TextView) findViewById(R.id.txt_Price);
        txt_Title = (TextView) findViewById(R.id.txt_Title);
        txt_RefId = (TextView) findViewById(R.id.txt_RefId);
        txt_PriceTopTag = (TextView) findViewById(R.id.txt_PriceTopTag);
        txt_Area = (TextView) findViewById(R.id.txt_Area);
        txt_Desp = (TextView) findViewById(R.id.txt_Desp);
        txt_BedroomCount = (TextView) findViewById(R.id.txt_BedroomCount);
        txt_Bathroom = (TextView) findViewById(R.id.txt_Bathroom);
        txt_AgentName = (TextView) findViewById(R.id.txt_AgentName);
        txt_PhoneNo = (TextView) findViewById(R.id.txt_PhoneNo);
        txt_Email = (TextView) findViewById(R.id.txt_Email);
        txt_Address = (TextView) findViewById(R.id.txt_Address);
        txt_Save = (TextView) findViewById(R.id.txt_Save);
        txt_Share = (TextView) findViewById(R.id.txt_Share);
        txt_Print = (TextView) findViewById(R.id.txt_Print);
        txt_SatelliteView = (TextView) findViewById(R.id.txt_SatelliteView);
        txt_NormalView = (TextView) findViewById(R.id.txt_NormalView);
        transparentImageView = (ImageView) findViewById(R.id.transparent_image);
        iv_Agent = (ImageView) findViewById(R.id.iv_Agent);
        iv_Save = (ImageView) findViewById(R.id.iv_Save);


        gv_Amenities = (ExpandableGridView) findViewById(R.id.gv_Amenities);
        gv_Amenities.setExpanded(true);
        adapterAmenities = new AmenitiesAdpForDetail(PropertyDetailWithMap.this, listAmenities);
        gv_Amenities.setAdapter(adapterAmenities);

        indicator = (CirclePageIndicator) findViewById(R.id.indicator);

        pager_Adp = new SlidingImage_Adapter(PropertyDetailWithMap.this, ImagesArray, width, (int) (height / 2));

        mPager = (ViewPager) findViewById(R.id.pager);


    }

    private void initGlobal() {
        Gson gson = new Gson();
        tb = new TinyDB(PropertyDetailWithMap.this);
        String json = tb.getString("userobj");
        userobj = gson.fromJson(json, User.class);
        String response = b.getString("property_detail");

        if (b.getString("coming_from").equals("recent")) {
            recent = gson.fromJson(response, Recent.class);
            setAllValues(true, false, false, false);
        } else if (b.getString("coming_from").equals("featured")) {
            featured = gson.fromJson(response, Featured.class);
            setAllValues(false, true, false, false);
        } else if (b.getString("coming_from").equals("search_result")) {
            searchProperty = gson.fromJson(response, SearchProperty.class);
            setAllValues(false, false, false, true);
        } else {
            offer = gson.fromJson(response, Offer.class);
            setAllValues(false, false, true, false);
        }

        mainScrollView.scrollTo(0, 0);

        mPager.setAdapter(pager_Adp);
        indicator.setViewPager(mPager);
        final float density = getResources().getDisplayMetrics().density;
        //Set circle indicator radius
        indicator.setRadius(4 * density);

        txt_header.setText("Properties");
        img_menu.setVisibility(View.GONE);
        img_back.setVisibility(View.VISIBLE);

        img_back.setOnClickListener(this);
        txt_Save.setOnClickListener(this);
        iv_Save.setOnClickListener(this);
        txt_Share.setOnClickListener(this);
        iv_Share.setOnClickListener(this);
        txt_PhoneNo.setOnClickListener(this);
        txt_Email.setOnClickListener(this);
        txt_NormalView.setOnClickListener(this);
        txt_SatelliteView.setOnClickListener(this);
        transparentImageView.setOnTouchListener(this);

    }

    private void setAllValues(boolean isRecent, boolean isFeatured, boolean isOffer, boolean isSearchResult) {

        if (isRecent) {
            propertyId = recent.getPropertyListId();
            txt_Title.setText("IN FOR " + recent.getCategoryName() + ", STARTED " + recent.getCreatedDate().split("\\s+")[0]);
            txt_Desp.setText(Html.fromHtml(recent.getDescription()));
            txt_RefId.setText(recent.getReferenceNumber());
            txt_Price.setText(AppGlobal.addCommaInPrice(recent.getPrice()));
            txt_PriceTopTag.setText(AppGlobal.addCommaInPrice(recent.getPrice()));
            txt_Area.setText(recent.getArea() + " SqFt");
            txt_BedroomCount.setText(recent.getBedroomNo());
            txt_Bathroom.setText(recent.getBathroomNo());
            txt_AgentName.setText(recent.getContactName());
            txt_PhoneNo.setText(recent.getContactNo());
            txt_Email.setText(recent.getContactEmail());
            txt_Address.setText(recent.getAddress());

            listAmenities.clear();
            ArrayList<String> amemities = new ArrayList<String>(Arrays.asList(recent.getAmenities().split(",")));
            for (int i = 0; i < amemities.size(); i++) {
                listAmenities.add(i, amemities.get(i));
            }
            adapterAmenities.notifyDataSetChanged();
            amemities.clear();
            amemities = new ArrayList<>(Arrays.asList(recent.getImages().split(",")));
            for (int i = 0; i < amemities.size(); i++) {
                ImagesArray.add(i, amemities.get(i));
            }
            pager_Adp.notifyDataSetChanged();
            latitude = recent.getLatitude();
            longitude = recent.getLongitude();
            if (recent.getIs_like().equals("0")) {
                iv_Save.setImageResource(R.drawable.like_detail);
            } else {
                iv_Save.setImageResource(R.drawable.like_filled_detail);
            }
        } else if (isFeatured) {
            propertyId = featured.getPropertyListId();
            txt_Title.setText("IN FOR " + featured.getCategoryName() + ", STARTED " + featured.getCreatedDate().split("\\s+")[0]);
            txt_Desp.setText(Html.fromHtml(featured.getDescription()));
            txt_RefId.setText(featured.getReferenceNumber());
            txt_Price.setText(AppGlobal.addCommaInPrice(featured.getPrice()));
            txt_PriceTopTag.setText(AppGlobal.addCommaInPrice(featured.getPrice()));
            txt_Area.setText(featured.getArea() + " SqFt");
            txt_BedroomCount.setText(featured.getBedroomNo());
            txt_Bathroom.setText(featured.getBathroomNo());
            txt_AgentName.setText(featured.getContactName());
            txt_PhoneNo.setText(featured.getContactNo());
            txt_Email.setText(featured.getContactEmail());
            txt_Address.setText(featured.getAddress());

            listAmenities.clear();
            ArrayList<String> amemities = new ArrayList<String>(Arrays.asList(featured.getAmenities().split(",")));
            for (int i = 0; i < amemities.size(); i++) {
                listAmenities.add(i, amemities.get(i));
            }
            adapterAmenities.notifyDataSetChanged();
            amemities.clear();
            amemities = new ArrayList<>(Arrays.asList(featured.getImages().split(",")));
            for (int i = 0; i < amemities.size(); i++) {
                ImagesArray.add(i, amemities.get(i));
            }
            pager_Adp.notifyDataSetChanged();
            latitude = featured.getLatitude();
            longitude = featured.getLongitude();
            if (featured.getIs_like().equals("0")) {
                iv_Save.setImageResource(R.drawable.like_detail);
            } else {
                iv_Save.setImageResource(R.drawable.like_filled_detail);
            }
        } else if (isSearchResult) {
            propertyId = searchProperty.getProperty_list_id();
            txt_Title.setText("IN FOR " + searchProperty.getCategoryName() );
            txt_Desp.setText(Html.fromHtml(searchProperty.getDescription()));
            txt_RefId.setText(searchProperty.getReference_number());
            txt_Price.setText(AppGlobal.addCommaInPrice(searchProperty.getPrice()));
            txt_PriceTopTag.setText(AppGlobal.addCommaInPrice(searchProperty.getPrice()));
            txt_Area.setText(searchProperty.getArea() + " SqFt");
            txt_BedroomCount.setText(searchProperty.getBedroomNo());
            txt_Bathroom.setText(searchProperty.getBathroom_no());
            txt_AgentName.setText(searchProperty.getContact_name());
            txt_PhoneNo.setText(searchProperty.getContact_no());
            txt_Email.setText(searchProperty.getContactEmail());
            txt_Address.setText(searchProperty.getAddress());

            listAmenities.clear();
            ArrayList<String> amemities = new ArrayList<String>(Arrays.asList(searchProperty.getAmenities().split(",")));
            for (int i = 0; i < amemities.size(); i++) {
                listAmenities.add(i, amemities.get(i));
            }
            adapterAmenities.notifyDataSetChanged();
            amemities.clear();
            amemities = new ArrayList<>(Arrays.asList(searchProperty.getImages().split(",")));
            for (int i = 0; i < amemities.size(); i++) {
                ImagesArray.add(i, amemities.get(i));
            }
            pager_Adp.notifyDataSetChanged();
            latitude = searchProperty.getLatitude();
            longitude = searchProperty.getLongitude();
            if (searchProperty.getIs_like().equals("0")) {
                iv_Save.setImageResource(R.drawable.like_detail);
            } else {
                iv_Save.setImageResource(R.drawable.like_filled_detail);
            }
        } else {
            propertyId = offer.getPropertyListId();
            txt_Title.setText("IN FOR " + offer.getCategoryName() + ", STARTED " + offer.getCreatedDate().split("\\s+")[0]);
            txt_Desp.setText(Html.fromHtml(offer.getDescription()));
            txt_RefId.setText(offer.getReferenceNumber());
            txt_Price.setText(AppGlobal.addCommaInPrice(offer.getPrice()));
            txt_PriceTopTag.setText(AppGlobal.addCommaInPrice(offer.getPrice()));
            txt_Area.setText(offer.getArea() + " SqFt");
            txt_BedroomCount.setText(offer.getBedroomNo());
            txt_Bathroom.setText(offer.getBathroomNo());
            txt_AgentName.setText(offer.getContactName());
            txt_PhoneNo.setText(offer.getContactNo());
            txt_Email.setText(offer.getContactEmail());
            txt_Address.setText(offer.getAddress());

            listAmenities.clear();
            ArrayList<String> amemities = new ArrayList<String>(Arrays.asList(offer.getAmenities().split(",")));
            for (int i = 0; i < amemities.size(); i++) {
                listAmenities.add(i, amemities.get(i));
            }
            adapterAmenities.notifyDataSetChanged();
            amemities.clear();
            amemities = new ArrayList<>(Arrays.asList(offer.getImages().split(",")));
            for (int i = 0; i < amemities.size(); i++) {
                ImagesArray.add(i, amemities.get(i));
            }
            pager_Adp.notifyDataSetChanged();
            latitude = offer.getLatitude();
            longitude = offer.getLongitude();
            if (offer.getIs_like().equals("0")) {
                iv_Save.setImageResource(R.drawable.like_detail);
            } else {
                iv_Save.setImageResource(R.drawable.like_filled_detail);
            }
        }


        mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(this);
        onMapReady(mMap);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {


        if (googleMap != null) {

            mMap = googleMap;
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            if (!(latitude.equals("") && longitude.equals("") && txt_Address.getText().toString().trim().equals(""))) {
                // Add a marker in Sydney and move the camera
                LatLng sydney = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                String title = txt_Address.getText().toString().trim();
                mMap.addMarker(new MarkerOptions().position(sydney).title(title));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(sydney, 16);
                mMap.animateCamera(location);
                changeMapTextView(true);

            }
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.txt_SatelliteView:
                changeMapTextView(false);
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.txt_NormalView:
                changeMapTextView(true);
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.img_back:
                onBackPressed();
                PropertyDetailWithMap.this.finish();
                break;
            case R.id.iv_Save:
                callAddPropertyToWishlist();
                break;
            case R.id.txt_Save:
                callAddPropertyToWishlist();
                break;
            case R.id.txt_PhoneNo:
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 369);

                } else {
                    intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + txt_PhoneNo.getText().toString().trim()));
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                }
                break;
            case R.id.txt_Email:
                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{txt_Email.getText().toString().trim()});
                intent.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
                intent.putExtra(Intent.EXTRA_TEXT, "body of email");
                try {
                    startActivity(Intent.createChooser(intent, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    AppGlobal.showToast(PropertyDetailWithMap.this, "There are no email clients installed.");
                }
                break;
            case R.id.txt_Share:
                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "http://35.154.4.76/property-listing/single-property/Beautiful-5-bedroom-villa-with-lovely-green-garden-and-pool-Arabian-Village/MTE=");
                startActivity(Intent.createChooser(intent, "Share via"));
                break;
            case R.id.iv_Share:
                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "http://35.154.4.76/property-listing/single-property/Beautiful-5-bedroom-villa-with-lovely-green-garden-and-pool-Arabian-Village/MTE=");
                startActivity(Intent.createChooser(intent, "Share via"));
                break;
        }

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 369: {

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + txt_PhoneNo.getText().toString().trim()));
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                } else {
                    AppGlobal.showSnackBar(txt_Save, "Call phone permission denied.");
                }
            }
            break;
        }
    }

    private void callAddPropertyToWishlist() {
        if (AppGlobal.isNetwork(PropertyDetailWithMap.this)) {

            Common cm = new Common();

            cm.setUser_id(userobj.getId());
            cm.setProperty_list_id(propertyId);

            try {
                new AsyncPostService(this, getString(R.string.Please_wait), WsConstant.Req_Add_Property_to_Wishlist, cm, true, true)
                        .execute(WsConstant.WS_ADD_PROPERTY_TO_WISHLIST);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            AppGlobal.showToast(PropertyDetailWithMap.this, getString(R.string.str_no_internet));
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                // Disallow ScrollView to intercept touch events.
                mainScrollView.requestDisallowInterceptTouchEvent(true);
                // Disable touch on transparent view
                return false;

            case MotionEvent.ACTION_UP:
                // Allow ScrollView to intercept touch events.
                mainScrollView.requestDisallowInterceptTouchEvent(false);
                return true;

            case MotionEvent.ACTION_MOVE:
                mainScrollView.requestDisallowInterceptTouchEvent(true);
                return false;

            default:
                return true;
        }
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error) {
        if (error == null) {
            if (serviceType.equalsIgnoreCase(WsConstant.Req_Add_Property_to_Wishlist)) {
                String status = ((ResponseResult) data).getStatus();

                if (status.equalsIgnoreCase("SUCCESS")) {


                    if (iv_Save.getDrawable().getConstantState() == ContextCompat.getDrawable(PropertyDetailWithMap.this, R.drawable.like_detail).getConstantState()) {
                        AppGlobal.showSnackBar(txt_Save, "Property added to wishlist.");
                        iv_Save.setImageResource(R.drawable.like_filled_detail);
                    } else {
                        AppGlobal.showSnackBar(txt_Save, "Property removed from wishlist.");
                        iv_Save.setImageResource(R.drawable.like_detail);
                    }
                } else {
                    if (iv_Save.getDrawable().getConstantState() == ContextCompat.getDrawable(PropertyDetailWithMap.this, R.drawable.like_detail).getConstantState()) {

                        AppGlobal.showSnackBar(txt_Save, "Add property to wishlist failed.");
                    } else {

                        AppGlobal.showSnackBar(txt_Save, "Remove property to wishlist failed.");
                    }

                }

            }
        }
    }

    public void changeMapTextView(boolean isNormal) {
        if (isNormal) {
            txt_NormalView.setTypeface(AppGlobal.getLatoBold(this));
            txt_NormalView.setTextColor(ContextCompat.getColor(PropertyDetailWithMap.this, R.color.colorPrimaryDark));

            txt_SatelliteView.setTypeface(AppGlobal.getLatoRegular(this));
            txt_SatelliteView.setTextColor(ContextCompat.getColor(PropertyDetailWithMap.this, R.color.black));
        } else {
            txt_SatelliteView.setTypeface(AppGlobal.getLatoBold(this));
            txt_SatelliteView.setTextColor(ContextCompat.getColor(PropertyDetailWithMap.this, R.color.colorPrimaryDark));

            txt_NormalView.setTypeface(AppGlobal.getLatoRegular(this));
            txt_NormalView.setTextColor(ContextCompat.getColor(PropertyDetailWithMap.this, R.color.black));
        }
    }
}
