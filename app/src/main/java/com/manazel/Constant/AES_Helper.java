package com.manazel.Constant;

import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES_Helper {

	private Cipher cipher;
	private SecretKeySpec skeySpec;
	private IvParameterSpec ivParameterSpec;

	/**
	 * 
	 */
	public AES_Helper(String masterKey) {

		try {
			skeySpec = getKey(masterKey);
			final byte[] iv = new byte[16];
			Arrays.fill(iv, (byte) 0x00);
			ivParameterSpec = new IvParameterSpec(iv);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String encode(String stringToEncode) throws NullPointerException {

		try {
			Log.e("AES_Helper", stringToEncode + "  encode: " );
			cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);
			byte[] clearText = stringToEncode.getBytes("UTF8");
			//Log.d("jacek", "Encrypted: " + stringToEncode + " -> " + encrypedValue);
			return Base64.encodeToString(cipher.doFinal(clearText), Base64.DEFAULT);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Decodes a String using AES-256 and Base64

	 * @param text
	 * @return desoded String
	 */
	public String decode(String text) throws NullPointerException {

		try {
			cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParameterSpec);
			byte[] encrypedPwdBytes = Base64.decode(text, Base64.DEFAULT);
			byte[] decrypedValueBytes = (cipher.doFinal(encrypedPwdBytes));
			return new String(decrypedValueBytes);

		} catch (Exception e) {
//			e.printStackTrace();
			return text;
 		}
 	}

	/**
	 * Generates a SecretKeySpec for given password
	 *
 	 * @return SecretKeySpec
	 * @throws UnsupportedEncodingException
	 */
	private SecretKeySpec getKey(String masterKey) throws UnsupportedEncodingException {

		// You can change it to 128 if you wish
		int keyLength = 256;
		byte[] keyBytes = new byte[keyLength / 8];
		// explicitly fill with zeros
		Arrays.fill(keyBytes, (byte) 0x0);

		// if password is shorter then key length, it will be zero-padded
		// to key length
		byte[] passwordBytes = masterKey.getBytes("UTF-8");
		int length = passwordBytes.length < keyBytes.length ? passwordBytes.length : keyBytes.length;
		System.arraycopy(passwordBytes, 0, keyBytes, 0, length);
		return new SecretKeySpec(keyBytes, "AES");
	}

}
