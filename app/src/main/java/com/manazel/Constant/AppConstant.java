package com.manazel.Constant;

/**
 * Created by c119 on 29/03/16.
 */
public class AppConstant {

    public static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 111;
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 112;
    public static final int PERMISSIONS_REQUEST_ACCESS_CALL_PHONE = 113;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;

    public static double latitude = 0.000000;
    public static double longitude = 0.000000;


    public static final String PREF_PROFILE_IMAGE = "pref_profile_image";

    public static final String PREF_CONTRACT_IMAGE = "pref_contract_image";

    public static final String PREF_EDIT_PROFILE_CONTRACT_IMAGE = "pref_edit_profile_contract_image";

    public static final String PREF_ADDTICKET_IMAGE = "pref_addticket_image";

    public static final String PREF_EDITPROFILE_IMAGE = "pref_editprofile_image";

    public static final String PREF_REGID = "pref_regId";

    public static final String PREF_REG_EMAIL_VALUE = "pref_email_value_registration";

    public static final String IMAGE_DIRECTORY_NAME = "Manazel";

    //for main app ws security
    public static final String APP_PREF_GLOBAL_PASSWORD = "pref_global_password";

    public static final String No_user_name = "nousername";

    public static final String APP_PREF_TEMPTOKEN = "pref_temptoken";

    //for getting from response

    public static final String PREF_SECRETE_KEY = "pref_Secrete_Key";

    public static final String PREF_GUID = "pref_guid";

    public static final String PREF_GALLERY_SECTION_VALUE = "pref_gallery_section";

    public static final String PREF_SECRETE_KEY_DATE = "pref_Secrete_Key_date";

    public static final String PREF_ISLOGIN = "pref_islogin";

//    public static final String PREF_WISHLIST_STATUS_CHANGED_FROM_PROPERTY_DETAIL = "wishlist_status";

    /*  Push Type */
    public static final String PUSH_TYPE_NEWS = "news_announcement";



    public static final String PUSH_TYPE_ARTICLE = "article";
}
