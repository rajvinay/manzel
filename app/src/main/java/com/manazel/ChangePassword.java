package com.manazel;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.manazel.Constant.AES_Helper;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;

/**
 * Created by c119 on 15/12/16.
 */

public class ChangePassword extends FragmentActivity implements View.OnClickListener,WsResponseListener
{
    ImageView imgmenu,imgback,imgplus,imgedit;
    TextView txtheader;

    Button btnsave;
    EditText edtoldpass,edtnewpass,edtrepass;

    TinyDB tb;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepass);

        tb = new TinyDB(this);

        imgmenu = (ImageView)findViewById(R.id.img_menu);
        imgmenu.setVisibility(View.GONE);

        imgback = (ImageView)findViewById(R.id.img_back);
        imgback.setOnClickListener(this);
        imgback.setVisibility(View.VISIBLE);

        imgplus = (ImageView)findViewById(R.id.img_plus);
        imgplus.setVisibility(View.GONE);

        imgedit = (ImageView)findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView)findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(this));
        txtheader.setText(R.string.str_change_pass);

        edtoldpass = (EditText) findViewById(R.id.edt_oldpass);
        edtnewpass = (EditText) findViewById(R.id.edt_newpass);
        edtrepass = (EditText) findViewById(R.id.edt_repass);

        btnsave = (Button) findViewById(R.id.btn_save);
        btnsave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.btn_save:
                checkvalidation();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

    private void checkvalidation()
    {
        if(edtoldpass.getText().toString().length() > 0 && edtoldpass.getText().toString() != ""
                && edtoldpass.getText().toString() != " ")
        {
            if(edtnewpass.getText().toString().length() > 0 && edtnewpass.getText().toString() != ""
                    && edtnewpass.getText().toString() != " ")
            {
                if(edtrepass.getText().toString().length() > 0 && edtrepass.getText().toString() != ""
                        && edtrepass.getText().toString() != " ")
                {
                    if(edtnewpass.getText().toString().equalsIgnoreCase(edtrepass.getText().toString()))
                    {
                        callchangepassws();
                    }
                    else
                    {
                        AppGlobal.showToast(this,"ReEnter Password do not match!");
                    }
                }
                else
                {
                    AppGlobal.showToast(this,getString(R.string.reenter_pass));
                }
            }
            else
            {
                AppGlobal.showToast(this,getString(R.string.enter_new_pass));
            }
        }
        else
        {
            AppGlobal.showToast(this,getString(R.string.enter_old_pass));
        }
    }

    private void callchangepassws()
    {
        if(AppGlobal.isNetwork(this))
        {
            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(this,AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            try
            {
                new AsyncPostService(ChangePassword.this,getString(R.string.Please_wait), WsConstant.Req_CreateTicket,cm,true,true)
                        .execute(WsConstant.WS_CREATETICKET);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        else
        {
            AppGlobal.showToast(this,getString(R.string.str_no_internet));
        }
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error)
    {
        if(error == null)
        {
            if(serviceType.equalsIgnoreCase(WsConstant.Req_CreateTicket))
            {

            }

        }
    }
}
