package com.manazel.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.manazel.Constant.AES_Helper;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.DividerItemDecoration;
import com.manazel.MainActivity;
import com.manazel.NewsDetail;
import com.manazel.R;
import com.manazel.adapter.NewsAdapter;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.Newsdata;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.User;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by c119 on 26/11/16.
 */

public class NewsFragment extends Fragment implements View.OnClickListener, WsResponseListener, SwipyRefreshLayout.OnRefreshListener {
    ImageView imgmenu, imgback, imgplus, imgedit;
    TextView txtheader, txtfetachnews;

    RecyclerView recyclerView;
    NewsAdapter mAdapter;
    List<Newsdata> NewsList, tempNewsList;
    RecyclerView.LayoutManager mLayoutManager;

    TinyDB tb;
    User userobj;

    SwipyRefreshLayout swipyrefreshlayout;
    int increaseval = -1;
    int pagelimit;

    boolean loadingMore = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_news, container, false);

        increaseval = 0;
        pagelimit = 10;

        tb = new TinyDB(getActivity());
        NewsList = new ArrayList<>();

        Gson gson = new Gson();
        String json = tb.getString("userobj");
        userobj = gson.fromJson(json, User.class);

        swipyrefreshlayout = (SwipyRefreshLayout) v.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setOnRefreshListener(this);

        imgmenu = (ImageView) v.findViewById(R.id.img_menu);
        imgmenu.setOnClickListener(this);
        imgmenu.setVisibility(View.VISIBLE);

        imgback = (ImageView) v.findViewById(R.id.img_back);
        imgback.setVisibility(View.GONE);

        imgplus = (ImageView) v.findViewById(R.id.img_plus);
        imgplus.setVisibility(View.GONE);

        imgedit = (ImageView) v.findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView) v.findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(getActivity()));
        txtheader.setText(R.string.str_news);

        txtfetachnews = (TextView) v.findViewById(R.id.txt_fetachnews);
        txtfetachnews.setVisibility(View.VISIBLE);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        recyclerView.setVisibility(View.GONE);

        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        callnewsws(increaseval);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void callnewsws(int val) {
        if (AppGlobal.isNetwork(getActivity())) {
            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(getActivity(), AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setStart_record(String.valueOf(val));
            cm.setBatch_value(String.valueOf(pagelimit));
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            try {
                new AsyncPostService(getActivity(), getString(R.string.Please_wait), NewsFragment.this, WsConstant.Req_GetAllNews, cm, false, true)
                        .execute(WsConstant.WS_GETALLNEWS);
            } catch (Exception e) {
                e.printStackTrace();
            }

            loadingMore = true;
        } else {
            AppGlobal.showToast(getActivity(), getString(R.string.str_no_internet));
        }
    }

    public void callnewsdetail(Newsdata newsdetailobj) {
        Gson gson = new Gson();
        String json = gson.toJson(newsdetailobj);
        tb.putString("newsdetailobj", json);

        Intent i = new Intent(getActivity(), NewsDetail.class);
        getActivity().startActivity(i);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_menu:
                ((MainActivity) getActivity()).opendrawer();
                break;
        }
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error) {
        if (error == null) {
            if (swipyrefreshlayout.isRefreshing()) {
                swipyrefreshlayout.setRefreshing(false);
            }

            if (serviceType.equalsIgnoreCase(WsConstant.Req_GetAllNews)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    int size = ((ResponseResult) data).getArrnews().size();
                    if (size > 0) {
                        if (increaseval == 0) {
                            txtfetachnews.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        }

                        tempNewsList = ((ResponseResult) data).getArrnews();
                        NewsList.addAll(tempNewsList);

                        if (mAdapter != null) {
                            mAdapter.setNewsList(NewsList);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            mAdapter = new NewsAdapter(getActivity(), NewsList, NewsFragment.this);
                            recyclerView.setAdapter(mAdapter);
                        }

                        loadingMore = false;
                    } else {
                        loadingMore = false;
                        if (mAdapter == null) {
                            txtfetachnews.setText("No News Found!");
                        }
                    }
                } else {
                    loadingMore = false;
                }

            }

        } else {
            if (swipyrefreshlayout.isRefreshing()) {
                swipyrefreshlayout.setRefreshing(false);
            }

            loadingMore = false;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataReceived(String type) {

        if (type.equalsIgnoreCase(Newsdata.class.getName())) {

            Log.e("TAG", "refresh data here");
            Toast.makeText(getActivity(), "refresh data here", Toast.LENGTH_SHORT).show();
            increaseval = 0;
            pagelimit = 10;
            callnewsws(increaseval);
        }
    }


    @Override
    public void onStop() {
        super.onStop();

        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (AppGlobal.isNetwork(getActivity()) && loadingMore == false) {
                increaseval = increaseval + pagelimit;
                callnewsws(increaseval);
            }
        }
    }
}
