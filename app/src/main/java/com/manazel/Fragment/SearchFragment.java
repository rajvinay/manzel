package com.manazel.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.google.gson.Gson;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.MainActivity;
import com.manazel.R;
import com.manazel.Registration;
import com.manazel.adapter.BathRoomAdpForSearch;
import com.manazel.adapter.BedRoomAdpForSearch;
import com.manazel.adapter.CategoryAdpForSearchData;
import com.manazel.adapter.FilterOptionSearchResultAdp;
import com.manazel.adapter.LocationAdpForSearchData;
import com.manazel.adapter.PropertyContractAdpForSearchProperty;
import com.manazel.adapter.RentAdpForSearch;
import com.manazel.adapter.SearchDataAdp;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Area;
import com.manazel.modal.Bathroom;
import com.manazel.modal.Bedroom;
import com.manazel.modal.Category;
import com.manazel.modal.Common;
import com.manazel.modal.Contract;
import com.manazel.modal.Location;
import com.manazel.modal.Price;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.SearchProperty;
import com.manazel.modal.User;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;


import static android.R.attr.max;
import static android.R.attr.value;
import static com.google.android.gms.plus.PlusOneDummyView.TAG;

/**
 * Created by c217 on 22/05/17.
 */

public class SearchFragment extends Fragment implements View.OnClickListener, WsResponseListener {

    TinyDB tb;
    private TextView txtheader;
    private RelativeLayout top_main_Relative;
    private ImageView img_menu, img_Back, img_Filter;
    //    private TextView txt_PropertyContract, txt_PropertyLocation, txt_PropertyCategories,
//            txt_Bedrooms, txt_Bathrooms, txt_PriceGap, txt_AreaGap, txt_Search;
    private TextView txt_Search, txt_PropertyFilterOption;
    private RecyclerView rv_SearchedProperty;
    private LinearLayout input_top_Linear;
    ArrayList<String> rentTypeData = new ArrayList<>();
    ArrayList<Contract> contractSpinnerData = new ArrayList<>();
    ArrayList<Category> categoriesSpinnerData = new ArrayList<>();
    ArrayList<Location> locationSpinnerData = new ArrayList<>();
    ArrayList<Area> areaSpinnerData = new ArrayList<>();
    ArrayList<Price> priceSpinnerData = new ArrayList<>();
    ArrayList<Bedroom> bedroomSpinnerData = new ArrayList<>();
    ArrayList<Bathroom> bathroomSpinnerData = new ArrayList<>();
    ArrayList<SearchProperty> searchProperties = new ArrayList<>();
    ArrayList<String> optionSearchProperties = new ArrayList<>();
    int width, height;

    private RentAdpForSearch rentAdp;
    private PropertyContractAdpForSearchProperty propertyContractAdpForSearchProperty;
    private CategoryAdpForSearchData categoryAdpForSearchData;
    private LocationAdpForSearchData locationAdpForSearchData;
    private BedRoomAdpForSearch bedRoomAdpForSearch;
    private BathRoomAdpForSearch bathRoomAdpForSearch;
    private SearchDataAdp searchDataAdp;
    private ListSearchAdp searchAdp;
    private FilterOptionSearchResultAdp filterOptionSearchResultAdp;
    //    private MultiSlider price_slider5, area_slider5;
    private User userobj;
    private LinearLayoutManager linearLayoutManager;
    private ListView main_List_Search;
    ArrayList<String> items = new ArrayList<>(10);
    int areaFrom, areaTo, priceFrom, priceTo;
    String keywordValue = "";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_search, container, false);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;
        height = displaymetrics.heightPixels;
        tb = new TinyDB(getActivity());
        txtheader = (TextView) v.findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(getActivity()));
        txtheader.setText(R.string.search_properties_title);


        img_Filter = (ImageView) v.findViewById(R.id.img_Filter);
        img_Back = (ImageView) v.findViewById(R.id.img_back);
        img_Back.setImageResource(R.drawable.icon_back);
        img_Back.setVisibility(View.VISIBLE);
        img_menu = (ImageView) v.findViewById(R.id.img_menu);
        img_menu.setVisibility(View.GONE);
        img_menu.setOnClickListener(this);
        img_Filter.setOnClickListener(this);
        img_Back.setOnClickListener(this);

        setupControls(v);
        initGlobal();

        return v;
    }

    private void setupControls(View view) {
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        top_main_Relative = (RelativeLayout) view.findViewById(R.id.top_main_Relative);
        main_List_Search = (ListView) view.findViewById(R.id.main_List_Search);

        txt_Search = (TextView) view.findViewById(R.id.txt_Search);
        txt_PropertyFilterOption = (TextView) view.findViewById(R.id.txt_PropertyFilterOption);
        rv_SearchedProperty = (RecyclerView) view.findViewById(R.id.rv_SearchedProperty);
        input_top_Linear = (LinearLayout) view.findViewById(R.id.input_top_Linear);

        rentAdp = new RentAdpForSearch(getActivity(), rentTypeData);
        filterOptionSearchResultAdp = new FilterOptionSearchResultAdp(getActivity(), optionSearchProperties);
        propertyContractAdpForSearchProperty = new PropertyContractAdpForSearchProperty(getActivity(), contractSpinnerData);
        locationAdpForSearchData = new LocationAdpForSearchData(getActivity(), locationSpinnerData);
        categoryAdpForSearchData = new CategoryAdpForSearchData(getActivity(), categoriesSpinnerData);
        bedRoomAdpForSearch = new BedRoomAdpForSearch(getActivity(), bedroomSpinnerData);
        bathRoomAdpForSearch = new BathRoomAdpForSearch(getActivity(), bathroomSpinnerData);
        searchDataAdp = new SearchDataAdp(getActivity(), (int) (width / 1.028), (int) (height / 1.5), searchProperties);

        callPropertyDropDownDataWS();

        Gson gson = new Gson();
        String json = tb.getString("userobj");
        userobj = gson.fromJson(json, User.class);
        rv_SearchedProperty.setLayoutManager(linearLayoutManager);
        rv_SearchedProperty.setAdapter(searchDataAdp);
        rv_SearchedProperty.setVisibility(View.GONE);
        input_top_Linear.setVisibility(View.GONE);
        img_Filter.setVisibility(View.GONE);


        searchAdp = new ListSearchAdp(getActivity(), items);
        items.clear();
        items.add(0, "Property Contract");
        items.add(1, "Property Location");
        items.add(2, "Property Category");
        items.add(3, "Bedrooms");
        items.add(4, "Bathrooms");
        items.add(5, "Keyword");
        items.add(6, "Price");
        items.add(7, "Area");
        items.add(8, "");
        main_List_Search.setAdapter(searchAdp);

        rentTypeData.clear();
        rentTypeData.add("Rent Slot");
        rentTypeData.add("Yearly");
        rentTypeData.add("Monthly");
        rentTypeData.add("Weekly");
        rentTypeData.add("Daily");

        optionSearchProperties.clear();
        optionSearchProperties.add("Lowest Price");
        optionSearchProperties.add("Highest Price");
        optionSearchProperties.add("Newest");
        optionSearchProperties.add("Lowest Beds");
        optionSearchProperties.add("Highest Beds");
        filterOptionSearchResultAdp.notifyDataSetChanged();
        txt_PropertyFilterOption.setText(optionSearchProperties.get(2));

    }

    public class ListSearchAdp extends BaseAdapter {

        Context mContext;
        ArrayList<String> items = new ArrayList<>();
        private LayoutInflater layoutInflater;

        public ListSearchAdp(Context context, ArrayList<String> items) {
            mContext = context;
            this.items = items;
            layoutInflater = LayoutInflater.from(context);

        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }


        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return 0;
            } else if (position == 1) {
                return 0;
            } else if (position == 2) {
                return 0;
            } else if (position == 3) {
                return 0;
            } else if (position == 4) {
                return 0;
            } else if (position == 5) {
                return 0;
            } else if (position == 6) {
                return 0;
            } else if (position == 7) {
                return 1;
            } else if (position == 8) {
                return 1;
            } else {
                return -1;
            }

        }

        public class ViewHolder {

            private TextView txt_PropertyContract, txt_RangeTitle, txt_PriceGap;
            private EditText et_Keyword;
            private ImageView iv_Triangle;
            private CrystalRangeSeekbar slider5;
            private LinearLayout input_main_Linear;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            int type = getItemViewType(position);

            if (convertView == null) {
                holder = new ViewHolder();

                if (type == 0) {
                    convertView = layoutInflater.inflate(R.layout.row_search_input, null);
                    holder.txt_PropertyContract = (TextView) convertView.findViewById(R.id.txt_PropertyContract);
                    holder.et_Keyword = (EditText) convertView.findViewById(R.id.et_Keyword);
                    holder.iv_Triangle = (ImageView) convertView.findViewById(R.id.iv_Triangle);
                    holder.input_main_Linear = (LinearLayout) convertView.findViewById(R.id.input_main_Linear);
                    holder.iv_Triangle.setTag(position);
                    holder.et_Keyword.setTag(position);
                    holder.txt_PropertyContract.setTag(position);

                } else {
                    convertView = layoutInflater.inflate(R.layout.row_search_range, null);
                    holder.txt_RangeTitle = (TextView) convertView.findViewById(R.id.txt_RangeTitle);
                    holder.txt_PriceGap = (TextView) convertView.findViewById(R.id.txt_PriceGap);
                    holder.slider5 = (CrystalRangeSeekbar) convertView.findViewById(R.id.slider5);
                }
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            if (type == 0) {
                switch (position) {
                    case 0:
                        holder.txt_PropertyContract.setText(items.get(0));
                        holder.et_Keyword.setVisibility(View.GONE);
                        holder.txt_PropertyContract.setVisibility(View.VISIBLE);
                        holder.iv_Triangle.setVisibility(View.VISIBLE);
                        holder.input_main_Linear.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showPopup_to_Select_Option(getActivity(), 0, holder.txt_PropertyContract);
                            }
                        });
                        holder.input_main_Linear.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        if (items.get(8).equals("")) {
                            holder.input_main_Linear.setVisibility(View.GONE);
                        } else {
                            holder.txt_PropertyContract.setText("Rent type");
                            holder.input_main_Linear.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    showPopup_to_Select_Option(getActivity(), 8, holder.txt_PropertyContract);
                                }
                            });
                            holder.iv_Triangle.setVisibility(View.VISIBLE);
                            holder.input_main_Linear.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 2:
                        holder.txt_PropertyContract.setText(items.get(1));
                        holder.et_Keyword.setVisibility(View.GONE);
                        holder.iv_Triangle.setVisibility(View.VISIBLE);
                        holder.txt_PropertyContract.setVisibility(View.VISIBLE);
                        holder.input_main_Linear.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showPopup_to_Select_Option(getActivity(), 1, holder.txt_PropertyContract);
                            }
                        });
                        holder.input_main_Linear.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        holder.txt_PropertyContract.setText(items.get(2));
                        holder.et_Keyword.setVisibility(View.GONE);
                        holder.iv_Triangle.setVisibility(View.VISIBLE);
                        holder.txt_PropertyContract.setVisibility(View.VISIBLE);
                        holder.input_main_Linear.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showPopup_to_Select_Option(getActivity(), 2, holder.txt_PropertyContract);
                            }
                        });
                        holder.input_main_Linear.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        holder.txt_PropertyContract.setText(items.get(3));
                        holder.et_Keyword.setVisibility(View.GONE);
                        holder.txt_PropertyContract.setVisibility(View.VISIBLE);
                        holder.iv_Triangle.setVisibility(View.VISIBLE);
                        holder.input_main_Linear.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showPopup_to_Select_Option(getActivity(), 3, holder.txt_PropertyContract);
                            }
                        });
                        holder.input_main_Linear.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        holder.txt_PropertyContract.setText(items.get(4));
                        holder.et_Keyword.setVisibility(View.GONE);
                        holder.iv_Triangle.setVisibility(View.VISIBLE);
                        holder.txt_PropertyContract.setVisibility(View.VISIBLE);
                        holder.input_main_Linear.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showPopup_to_Select_Option(getActivity(), 4, holder.txt_PropertyContract);
                            }
                        });
                        holder.input_main_Linear.setVisibility(View.VISIBLE);
                        break;
                    case 6:
                        holder.et_Keyword.setVisibility(View.VISIBLE);
                        holder.txt_PropertyContract.setVisibility(View.GONE);
                        holder.iv_Triangle.setVisibility(View.GONE);
                        if (keywordValue.equals("")) {
                            holder.et_Keyword.setHint("Keyword");
                        } else {
                            holder.et_Keyword.setText(keywordValue);
                        }
                        holder.input_main_Linear.setVisibility(View.VISIBLE);
                        holder.et_Keyword.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                keywordValue = String.valueOf(s);
                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        });
                        break;

                }
            } else {
                switch (position) {
                    case 7:
                        holder.txt_RangeTitle.setText("Price");
                        if (priceSpinnerData.size() > 0) {
                            holder.slider5.setMinValue(Integer.parseInt(priceSpinnerData.get(0).getMinPrice()));
                            holder.slider5.setMaxValue(Integer.parseInt(priceSpinnerData.get(0).getMaxPrice()));
                        }
                        holder.txt_PriceGap.setText("$ " + String.valueOf(holder.slider5.getSelectedMinValue()) + " - $ " + String.valueOf(holder.slider5.getSelectedMaxValue()));
                        holder.slider5.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
                            @Override
                            public void valueChanged(Number minValue, Number maxValue) {
                                priceFrom = Integer.parseInt(String.valueOf(minValue));
                                priceTo = (Integer.parseInt(String.valueOf(maxValue)));
                                holder.txt_PriceGap.setText("$ " + String.valueOf(minValue) + " - $ " + String.valueOf(maxValue));
                            }
                        });
                        break;
                    case 8:
                        holder.txt_RangeTitle.setText("Area");
                        if (areaSpinnerData.size() > 0) {
                            holder.slider5.setMinValue(Integer.parseInt(areaSpinnerData.get(0).getMinArea()));
                            holder.slider5.setMaxValue(Integer.parseInt(areaSpinnerData.get(0).getMaxArea()));
                        }
                        holder.txt_PriceGap.setText(String.valueOf(holder.slider5.getSelectedMinValue()) + " -  " + String.valueOf(holder.slider5.getSelectedMaxValue() + " SqFt"));
                        holder.slider5.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
                            @Override
                            public void valueChanged(Number minValue, Number maxValue) {
                                holder.txt_PriceGap.setText(String.valueOf(minValue) + " - " + String.valueOf(maxValue + " SqFt"));
                                areaFrom = Integer.parseInt(String.valueOf(minValue));
                                areaTo = Integer.parseInt(String.valueOf(maxValue));
                            }
                        });
                        break;
                }
            }
            return convertView;
        }

    }

    private void initGlobal() {

        makeAdpOfBedroomAndBathroomReady();
        txt_Search.setOnClickListener(this);
        input_top_Linear.setOnClickListener(this);

//
//        txt_PropertyContract.setOnClickListener(this);
//        txt_PropertyLocation.setOnClickListener(this);
//        txt_PropertyCategories.setOnClickListener(this);
//        txt_Bedrooms.setOnClickListener(this);
//        txt_Bathrooms.setOnClickListener(this);
//        price_slider5.setOnThumbValueChangeListener(this);
//        area_slider5.setOnThumbValueChangeListener(this);
    }

    private void makeAdpOfBedroomAndBathroomReady() {
        Bathroom bathroom;
        Bedroom bedroom;
        bedroomSpinnerData.clear();
        bathroomSpinnerData.clear();
        for (int i = 0; i < 6; i++) {
            bedroom = new Bedroom();
            bathroom = new Bathroom();
            if (i == 0) {
                bedroom.setBedroom_Count("0");
                bedroom.setBedroom_Count_Name("Bedrooms");
                bathroom.setBathroom_Count("0");
                bathroom.setBathroom_Count_Name("Bathrooms");
            } else {
                bedroom.setBedroom_Count(String.valueOf(i + 1));
                bathroom.setBathroom_Count(String.valueOf(i + 1));
                if (i == 5) {
                    bedroom.setBedroom_Count_Name("Bedrooms 5 or more");
                    bathroom.setBathroom_Count_Name("Bathrooms 5 or more");
                } else {
                    bedroom.setBedroom_Count_Name("Bedrooms at least " + String.valueOf(i + 1));
                    bathroom.setBathroom_Count_Name("Bathrooms at least " + String.valueOf(i + 1));
                }
            }
            bedroomSpinnerData.add(i, bedroom);
            bathroomSpinnerData.add(i, bathroom);
            bedRoomAdpForSearch.notifyDataSetChanged();
            bathRoomAdpForSearch.notifyDataSetChanged();
        }
    }

    private void callPropertyDropDownDataWS() {
        if (AppGlobal.isNetwork(getActivity())) {

            Common cm = new Common();

            try {
                new AsyncPostService(getActivity(), getString(R.string.Please_wait), SearchFragment.this, WsConstant.Req_Get_All_Property_For_Search_Dropdown,
                        cm, false, true).execute(WsConstant.WS_GET_ALL_PROPERTY_FOR_SEARCH_DROPDOWN);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            AppGlobal.showToast(getActivity(), getString(R.string.str_no_internet));
        }
    }

    @Override
    public void onStart() {
        super.onStart();

//        if (!EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().register(this);
//        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.input_top_Linear:

                break;
            case R.id.img_Filter:
                showPopup_to_Select_Option(getActivity(), 10, txt_PropertyFilterOption);
                break;
            case R.id.img_menu:
                ((MainActivity) getActivity()).opendrawer();
                break;
            case R.id.img_back:
                if (rv_SearchedProperty.getVisibility() == View.VISIBLE && main_List_Search.getVisibility() == View.GONE) {
                    top_main_Relative.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.search_bg_color));
                    rv_SearchedProperty.setVisibility(View.GONE);
                    img_Filter.setVisibility(View.GONE);
                    input_top_Linear.setVisibility(View.GONE);
                    main_List_Search.setVisibility(View.VISIBLE);
                    txt_Search.setVisibility(View.VISIBLE);
                    txtheader.setText(R.string.search_properties_title);
                } else {
                    callpropertyfrag();
                }
                break;

            case R.id.txt_Search:
                if (items.get(8).equals("")) {
                    searchData("Yearly", optionSearchProperties.get(2));
                } else {
                    searchData(items.get(8), optionSearchProperties.get(2));
                }
                break;

        }
    }

    private void callpropertyfrag() {
        Fragment fragment = new PropertyFragment();
        FragmentTransaction transaction = ((MainActivity) getActivity()).getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment, "PropertyFragment");
        transaction.commit();
        ((MainActivity) getActivity()).getSupportFragmentManager().executePendingTransactions();
    }

    private void searchData(String rentType, String sortingValue) {
        if (AppGlobal.isNetwork(getActivity())) {

            Common cm = new Common();

            cm.setProperty_category_id(propertyContractAdpForSearchProperty.getSelectedDataId(items.get(0)));
//            cm.setProperty_category_id("3");
            cm.setProperty_type_id(categoryAdpForSearchData.getSelectedDataId(items.get(2)));
//            cm.setProperty_type_id("1");
            cm.setLocation(items.get(1));
//            cm.setLocation("Dubai");
            cm.setRent_type(rentType);
            cm.setBedroom_no(bedRoomAdpForSearch.getSelectedDataId(items.get(3)));
//            cm.setBedroom_no("0");
            cm.setBathroom_no(bathRoomAdpForSearch.getSelectedDataId(items.get(4)));
//            cm.setBathroom_no("0");
            cm.setArea_from(String.valueOf(areaFrom));
//            cm.setArea_from("0");
            cm.setArea_to(String.valueOf(areaTo));
//            cm.setArea_to("5019");
            cm.setPrice_from(String.valueOf(priceFrom));
//            cm.setPrice_from("3995000");
            cm.setPrice_to(String.valueOf(priceTo));
//            cm.setPrice_to("5995000");
            cm.setKeyword(keywordValue);
//            cm.setKeyword("Pool");
            cm.setStart_limit("0");
            cm.setEnd_limit("10");
            cm.setUser_id(userobj.getId());
            cm.setSortedBy(sortingValue);
//            cm.setUser_id("91");


            try {
                new AsyncPostService(getActivity(), "Searching", SearchFragment.this, WsConstant.Req_Search_Property,
                        cm, true, true).execute(WsConstant.WS_SEARCH_PROPERTY);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            AppGlobal.showToast(getActivity(), getString(R.string.str_no_internet));
        }
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error) {
        if (error == null) {
            if (serviceType.equalsIgnoreCase(WsConstant.Req_Get_All_Property_For_Search_Dropdown)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    int size = ((ResponseResult) data).getProperty().getContracts().size();
                    if (size > 0) {
                        contractSpinnerData.clear();
                        Contract contract = new Contract();
                        contract.setStatus("Active");
                        contract.setPropertyCategoryId("0");
                        contract.setPropertyCategoryName("Property Contract");
                        contractSpinnerData.add(contract);
                        contractSpinnerData.addAll(((ResponseResult) data).getProperty().getContracts());
                        propertyContractAdpForSearchProperty.notifyDataSetChanged();
                    }
                    size = ((ResponseResult) data).getProperty().getCategories().size();
                    if (size > 0) {
                        categoriesSpinnerData.clear();
                        Category category = new Category();
                        category.setStatus("Active");
                        category.setPropertyTypeId("0");
                        category.setPropertyTypeName("Property Category");
                        categoriesSpinnerData.add(category);
                        categoriesSpinnerData.addAll(((ResponseResult) data).getProperty().getCategories());
                        categoryAdpForSearchData.notifyDataSetChanged();
                    }
                    size = ((ResponseResult) data).getProperty().getLocations().size();
                    if (size > 0) {
                        locationSpinnerData.clear();
                        Location location = new Location();
                        location.setCity("Property Location");
                        location.setCount("0");
                        locationSpinnerData.add(location);
                        locationSpinnerData.addAll(((ResponseResult) data).getProperty().getLocations());
                        locationAdpForSearchData.notifyDataSetChanged();

                    }
                    size = ((ResponseResult) data).getProperty().getAreas().size();
                    if (size > 0) {
                        areaSpinnerData.clear();
                        areaSpinnerData.addAll(((ResponseResult) data).getProperty().getAreas());
                        areaFrom = Integer.parseInt(areaSpinnerData.get(0).getMinArea());
                        areaTo = Integer.parseInt(areaSpinnerData.get(0).getMaxArea());
                    }
                    size = ((ResponseResult) data).getProperty().getPrices().size();
                    if (size > 0) {
                        priceSpinnerData.clear();
                        priceSpinnerData.addAll(((ResponseResult) data).getProperty().getPrices());
                        priceFrom = Integer.parseInt(priceSpinnerData.get(0).getMinPrice());
                        priceTo = Integer.parseInt(priceSpinnerData.get(0).getMaxPrice());
                    }
                    searchAdp.notifyDataSetChanged();
                } else {
                }

            } else if (serviceType.equalsIgnoreCase(WsConstant.Req_Search_Property)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    int size = ((ResponseResult) data).getSearchProperties().size();
                    Log.e(TAG, "onDelieverResponse: SIZE ::: " + size);
                    if (size == 0) {
                        show_Dialogue_for_No_Property_Found(R.string.no_property_found);
                    } else {
                        searchProperties.clear();
                        searchProperties.addAll(((ResponseResult) data).getSearchProperties());
                        searchDataAdp.notifyDataSetChanged();
                        changeViewToShowSearchData(searchProperties.size());
                    }
                } else {
                    show_Dialogue_for_No_Property_Found(R.string.no_property_found);
                }
            }

        }
    }

    private void changeViewToShowSearchData(int size) {
        if (size > 0) {
            top_main_Relative.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.search_bg_color_for_rv));
            rv_SearchedProperty.setVisibility(View.VISIBLE);
            img_Filter.setVisibility(View.VISIBLE);
//            input_top_Linear.setVisibility(View.VISIBLE);
            main_List_Search.setVisibility(View.GONE);
            txtheader.setText(R.string.drawer_properties);
            txt_Search.setVisibility(View.GONE);
        } else {
            top_main_Relative.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.search_bg_color));
            rv_SearchedProperty.setVisibility(View.GONE);
            img_Filter.setVisibility(View.GONE);
            input_top_Linear.setVisibility(View.GONE);
            main_List_Search.setVisibility(View.VISIBLE);
            txtheader.setText(R.string.search_properties_title);
            txt_Search.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataReceived(String type) {

    }

    @Override
    public void onStop() {
        super.onStop();
//
//        if (EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().unregister(this);
//        }
    }

    private void showPopup_to_Select_Option(final Activity context, final int count, final TextView textView) {

        final Dialog dialog = new Dialog(context);
        set_DialougfeParam(dialog);

        final ListView lv_Filter = (ListView) dialog.findViewById(R.id.lv_Filter);
        lv_Filter.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        switch (count) {
            case 0:
                lv_Filter.setAdapter(propertyContractAdpForSearchProperty);
                lv_Filter.setItemChecked(propertyContractAdpForSearchProperty.getIndexByProperty(items.get(0)), true);
//                String[] temp = new String[]{"list 1", "list 2", "list 3"};
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_single_choice, temp);
//                lv_Filter.setAdapter(adapter);
                break;
            case 1:
                lv_Filter.setAdapter(locationAdpForSearchData);
                lv_Filter.setItemChecked(locationAdpForSearchData.getIndexByProperty(items.get(1)), true);
                break;
            case 2:
                lv_Filter.setAdapter(categoryAdpForSearchData);
                lv_Filter.setItemChecked(categoryAdpForSearchData.getIndexByProperty(items.get(2)), true);
                break;
            case 3:
                lv_Filter.setAdapter(bedRoomAdpForSearch);
                lv_Filter.setItemChecked(bedRoomAdpForSearch.getIndexByProperty(items.get(3)), true);
                break;
            case 4:
                lv_Filter.setAdapter(bathRoomAdpForSearch);
                lv_Filter.setItemChecked(bathRoomAdpForSearch.getIndexByProperty(items.get(4)), true);
                break;
            case 8:
                lv_Filter.setAdapter(rentAdp);
                lv_Filter.setItemChecked(rentAdp.getIndexByProperty(items.get(8)), true);
                break;
            case 10:
                lv_Filter.setAdapter(filterOptionSearchResultAdp);
                lv_Filter.setItemChecked(filterOptionSearchResultAdp.getIndexByProperty(txt_PropertyFilterOption.getText().toString()), true);
                break;
        }


        lv_Filter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
//                lv_Filter.setItemChecked(position, true);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        switch (count) {
                            case 0:
                                textView.setText(propertyContractAdpForSearchProperty.getValueforPosition(position));
                                if (position == 2) {
                                    items.set(8, "Rent Slot");
                                    items.set(0, propertyContractAdpForSearchProperty.getValueforPosition(position));
                                    searchAdp.notifyDataSetChanged();
                                } else {
                                    items.set(8, "");
                                    items.set(0, propertyContractAdpForSearchProperty.getValueforPosition(position));
                                    searchAdp.notifyDataSetChanged();
                                }
                                break;
                            case 1:
                                textView.setText(locationAdpForSearchData.getValueforPosition(position));
                                items.set(1, locationAdpForSearchData.getValueforPosition(position));
                                break;
                            case 2:
                                textView.setText(categoryAdpForSearchData.getValueforPosition(position));
                                items.set(2, categoryAdpForSearchData.getValueforPosition(position));
                                break;
                            case 3:
                                textView.setText(bedRoomAdpForSearch.getValueforPosition(position));
                                items.set(3, bedRoomAdpForSearch.getValueforPosition(position));
                                break;
                            case 4:
                                textView.setText(bathRoomAdpForSearch.getValueforPosition(position));
                                items.set(4, bathRoomAdpForSearch.getValueforPosition(position));
                                break;
                            case 8:
                                textView.setText(rentTypeData.get(position));
                                items.set(8, rentTypeData.get(position));
                                break;
                            case 10:
                                textView.setText(optionSearchProperties.get(position));
                                if (items.get(8).equals("")) {
                                    searchData("Yearly", optionSearchProperties.get(position));
                                } else {
                                    searchData(items.get(8), optionSearchProperties.get(position));
                                }
                                break;
                        }
                    }
                }, 500);
            }
        });
    }

    public void set_DialougfeParam(Dialog dialog) {
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.search_option);
        dialog.getWindow().getAttributes().windowAnimations = R.style.animationName;
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = (int) (width * 1.01);
        lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }


    public void show_Dialogue_for_No_Property_Found(int msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().getAttributes().windowAnimations = R.style.animationName;
        dialog.show();

    }
}
