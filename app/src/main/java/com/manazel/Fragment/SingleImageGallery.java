package com.manazel.Fragment;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;
import com.manazel.Constant.WsConstant;
import com.manazel.R;
import com.squareup.picasso.Picasso;

import static com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.TAG;

/**
 * Created by c217 on 29/05/17.
 */
@SuppressLint("ValidFragment")
public class SingleImageGallery extends Fragment {

    private PhotoView giv_Gallery;
    String img_url;
    int fragVal;

    public SingleImageGallery() {
    }

    public SingleImageGallery(String img_url, int val) {
        // Required empty public constructor
        this.img_url = img_url;
        init(val);
    }

    public SingleImageGallery init(int val) {
        SingleImageGallery singleImageGallery = new SingleImageGallery();
        Bundle args = new Bundle();
        args.putInt("val", val);
        singleImageGallery.setArguments(args);

        return singleImageGallery;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragVal = getArguments() != null ? getArguments().getInt("val") : 1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_single_image_gallery, container, false);

        setupControls(view);
        initGlobal();
        return view;
    }



    private void initGlobal() {
        Picasso.with(getActivity())
                .load(WsConstant.GALLERY_IMAGE_HOST_URL + img_url)
                .priority(Picasso.Priority.HIGH)
                .into(giv_Gallery);
    }

    private void setupControls(View view) {
        giv_Gallery = (PhotoView) view.findViewById(R.id.giv_Gallery);
    }

}