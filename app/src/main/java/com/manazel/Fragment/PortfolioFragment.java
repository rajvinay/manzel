package com.manazel.Fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.MainActivity;
import com.manazel.R;
import com.manazel.adapter.RecentPropertiesAdp;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.User;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;

/**
 * Created by c217 on 25/05/17.
 */

public class PortfolioFragment extends Fragment implements View.OnClickListener {

    TinyDB tb;
    RecentPropertiesAdp recent_adapter;
    int width, height;
    private ImageView imgmenu, img_Search;
    private TextView txt_header, txt_FetchData;

    private WebView webView;
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_portfolio, container, false);

        tb = new TinyDB(getActivity());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;
        height = displaymetrics.heightPixels;

        imgmenu = (ImageView) v.findViewById(R.id.img_menu);
        img_Search = (ImageView) v.findViewById(R.id.img_Search);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        imgmenu.setOnClickListener(this);
        imgmenu.setVisibility(View.VISIBLE);
        img_Search.setVisibility(View.GONE);


        txt_FetchData = (TextView) v.findViewById(R.id.txt_FetchData);
        txt_header = (TextView) v.findViewById(R.id.txt_header);
        txt_header.setText(R.string.drawer_portfolio);



        webView = (WebView) v.findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setVisibility(View.INVISIBLE);
        txt_FetchData.setVisibility(View.VISIBLE);
        webView.loadUrl(WsConstant.URL_PORTFOLIO);

        initGlobal();

        return v;
    }

    private void initGlobal() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
//                invalidateOptionsMenu();
//                webView.setVisibility(View.INVISIBLE);
//                txt_FetchData.setVisibility(View.VISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                webView.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
//                invalidateOptionsMenu();
                webView.setVisibility(View.VISIBLE);
                txt_FetchData.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                progressBar.setVisibility(View.GONE);
//                invalidateOptionsMenu();
                txt_FetchData.setText("Load Portfolio Failed");
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

//        if (!EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().register(this);
//        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_menu:
                ((MainActivity) getActivity()).opendrawer();
                break;
            case R.id.img_Search:
                break;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataReceived(String type) {


    }


    @Override
    public void onStop() {
        super.onStop();
//
//        if (EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().unregister(this);
//        }
    }

}
