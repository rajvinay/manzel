package com.manazel.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.manazel.Constant.AES_Helper;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.MainActivity;
import com.manazel.R;
import com.manazel.adapter.FaqExpandableListAdp;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by c119 on 26/11/16.
 */

public class FAQFragment extends Fragment implements View.OnClickListener,WsResponseListener
{
    ImageView imgmenu,imgback,imgplus,imgedit;
    TextView txtheader,txtfetachfaq;

    ExpandableListView expListView;

    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    FaqExpandableListAdp listAdapter;

    TinyDB tb;
    User userobj;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.frag_faq, container, false);

        tb = new TinyDB(getActivity());

        Gson gson = new Gson();
        String json = tb.getString("userobj");
        userobj = gson.fromJson(json,User.class);

        imgmenu = (ImageView)v.findViewById(R.id.img_menu);
        imgmenu.setOnClickListener(this);
        imgmenu.setVisibility(View.VISIBLE);

        imgback = (ImageView)v.findViewById(R.id.img_back);
        imgback.setVisibility(View.GONE);

        imgplus = (ImageView)v.findViewById(R.id.img_plus);
        imgplus.setVisibility(View.GONE);

        imgedit = (ImageView)v.findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView)v.findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(getActivity()));
        txtheader.setText(R.string.faq);

        txtfetachfaq = (TextView)v.findViewById(R.id.txt_fetachfaq);
        txtfetachfaq.setVisibility(View.VISIBLE);
        expListView = (ExpandableListView)v.findViewById(R.id.faqExplist);
        expListView.setGroupIndicator(null);
        expListView.setVisibility(View.GONE);

        callFaqWs();

        return v;
    }

    private void callFaqWs()
    {
        if(AppGlobal.isNetwork(getActivity()))
        {
            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(getActivity(),AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            try
            {
                new AsyncPostService(getActivity(),getString(R.string.Please_wait),FAQFragment.this,WsConstant.Req_GetAllFAQ,cm,false,true)
                        .execute(WsConstant.WS_GetAllFAQ);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            AppGlobal.showToast(getActivity(),getString(R.string.str_no_internet));
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_menu:
                ((MainActivity)getActivity()).opendrawer();
                break;
        }
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error)
    {
        if(error == null)
        {
            if(serviceType.equalsIgnoreCase(WsConstant.Req_GetAllFAQ))
            {
                String status = ((ResponseResult) data).getStatus();
                if(status.equalsIgnoreCase("SUCCESS"))
                {
                    int size = ((ResponseResult) data).getArrfaqdata().size();
                    if(size > 0)
                    {
                        txtfetachfaq.setVisibility(View.GONE);
                        expListView.setVisibility(View.VISIBLE);

                        listDataHeader = new ArrayList<String>();
                        listDataHeader.clear();
                        listDataChild = new HashMap<String, List<String>>();
                        listDataChild.clear();

                        for (int i = 0; i < size; i++)
                        {
                            ArrayList<String> arrtemp = new ArrayList<String>();
                            arrtemp.add(((ResponseResult) data).getArrfaqdata().get(i).getAnswers());
                            String headertxt = ((ResponseResult) data).getArrfaqdata().get(i).getQuestions();

                            listDataHeader.add(headertxt);
                            listDataChild.put(headertxt, arrtemp);
                        }

                        listAdapter = new FaqExpandableListAdp(getActivity(),listDataHeader,listDataChild);

                        expListView.setAdapter(listAdapter);
                    }
                    else
                    {
                        if(listAdapter == null)
                        {
                            txtfetachfaq.setText("No Faq Found!");
                        }
                    }
                }

            }

        }
    }
}
