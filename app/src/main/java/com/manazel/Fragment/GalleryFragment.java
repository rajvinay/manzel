package com.manazel.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.MainActivity;
import com.manazel.R;
import com.manazel.adapter.GalleryAdp;
import com.manazel.adapter.RecentPropertiesAdp;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.User;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;



/**
 * Created by c217 on 25/05/17.
 */

public class GalleryFragment extends Fragment implements View.OnClickListener, WsResponseListener {

    TinyDB tb;
    int width, height;
    private ImageView imgmenu, img_Search;
    private TextView txt_header, txt_FetchData;
    private RecyclerView rv_Gallery;
    private GridLayoutManager gridLayoutManager;
    private GalleryAdp adp;
    private ArrayList<String> imgs = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_gallery, container, false);

        tb = new TinyDB(getActivity());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;
        height = displaymetrics.heightPixels;

        setupControls(v);

        initGlobal();

        callGalleryDataWS();

        return v;
    }

    private void initGlobal() {

        imgmenu.setVisibility(View.VISIBLE);
        img_Search.setVisibility(View.GONE);
        txt_header.setText(R.string.drawer_gallery);

        rv_Gallery.setVisibility(View.GONE);
        txt_FetchData.setVisibility(View.VISIBLE);
        imgmenu.setOnClickListener(this);
    }

    private void setupControls(View v) {
        imgmenu = (ImageView) v.findViewById(R.id.img_menu);
        img_Search = (ImageView) v.findViewById(R.id.img_Search);
        rv_Gallery = (RecyclerView) v.findViewById(R.id.rv_Gallery);
        txt_FetchData = (TextView) v.findViewById(R.id.txt_FetchData);
        txt_header = (TextView) v.findViewById(R.id.txt_header);
        gridLayoutManager = new GridLayoutManager(getActivity(), 3, LinearLayoutManager.VERTICAL, false);
        adp = new GalleryAdp(getActivity(), imgs, width / 3, (int) (height / 4.95));
        rv_Gallery.setLayoutManager(gridLayoutManager);
        rv_Gallery.setAdapter(adp);
    }

    @Override
    public void onStart() {
        super.onStart();

//        if (!EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().register(this);
//        }
    }

    private void callGalleryDataWS() {
        if (AppGlobal.isNetwork(getActivity())) {

            Common cm = new Common();

            cm.setSection(tb.getString(AppConstant.PREF_GALLERY_SECTION_VALUE));

            try {
                new AsyncPostService(getActivity(), getString(R.string.Please_wait), GalleryFragment.this, WsConstant.Req_Get_All_Gallery_Photos,
                        cm, false, true).execute(WsConstant.WS_GET_ALL_GALLERY_PHOTOS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            AppGlobal.showToast(getActivity(), getString(R.string.str_no_internet));
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_menu:
                ((MainActivity) getActivity()).opendrawer();
                break;
            case R.id.img_Search:
                break;
        }
    }


    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error) {
        if (error == null) {
            if (serviceType.equalsIgnoreCase(WsConstant.Req_Get_All_Gallery_Photos)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    int size = ((ResponseResult) data).getSection().size();
                    imgs.clear();

                    if (size > 0) {
//                        for (int i = 0; i < size; i++) {
//                            imgs.add(i,((ResponseResult) data).getSection().get(i));
//                        }
                        imgs.addAll(((ResponseResult) data).getSection());
                        adp.notifyDataSetChanged();
                        txt_FetchData.setVisibility(View.GONE);
                        rv_Gallery.setVisibility(View.VISIBLE);
                    } else {

                    }
                } else {
                    txt_FetchData.setText(((ResponseResult) data).getMessage());
                }

            }

        } else {
            txt_FetchData.setText("Error!");
        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataReceived(String type) {


    }


    @Override
    public void onStop() {
        super.onStop();
//
//        if (EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().unregister(this);
//        }
    }

}
