package com.manazel.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.manazel.Constant.AppGlobal;
import com.manazel.MainActivity;
import com.manazel.R;

/**
 * Created by c119 on 29/11/16.
 */

public class CommunityFragment extends Fragment implements View.OnClickListener
{
    ImageView imgmenu,imgback,imgplus,imgedit;
    TextView txtheader;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.frag_community, container, false);

        imgmenu = (ImageView)v.findViewById(R.id.img_menu);
        imgmenu.setOnClickListener(this);
        imgmenu.setVisibility(View.VISIBLE);

        imgback = (ImageView)v.findViewById(R.id.img_back);
        imgback.setVisibility(View.GONE);

        imgplus = (ImageView)v.findViewById(R.id.img_plus);
        imgplus.setVisibility(View.GONE);

        imgedit = (ImageView)v.findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView)v.findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(getActivity()));
        txtheader.setText(R.string.community);

        return v;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_menu:
                ((MainActivity)getActivity()).opendrawer();
                break;
        }
    }
}
