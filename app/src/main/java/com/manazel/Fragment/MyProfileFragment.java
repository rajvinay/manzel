package com.manazel.Fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.ContractListActivity;
import com.manazel.EditProfile;
import com.manazel.MainActivity;
import com.manazel.R;
import com.manazel.modal.User;
import com.manazel.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by c119 on 29/11/16.
 */

public class MyProfileFragment extends Fragment implements View.OnClickListener {

    //CircleImageView imgprofile;
    ImageView imgprofile, imgmenu, imgback, imgplus, imgedit; //imgviewprevious, imgcontracts;
    TextView txtheader, txtname, txtaddress, txtemail, txtphone;
    TinyDB tb;
    User userobj;
    Gson gson;
    Button btn_current, btn_previous;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_myprofile, container, false);

        tb = new TinyDB(getActivity());

        gson = new Gson();

        imgmenu = (ImageView) v.findViewById(R.id.img_menu);
        imgmenu.setOnClickListener(this);
        imgmenu.setVisibility(View.VISIBLE);

        imgback = (ImageView) v.findViewById(R.id.img_back);
        imgback.setVisibility(View.GONE);

        imgplus = (ImageView) v.findViewById(R.id.img_plus);
        imgplus.setVisibility(View.GONE);

        imgedit = (ImageView) v.findViewById(R.id.img_edit);
        imgedit.setOnClickListener(this);
        imgedit.setVisibility(View.VISIBLE);

        txtheader = (TextView) v.findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(getActivity()));
        txtheader.setText(R.string.myprofile);

        imgprofile = (ImageView) v.findViewById(R.id.img_profile);

        btn_current = (Button) v.findViewById(R.id.btn_current);
        btn_previous = (Button) v.findViewById(R.id.btn_previous);

        txtname = (TextView) v.findViewById(R.id.txt_name);
        txtaddress = (TextView) v.findViewById(R.id.txt_address);
        txtemail = (TextView) v.findViewById(R.id.txt_email);
        txtphone = (TextView) v.findViewById(R.id.txt_phone);

        btn_current.setOnClickListener(this);
        btn_previous.setOnClickListener(this);
//        imgviewprevious = (ImageView) v.findViewById(R.id.img_viewprevious);
//        imgviewprevious.setOnClickListener(this);
//        imgcontracts = (ImageView) v.findViewById(R.id.img_contracts);
//        imgcontracts.setOnClickListener(this);

        setProfileInfo();

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_menu:
                ((MainActivity) getActivity()).opendrawer();
                break;

            case R.id.img_edit:
                calleditprofile();
                break;

            case R.id.btn_current:
                //Toast.makeText(getActivity(), "Work in progress", Toast.LENGTH_SHORT).show();
                showContractDialog();
                // Log.e("current contract", userobj.getContract());
                break;

            case R.id.btn_previous:
                startActivity(new Intent(getActivity(), ContractListActivity.class));
                //Toast.makeText(getActivity(), "Work in progress" + userobj.getContractImages().size(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void calleditprofile() {

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        Intent i = new Intent(getActivity(), EditProfile.class);
        getActivity().startActivity(i);

    }

    public void setProfileInfo() {

        String json = tb.getString("userobj");
        userobj = gson.fromJson(json, User.class);


        if (userobj != null) {
            Picasso.with(getActivity())
                    .load(userobj.getProfile_pic())
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.placeholder_profile)
                    .error(R.drawable.placeholder_profile)
                    .into(imgprofile);

            txtname.setText(userobj.getFname() + " " + userobj.getLname());
            txtaddress.setText(userobj.getAddress().toString());
            txtemail.setText(userobj.getEmail().toString());
            txtphone.setText(userobj.getContactno().toString());
        }
    }


    public void showContractDialog() {

        Dialog d = new Dialog(getActivity());
        d.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        d.setContentView(R.layout.dialog_layout_contract);
        ImageView imgview = (ImageView) d.findViewById(R.id.imgview);

        Picasso.with(getActivity()).load(userobj.getContract()).into(imgview);
        d.show();
    }

    @Subscribe
    public void onUpdateProfile(String key) {


        //Method will called after edit profile from EditPtofile screen

        if (key.equalsIgnoreCase("update_profile")) {

            setProfileInfo();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }
}

