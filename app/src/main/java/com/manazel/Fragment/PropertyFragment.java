package com.manazel.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.MainActivity;
import com.manazel.R;
import com.manazel.adapter.FeaturedPropertiesAdp;
import com.manazel.adapter.OffersPropertiesAdp;
import com.manazel.adapter.RecentPropertiesAdp;
import com.manazel.adapter.WishlistAdpForProperty;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.Featured;
import com.manazel.modal.Offer;
import com.manazel.modal.Property;
import com.manazel.modal.Recent;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.User;
import com.manazel.modal.Wishlist;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import static com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.TAG;

/**
 * Created by c217 on 19/05/17.
 */

public class PropertyFragment extends Fragment implements View.OnClickListener, WsResponseListener {

    TinyDB tb;
    RecyclerView rv_Recent, rv_Featured, rv_Offers, rv_Wishlist;
    LinearLayoutManager linearLayoutManagerFeatured, linearLayoutManagerRecents, linearLayoutManagerOfferes,
            linearLayoutManagerWishlist;
    RecentPropertiesAdp recent_adapter;
    FeaturedPropertiesAdp feature_adapter;
    OffersPropertiesAdp offer_adapter;
    WishlistAdpForProperty wishlist_adapter;
    int width, height;
    private ImageView imgmenu, img_Search;
    private TextView txtheader, txt_FetchData, txt_Title1, txt_Title2, txt_Title3,
            txt_Title4;

    //WS
    Property property;
    List<Recent> recents = new ArrayList<>();
    List<Featured> featureds = new ArrayList<>();
    List<Offer> offers = new ArrayList<>();
    List<Wishlist> wishlists = new ArrayList<>();
    private User userobj;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_property, container, false);

        tb = new TinyDB(getActivity());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;
        height = displaymetrics.heightPixels;

        imgmenu = (ImageView) v.findViewById(R.id.img_menu);
        img_Search = (ImageView) v.findViewById(R.id.img_Search);
        imgmenu.setOnClickListener(this);
        imgmenu.setVisibility(View.VISIBLE);
        img_Search.setVisibility(View.VISIBLE);
        img_Search.setOnClickListener(this);

        txt_Title1 = (TextView) v.findViewById(R.id.txt_Title1);
        txt_Title2 = (TextView) v.findViewById(R.id.txt_Title2);
        txt_Title3 = (TextView) v.findViewById(R.id.txt_Title3);
        txt_Title4 = (TextView) v.findViewById(R.id.txt_Title4);


        txt_FetchData = (TextView) v.findViewById(R.id.txt_FetchData);
        txt_FetchData.setVisibility(View.VISIBLE);

        txtheader = (TextView) v.findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(getActivity()));
        txtheader.setText(R.string.drawer_properties);

        rv_Recent = (RecyclerView) v.findViewById(R.id.rv_Recent);
        rv_Featured = (RecyclerView) v.findViewById(R.id.rv_Featured);
        rv_Offers = (RecyclerView) v.findViewById(R.id.rv_Offers);
        rv_Wishlist = (RecyclerView) v.findViewById(R.id.rv_Wishlist);

        rv_Recent.setVisibility(View.GONE);
        rv_Featured.setVisibility(View.GONE);
        rv_Offers.setVisibility(View.GONE);
        rv_Wishlist.setVisibility(View.GONE);

        txt_Title1.setVisibility(View.GONE);
        txt_Title2.setVisibility(View.GONE);
        txt_Title3.setVisibility(View.GONE);
        txt_Title4.setVisibility(View.GONE);

        linearLayoutManagerFeatured = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        linearLayoutManagerOfferes = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        linearLayoutManagerRecents = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        linearLayoutManagerWishlist = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        rv_Featured.setLayoutManager(linearLayoutManagerFeatured);
        rv_Recent.setLayoutManager(linearLayoutManagerRecents);
        rv_Offers.setLayoutManager(linearLayoutManagerOfferes);
        rv_Wishlist.setLayoutManager(linearLayoutManagerWishlist);

        recent_adapter = new RecentPropertiesAdp(getActivity(), (int) (width / 2.05), (int) (height / 2.85), recents);
        feature_adapter = new FeaturedPropertiesAdp(getActivity(), (int) (width / 2.05), (int) (height / 2.85), featureds);
        offer_adapter = new OffersPropertiesAdp(getActivity(), (int) (width / 2.05), (int) (height / 2.85), offers);
        wishlist_adapter = new WishlistAdpForProperty(getActivity(), (int) (width / 2.05), (int) (height / 2.85), wishlists);

        rv_Offers.setAdapter(offer_adapter);
        rv_Recent.setAdapter(recent_adapter);
        rv_Featured.setAdapter(feature_adapter);
        rv_Wishlist.setAdapter(wishlist_adapter);

        Gson gson = new Gson();
        String json = tb.getString("userobj");
        userobj = gson.fromJson(json, User.class);

//        callPropertyDataWS();

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

//        if (!EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().register(this);
//        }
    }

    private void callPropertyDataWS() {
        if (AppGlobal.isNetwork(getActivity())) {

            Common cm = new Common();
            cm.setStart_limit("0");
            cm.setEnd_limit("5");
            cm.setUser_id(userobj.getId());

            try {
                new AsyncPostService(getActivity(), getString(R.string.Please_wait), PropertyFragment.this, WsConstant.Req_Get_All_Property_Data,
                        cm, false, true).execute(WsConstant.WS_GET_ALL_PROPERTY_DATA);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            AppGlobal.showToast(getActivity(), getString(R.string.str_no_internet));
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_menu:
                ((MainActivity) getActivity()).opendrawer();
                break;
            case R.id.img_Search:
                callSearchFrag();
                break;
        }
    }

    private void callSearchFrag() {
        Fragment fragment = new SearchFragment();
        FragmentTransaction transaction = ((MainActivity) getActivity()).getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment, "SearchFragment");
        transaction.commit();
        ((MainActivity) getActivity()).getSupportFragmentManager().executePendingTransactions();
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error) {
        if (error == null) {
            if (serviceType.equalsIgnoreCase(WsConstant.Req_Get_All_Property_Data)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    int size = ((ResponseResult) data).getProperty().getRecent().size();
                    if (size > 0) {
//                        if (increaseval == 0) {
//                            txtfetachnews.setVisibility(View.GONE);
//                            recyclerView.setVisibility(View.VISIBLE);
//                        }
                        property = ((ResponseResult) data).getProperty();
                        if (property.getRecent().size() > 0) {
                            recents.clear();
                            recents.addAll(property.getRecent());
                            recent_adapter.notifyDataSetChanged();
                            txt_Title1.setVisibility(View.VISIBLE);
                            rv_Recent.setVisibility(View.VISIBLE);
                        } else {
                            txt_Title1.setVisibility(View.GONE);
                            rv_Recent.setVisibility(View.GONE);
                        }
                        if (property.getFeatured().size() > 0) {
                            featureds.clear();
                            featureds.addAll(property.getFeatured());
                            feature_adapter.notifyDataSetChanged();
                            txt_Title2.setVisibility(View.VISIBLE);
                            rv_Featured.setVisibility(View.VISIBLE);
                        } else {
                            txt_Title2.setVisibility(View.GONE);
                            rv_Featured.setVisibility(View.GONE);

                        }
                        if (property.getOffer().size() > 0) {

                            offers.clear();
                            offers.addAll(property.getOffer());
                            offer_adapter.notifyDataSetChanged();
                            txt_Title3.setVisibility(View.VISIBLE);
                            rv_Offers.setVisibility(View.VISIBLE);
                        } else {
                            txt_Title3.setVisibility(View.GONE);
                            rv_Offers.setVisibility(View.GONE);
                        }

                        if (property.getWishlists().size() > 0) {
                            wishlists.clear();
                            wishlists.addAll(property.getWishlists());
                            wishlist_adapter.notifyDataSetChanged();
                            txt_Title4.setVisibility(View.VISIBLE);
                            rv_Wishlist.setVisibility(View.VISIBLE);
                        } else {
                            txt_Title4.setVisibility(View.GONE);
                            rv_Wishlist.setVisibility(View.GONE);
                        }

                        txt_FetchData.setVisibility(View.GONE);

//                        loadingMore = false;
                    } else {
//                        loadingMore = false;
                        if (recent_adapter == null) {
                            txt_FetchData.setText("No Properties Found!");
                        }
                    }
                } else {
//                    loadingMore = false;
                }

            }

        } else {
//            if (swipyrefreshlayout.isRefreshing()) {
//                swipyrefreshlayout.setRefreshing(false);
//            }
//
//            loadingMore = false;
        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataReceived(String type) {


    }

    @Override
    public void onResume() {
        super.onResume();
        callPropertyDataWS();
    }

    @Override
    public void onPause() {

        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
//
//        if (EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().unregister(this);
//        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }


}
