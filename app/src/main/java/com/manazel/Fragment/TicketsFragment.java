package com.manazel.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.manazel.AddTicket;
import com.manazel.Constant.AES_Helper;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.MainActivity;
import com.manazel.R;
import com.manazel.TicketDetail;
import com.manazel.adapter.TicketsAdapter;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.Ticketsdata;
import com.manazel.modal.User;
import com.manazel.ws.AsyncPostCall;
import com.manazel.ws.DelieverResponse;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by c119 on 25/11/16.
 */

public class TicketsFragment extends Fragment implements View.OnClickListener, WsResponseListener,
        SwipyRefreshLayout.OnRefreshListener, DelieverResponse {
    ImageView imgmenu, imgback, imgplus, imgedit;
    TextView txtheader, txtfetachticket;

    ListView listarticle;
    TicketsAdapter ticketsadp;
    ArrayList<Ticketsdata> arrtickets, temparrtickets;
    TinyDB tb;
    User userobj;

    SwipyRefreshLayout swipyrefreshlayout;
    int increaseval = -1;
    int pagelimit;
    Gson gson;

    boolean loadingMore = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_tickets, container, false);

        increaseval = 0;
        pagelimit = 10;

        tb = new TinyDB(getActivity());

        arrtickets = new ArrayList<>();

        gson = new Gson();
        String json = tb.getString("userobj");
        userobj = gson.fromJson(json, User.class);

        swipyrefreshlayout = (SwipyRefreshLayout) v.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setOnRefreshListener(this);

        imgmenu = (ImageView) v.findViewById(R.id.img_menu);
        imgmenu.setOnClickListener(this);
        imgmenu.setVisibility(View.VISIBLE);

        imgback = (ImageView) v.findViewById(R.id.img_back);
        imgback.setVisibility(View.GONE);

        imgplus = (ImageView) v.findViewById(R.id.img_plus);
        imgplus.setOnClickListener(this);
        imgplus.setVisibility(View.VISIBLE);

        imgedit = (ImageView) v.findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView) v.findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(getActivity()));
        txtheader.setText(R.string.str_tickets);

        txtfetachticket = (TextView) v.findViewById(R.id.txt_fetachticket);
        txtfetachticket.setVisibility(View.VISIBLE);
        listarticle = (ListView) v.findViewById(R.id.list_tickets);
        listarticle.setVisibility(View.GONE);

        callAllTicketsWs(increaseval);

        return v;
    }

    private void callAllTicketsWs(int val) {

        if (AppGlobal.isNetwork(getActivity())) {

            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(getActivity(), AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setStart_record(String.valueOf(val));
            cm.setBatch_value(String.valueOf(pagelimit));
            cm.setUser_id(userobj.getId());
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            try {

                new AsyncPostService(getActivity(), getString(R.string.Please_wait), TicketsFragment.this, WsConstant.Req_Getalltickets, cm, false, true)
                        .execute(WsConstant.WS_GetAllTICKETS);

            } catch (Exception e) {
                e.printStackTrace();
            }

            loadingMore = true;

        } else {
            AppGlobal.showToast(getActivity(), getString(R.string.str_no_internet));
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_menu:
                ((MainActivity) getActivity()).opendrawer();
                break;

            case R.id.img_plus: {

                callVerifyUserApi();
//                if (userobj.getStatus().equalsIgnoreCase("1")) {
//
//                    calladdticket();
//
//                } else {
//
//                    Toast.makeText(getActivity(), "Only verify user can create ticket", Toast.LENGTH_SHORT).show();
//                }
            }
            break;
            default:
                break;
        }
    }

    @Subscribe
    public void createticket(String msg) {
        if (AppGlobal.isNetwork(getActivity())) {

            increaseval = 0;
            callAllTicketsWs(increaseval);

        } else {

            AppGlobal.showToast(getActivity(), getString(R.string.str_no_internet));
        }
    }

    public void callticketdetail(Ticketsdata ojbticket) {
        Gson gson = new Gson();
        String json = gson.toJson(ojbticket);
        tb.putString("ticketobj", json);

        Intent i = new Intent(getActivity(), TicketDetail.class);
        getActivity().startActivity(i);
    }

    private void calladdticket() {
        Intent i = new Intent(getActivity(), AddTicket.class);
        getActivity().startActivity(i);
    }


    public void callVerifyUserApi() {

        if (AppGlobal.isNetwork(getActivity())) {

            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(getActivity(), AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setUser_id(userobj.getId());
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            JSONObject jobj = new JSONObject();

            try {

                jobj.put("user_id", userobj.getId());
                jobj.put("secret_key", secretekey);
                jobj.put("access_key", new AES_Helper(globalPassword).encode(guid));

                new AsyncPostCall(this, WsConstant.WS_VERIFY_USER, WsConstant.Req_VerifyUser, jobj, true, "veryfing user..").execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else {

            if (userobj.getStatus().equalsIgnoreCase("1")) {

                calladdticket();

            } else {

                Toast.makeText(getActivity(), "Only verify user can create ticket", Toast.LENGTH_SHORT).show();
            }


            //AppGlobal.showToast(getActivity(), getString(R.string.str_no_internet));

        }


    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error) {
        if (error == null) {
            if (swipyrefreshlayout.isRefreshing()) {
                swipyrefreshlayout.setRefreshing(false);
            }

            if (serviceType.equalsIgnoreCase(WsConstant.Req_Getalltickets)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    int size = ((ResponseResult) data).getArrticket().size();
                    if (size > 0) {
                        if (increaseval == 0) {
                            txtfetachticket.setVisibility(View.GONE);
                            listarticle.setVisibility(View.VISIBLE);

                            arrtickets.clear();
                        }

                        temparrtickets = ((ResponseResult) data).getArrticket();
                        arrtickets.addAll(temparrtickets);

                        if (ticketsadp != null) {
                            ticketsadp.set_listtickets(arrtickets);
                            ticketsadp.notifyDataSetChanged();
                        } else {
                            ticketsadp = new TicketsAdapter(getActivity(), arrtickets, TicketsFragment.this);
                            listarticle.setAdapter(ticketsadp);
                        }

                        loadingMore = false;
                    } else {
                        loadingMore = false;
                        if (ticketsadp == null) {
                            txtfetachticket.setText("No Tickets Found!");
                        }
                    }
                } else {
                    loadingMore = false;
                }
            }

        } else {

            if (swipyrefreshlayout.isRefreshing()) {
                swipyrefreshlayout.setRefreshing(false);
            }

            loadingMore = false;
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (AppGlobal.isNetwork(getActivity()) && loadingMore == false) {
                increaseval = increaseval + pagelimit;
                callAllTicketsWs(increaseval);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDelieverResponse(int request_id, int status, Object data, String error, Bundle payload) {

        switch (request_id) {

            case WsConstant.Req_VerifyUser: {

                if (data != null) {
                    try {
                        JSONObject jobj = new JSONObject(data.toString());
                        if (jobj.optString("status").equalsIgnoreCase("SUCCESS")) {

                            JSONArray jarray = jobj.getJSONArray("userticketdata");
                            JSONObject childObj = jarray.getJSONObject(0);
                            if (childObj.has("is_verified")) {
                                if (childObj.optString("is_verified").equalsIgnoreCase("1")) {

                                    String json = tb.getString("userobj");
                                    userobj = gson.fromJson(json, User.class);
                                    userobj.setStatus("1");
                                    tb.putString("userobj", gson.toJson(userobj));
                                    calladdticket();
                                } else {

                                    Toast.makeText(getActivity(), "Only verify user can create ticket", Toast.LENGTH_SHORT).show();
                                }

                            }

                        } else {

                            if (jobj.has("message")) {

                                Toast.makeText(getActivity(), jobj.optString("message"), Toast.LENGTH_SHORT).show();

                            } else {

                                Toast.makeText(getActivity(), "problem while verifyig user information", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
            default:
                break;
        }
    }
}
