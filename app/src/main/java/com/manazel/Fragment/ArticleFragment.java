package com.manazel.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.manazel.ArticleDetail;
import com.manazel.Constant.AES_Helper;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.MainActivity;
import com.manazel.R;
import com.manazel.adapter.ArticleExpandableListAdp;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.User;
import com.manazel.modal.articlesarr;

/**
 * Created by c119 on 29/11/16.
 */

public class ArticleFragment extends Fragment implements View.OnClickListener, WsResponseListener {
    ImageView imgmenu, imgback, imgplus, imgedit;
    TextView txtheader, txtfetacharticle;
    ExpandableListView expListView;
    ArticleExpandableListAdp listAdapter;

    TinyDB tb;
    User userobj;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_article, container, false);

        tb = new TinyDB(getActivity());

        Gson gson = new Gson();
        String json = tb.getString("userobj");
        userobj = gson.fromJson(json, User.class);

        imgmenu = (ImageView) v.findViewById(R.id.img_menu);
        imgmenu.setOnClickListener(this);
        imgmenu.setVisibility(View.VISIBLE);

        imgback = (ImageView) v.findViewById(R.id.img_back);
        imgback.setVisibility(View.GONE);

        imgplus = (ImageView) v.findViewById(R.id.img_plus);
        imgplus.setVisibility(View.GONE);

        imgedit = (ImageView) v.findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView) v.findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(getActivity()));
        txtheader.setText(R.string.article);

        txtfetacharticle = (TextView) v.findViewById(R.id.txt_fetacharticle);
        txtfetacharticle.setVisibility(View.VISIBLE);
        expListView = (ExpandableListView) v.findViewById(R.id.articleExplist);
        expListView.setGroupIndicator(null);
        expListView.setChildIndicator(null);
        expListView.setChildDivider(getResources().getDrawable(R.color.black));
        expListView.setDivider(getResources().getDrawable(R.color.black));
        expListView.setDividerHeight(1);
        expListView.setVisibility(View.GONE);

        callArticleWs();

        return v;
    }

    private void callArticleWs() {
        if (AppGlobal.isNetwork(getActivity())) {
            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(getActivity(), AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            try {

                new AsyncPostService(getActivity(), getString(R.string.Please_wait), ArticleFragment.this, WsConstant.Req_GetAllArticles, cm, false, true)
                        .execute(WsConstant.WS_GETALLARTICLES);

            } catch (Exception e) {

                e.printStackTrace();
            }
        } else {

            AppGlobal.showToast(getActivity(), getString(R.string.str_no_internet));
        }
    }

    public void callarticledetail(articlesarr articlechildobj) {
        Gson gson = new Gson();
        String json = gson.toJson(articlechildobj);
        tb.putString("articledetailobj", json);

        Intent i = new Intent(getActivity(), ArticleDetail.class);
        getActivity().startActivity(i);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_menu:
                ((MainActivity) getActivity()).opendrawer();
                break;
        }
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error) {
        if (error == null) {
            if (serviceType.equalsIgnoreCase(WsConstant.Req_GetAllArticles)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    int size = ((ResponseResult) data).getArrarticledata().size();
                    if (size > 0) {
                        txtfetacharticle.setVisibility(View.GONE);
                        expListView.setVisibility(View.VISIBLE);

                        listAdapter = new ArticleExpandableListAdp(getActivity(), ((ResponseResult) data).getArrarticledata()
                                , ArticleFragment.this);
                        expListView.setAdapter(listAdapter);
                    } else {
                        if (listAdapter == null) {
                            txtfetacharticle.setText("No Articles Found!");
                        }
                    }
                }
            }

        }
    }
}
