package com.manazel.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.manazel.AnnouncementDetail;
import com.manazel.Constant.AES_Helper;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.DividerItemDecoration;
import com.manazel.MainActivity;
import com.manazel.R;
import com.manazel.adapter.AnnouncementAdapter;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Announcementsdata;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.User;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by c119 on 26/11/16.
 */

public class AnnouncementFragment extends Fragment implements View.OnClickListener,WsResponseListener,SwipyRefreshLayout.OnRefreshListener
{
    ImageView imgmenu,imgback,imgplus,imgedit;
    TextView txtheader,txtfetachannounce;

    RecyclerView recyclerView;
    AnnouncementAdapter mAdapter;
    List<Announcementsdata> AnnounceList,tempAnnounceList;
    RecyclerView.LayoutManager mLayoutManager;

    TinyDB tb;
    User userobj;

    SwipyRefreshLayout swipyrefreshlayout;
    int increaseval = -1;
    int pagelimit;

    boolean loadingMore = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.frag_announcement, container, false);

        increaseval = 0;
        pagelimit = 10;

        tb = new TinyDB(getActivity());

        AnnounceList = new ArrayList<>();

        Gson gson = new Gson();
        String json = tb.getString("userobj");
        userobj = gson.fromJson(json,User.class);

        swipyrefreshlayout = (SwipyRefreshLayout)v.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setOnRefreshListener(this);

        imgmenu = (ImageView)v.findViewById(R.id.img_menu);
        imgmenu.setOnClickListener(this);
        imgmenu.setVisibility(View.VISIBLE);

        imgback = (ImageView)v.findViewById(R.id.img_back);
        imgback.setVisibility(View.GONE);

        imgplus = (ImageView)v.findViewById(R.id.img_plus);
        imgplus.setVisibility(View.GONE);

        imgedit = (ImageView)v.findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView)v.findViewById(R.id.txt_header);
        txtheader.setText(R.string.announcement);

        txtfetachannounce = (TextView) v.findViewById(R.id.txt_fetachannounce);
        txtfetachannounce.setVisibility(View.VISIBLE);
        recyclerView = (RecyclerView)v.findViewById(R.id.recycler_view);
        recyclerView.setVisibility(View.GONE);

        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        callannouncews(increaseval);

        return v;
    }


    private void callannouncews(int val)
    {
        if(AppGlobal.isNetwork(getActivity()))
        {
            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(getActivity(),AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setStart_record(String.valueOf(val));
            cm.setBatch_value(String.valueOf(pagelimit));
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            try
            {
                new AsyncPostService(getActivity(),getString(R.string.Please_wait),AnnouncementFragment.this,WsConstant.Req_GetAllAnnouncement,cm,false,true)
                        .execute(WsConstant.WS_GETALLANNOUNCEMENTS);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            loadingMore = true;
        }
        else
        {
            AppGlobal.showToast(getActivity(),getString(R.string.str_no_internet));
        }
    }

    public void callannouncedetail(Announcementsdata announcedetailobj)
    {
        Gson gson = new Gson();
        String json = gson.toJson(announcedetailobj);
        tb.putString("announcedetailobj",json);

        Intent i = new Intent(getActivity(),AnnouncementDetail.class);
        getActivity().startActivity(i);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_menu:
                ((MainActivity)getActivity()).opendrawer();
                break;
        }
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error)
    {
        if (error == null)
        {
            if(swipyrefreshlayout.isRefreshing())
            {
                swipyrefreshlayout.setRefreshing(false);
            }

            if (serviceType.equalsIgnoreCase(WsConstant.Req_GetAllAnnouncement))
            {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("SUCCESS"))
                {
                    int size = ((ResponseResult) data).getArrannouncements().size();
                    if (size > 0)
                    {
                        if(increaseval == 0)
                        {
                            txtfetachannounce.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        }

                        tempAnnounceList = ((ResponseResult) data).getArrannouncements();
                        AnnounceList.addAll(tempAnnounceList);

                        if(mAdapter != null)
                        {
                            mAdapter.setAnnounceList(AnnounceList);
                            mAdapter.notifyDataSetChanged();
                        }
                        else
                        {
                            mAdapter = new AnnouncementAdapter(getActivity(),AnnounceList,AnnouncementFragment.this);
                            recyclerView.setAdapter(mAdapter);
                        }

                        loadingMore = false;
                    }
                    else
                    {
                        loadingMore = false;
                        if(mAdapter == null)
                        {
                            txtfetachannounce.setText("No Announcements Found!");
                        }
                    }
                }
                else
                {
                    loadingMore = false;
                }

            }

        }
        else
        {
            if(swipyrefreshlayout.isRefreshing())
            {
                swipyrefreshlayout.setRefreshing(false);
            }

            loadingMore = false;
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction)
    {
        if(direction == SwipyRefreshLayoutDirection.BOTTOM)
        {
            if (AppGlobal.isNetwork(getActivity()) && loadingMore == false)
            {
                increaseval = increaseval+pagelimit;
                callannouncews(increaseval);
            }
        }
    }
}
