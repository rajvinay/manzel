package com.manazel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.manazel.Constant.AES_Helper;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.AppGlobal;
import com.manazel.Constant.TinyDB;
import com.manazel.Constant.WsConstant;
import com.manazel.asynktask.AsyncPostService;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.modal.Ticketsdata;
import com.manazel.modal.User;
import com.manazel.modal.displaymessagedata;
import com.manazel.ws.AsyncPostCall;
import com.manazel.ws.DelieverResponse;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.manazel.Constant.WsConstant.Req_SendMessage;
import static com.manazel.R.id.btn_send;
import static com.manazel.R.id.img_profile;

/**
 * Created by c119 on 29/11/16.
 */

public class TicketDetail extends FragmentActivity implements View.OnClickListener, WsResponseListener,
        SwipyRefreshLayout.OnRefreshListener, DelieverResponse {
    ImageView imgmenu, imgback, imgplus, imgedit;
    TextView txtheader;
    ListView listticketdetail;
    ViewGroup listheader;

    TicketDetailAdapter tda;
    ArrayList<displaymessagedata> arrcomments, temparrcomments;

    TinyDB tb;
    Ticketsdata ojbticket;

    TextView lblseriesno, txtseriesno, lblassignto, txtassignto, lbltenant, txttenant, lbltitle, txttitle,
            lbldepartment, txtdepartment, lbltickettype, txttickettype, lblticketpriority, txtticketpriority,
            lblticketstatus, txtticketstatus, lbladdedon, txtaddedon, lbldescription, txtdescription, lblcomment;

    EditText edtinputcomment;
    Button btnsend;

    ImageView imgmainticket;

    SwipyRefreshLayout swipyrefreshlayout;
    int increaseval = -1;
    int pagelimit;
    User userobj;
    Gson gson1;

    boolean loadingMore = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticketdetail);

        tb = new TinyDB(this);

        increaseval = 0;
        pagelimit = 5;

        arrcomments = new ArrayList<>();

        gson1 = new Gson();
        String json1 = tb.getString("userobj");
        userobj = gson1.fromJson(json1, User.class);

        imgmenu = (ImageView) findViewById(R.id.img_menu);
        imgmenu.setVisibility(View.GONE);

        imgback = (ImageView) findViewById(R.id.img_back);
        imgback.setOnClickListener(this);
        imgback.setVisibility(View.VISIBLE);

        imgplus = (ImageView) findViewById(R.id.img_plus);
        imgplus.setOnClickListener(this);
        imgplus.setVisibility(View.VISIBLE);

        imgedit = (ImageView) findViewById(R.id.img_edit);
        imgedit.setVisibility(View.GONE);

        txtheader = (TextView) findViewById(R.id.txt_header);
        txtheader.setTypeface(AppGlobal.getHelveticaNuneLight(this));
        txtheader.setText(R.string.ticket_details);

        swipyrefreshlayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setOnRefreshListener(this);

        listticketdetail = (ListView) findViewById(R.id.list_ticketdetail);

        Gson gson = new Gson();
        String json = tb.getString("ticketobj");
        ojbticket = gson.fromJson(json, Ticketsdata.class);

        LayoutInflater myinflater = getLayoutInflater();
        listheader = (ViewGroup) myinflater.inflate(R.layout.ticketdetail_header, listticketdetail, false);
        listticketdetail.addHeaderView(listheader, null, false);

        imgmainticket = (ImageView) listheader.findViewById(R.id.img_main_ticket);

        if (ojbticket.getTicketImages() != "") {
            Picasso.with(this).load(ojbticket.getTicketImages())
                    .placeholder(R.drawable.ticket_detail_placeholder)
                    .error(R.drawable.ticket_detail_placeholder).into(imgmainticket);
        } else {
            imgmainticket.setImageResource(R.drawable.ticket_detail_placeholder);
        }

        lblseriesno = (TextView) listheader.findViewById(R.id.lbl_seriesno);
        lblseriesno.setTypeface(AppGlobal.getLatoBold(this));

        txtseriesno = (TextView) listheader.findViewById(R.id.txt_seriesno);
        txtseriesno.setTypeface(AppGlobal.getLatoRegular(this));
        txtseriesno.setText(ojbticket.getSeries_no().toString());

        lblassignto = (TextView) listheader.findViewById(R.id.lbl_assignto);
        lblassignto.setTypeface(AppGlobal.getLatoBold(this));

        txtassignto = (TextView) listheader.findViewById(R.id.txt_assignto);
        txtassignto.setTypeface(AppGlobal.getLatoRegular(this));
        txtassignto.setText(ojbticket.getAssignTofname() + " " + ojbticket.getAssignTolname());

        lbltenant = (TextView) listheader.findViewById(R.id.lbl_tenant);
        lbltenant.setTypeface(AppGlobal.getLatoBold(this));

        txttenant = (TextView) listheader.findViewById(R.id.txt_tenant);
        txttenant.setTypeface(AppGlobal.getLatoRegular(this));
        txttenant.setText(userobj.getFname() + " " + userobj.getLname());

        lbltitle = (TextView) listheader.findViewById(R.id.lbl_title);
        lbltitle.setTypeface(AppGlobal.getLatoBold(this));

        txttitle = (TextView) listheader.findViewById(R.id.txt_title);
        txttitle.setTypeface(AppGlobal.getLatoRegular(this));
        txttitle.setText(ojbticket.getTitle().toString());

        lbldepartment = (TextView) listheader.findViewById(R.id.lbl_department);
        lbldepartment.setTypeface(AppGlobal.getLatoBold(this));

        txtdepartment = (TextView) listheader.findViewById(R.id.txt_department);
        txtdepartment.setTypeface(AppGlobal.getLatoRegular(this));
        txtdepartment.setText(ojbticket.getDepartmentName().toString());

        lbltickettype = (TextView) listheader.findViewById(R.id.lbl_tickettype);
        lbltickettype.setTypeface(AppGlobal.getLatoBold(this));

        txttickettype = (TextView) listheader.findViewById(R.id.txt_tickettype);
        txttickettype.setTypeface(AppGlobal.getLatoRegular(this));
        txttickettype.setText(ojbticket.getTickettypeName().toString());

        lblticketpriority = (TextView) listheader.findViewById(R.id.lbl_ticketpriority);
        lblticketpriority.setTypeface(AppGlobal.getLatoBold(this));

        txtticketpriority = (TextView) listheader.findViewById(R.id.txt_ticketpriority);
        txtticketpriority.setTypeface(AppGlobal.getLatoRegular(this));
        txtticketpriority.setText(ojbticket.getTicketPriority().toString());

        lblticketstatus = (TextView) listheader.findViewById(R.id.lbl_ticketstatus);
        lblticketstatus.setTypeface(AppGlobal.getLatoBold(this));

        txtticketstatus = (TextView) listheader.findViewById(R.id.txt_ticketstatus);
        txtticketstatus.setTypeface(AppGlobal.getLatoRegular(this));
        txtticketstatus.setText(ojbticket.getTicketStatus().toString());

        lbladdedon = (TextView) listheader.findViewById(R.id.lbl_addedon);
        lbladdedon.setTypeface(AppGlobal.getLatoBold(this));

        txtaddedon = (TextView) listheader.findViewById(R.id.txt_addedon);
        txtaddedon.setTypeface(AppGlobal.getLatoRegular(this));
        String date[] = ojbticket.getModified().toString().split(" ");
        txtaddedon.setText(date[0]);

        lbldescription = (TextView) listheader.findViewById(R.id.lbl_description);
        lbldescription.setTypeface(AppGlobal.getLatoBold(this));

        txtdescription = (TextView) listheader.findViewById(R.id.txt_description);
        txtdescription.setTypeface(AppGlobal.getLatoRegular(this));
        txtdescription.setText(ojbticket.getDescriptions().toString());

        lblcomment = (TextView) listheader.findViewById(R.id.lbl_comment);
        lblcomment.setTypeface(AppGlobal.getLatoBold(this));
        lblcomment.setVisibility(View.GONE);

        edtinputcomment = (EditText) findViewById(R.id.edt_inputcomment);
        edtinputcomment.setTypeface(AppGlobal.getLatoRegular(this));

        btnsend = (Button) findViewById(btn_send);
        btnsend.setTypeface(AppGlobal.getLatoRegular(this));
        btnsend.setOnClickListener(this);

        callDisplayMessage(increaseval);
    }

    private void callDisplayMessage(int val) {
        if (AppGlobal.isNetwork(this)) {
            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(this, AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setStart_record(String.valueOf(val));
            cm.setBatch_value(String.valueOf(pagelimit));
            cm.setTicket_id(ojbticket.getTicketId());
            cm.setSent_from(userobj.getId());
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            try {
                new AsyncPostService(TicketDetail.this, getString(R.string.Please_wait), WsConstant.Req_DisplayMessage, cm, false, true)
                        .execute(WsConstant.WS_DISPLAYMESSAGE);
            } catch (Exception e) {
                e.printStackTrace();
            }

            loadingMore = true;
        } else {
            AppGlobal.showToast(this, getString(R.string.str_no_internet));
        }
    }

    @Override
    public void onDelieverResponse(String serviceType, Object data, Exception error) {
        if (error == null) {
            if (swipyrefreshlayout.isRefreshing()) {
                swipyrefreshlayout.setRefreshing(false);
            }

            if (serviceType.equalsIgnoreCase(WsConstant.Req_DisplayMessage)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    int size = ((ResponseResult) data).getArrticketcomments().size();
                    if (size > 0) {
                        if (increaseval == 0) {
                            lblcomment.setVisibility(View.VISIBLE);
                            arrcomments.clear();
                        }

                        temparrcomments = ((ResponseResult) data).getArrticketcomments();
                        arrcomments.addAll(temparrcomments);

                        if (tda != null) {
                            tda.set_ticketscomments(arrcomments);
                            tda.notifyDataSetChanged();
                        } else {
                            tda = new TicketDetailAdapter(TicketDetail.this, arrcomments);
                            listticketdetail.setAdapter(tda);
                        }

                        loadingMore = false;
                    } else {
                        loadingMore = false;
                        if (tda == null) {
                            listticketdetail.setAdapter(null);
                        }

                    }
                } else {
                    loadingMore = false;
                }
            }

            if (serviceType.equalsIgnoreCase(WsConstant.Req_SendMessage)) {
                String status = ((ResponseResult) data).getStatus();
                if (status.equalsIgnoreCase("1")) {
                    if (((ResponseResult) data).getMessage().equalsIgnoreCase("Message Send Successfully.")) {
                        edtinputcomment.setText("");
                        increaseval = 0;
                        callDisplayMessage(increaseval);
                    }
                }
            }

        } else {
            if (swipyrefreshlayout.isRefreshing()) {
                swipyrefreshlayout.setRefreshing(false);
            }

            loadingMore = false;
        }
    }

    @Override
    public void onDelieverResponse(int request_id, int status, Object data, String error, Bundle payload) {
        switch (request_id) {

            case WsConstant.Req_VerifyUser: {

                if (data != null) {

                    try {

                        JSONObject jobj = new JSONObject(data.toString());
                        if (jobj.optString("status").equalsIgnoreCase("SUCCESS")) {

                            JSONArray jarray = jobj.getJSONArray("userticketdata");
                            JSONObject childObj = jarray.getJSONObject(0);

                            if (childObj.has("is_verified")) {

                                if (childObj.optString("is_verified").equalsIgnoreCase("1")) {

                                    String json = tb.getString("userobj");
                                    userobj = gson1.fromJson(json, User.class);
                                    userobj.setStatus("1");
                                    tb.putString("userobj", gson1.toJson(userobj));
                                    calladdticket();

                                } else {

                                    Toast.makeText(this, "Only verify user can create ticket", Toast.LENGTH_SHORT).show();
                                }

                            }

                        } else {

                            if (jobj.has("message")) {

                                Toast.makeText(this, jobj.optString("message"), Toast.LENGTH_SHORT).show();

                            } else {

                                Toast.makeText(this, "problem while verifyig user information", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
            default:
                break;
        }
    }


    public class TicketDetailAdapter extends BaseAdapter {
        public Context _context;
        public ArrayList<displaymessagedata> _ticketscomments;
        public LayoutInflater layoutInflater;
        public int selected_grp = -1;
        public SimpleDateFormat simpleDateFormat;
        public TimeZone utcZone;

        public TicketDetailAdapter(Context context, ArrayList<displaymessagedata> _ticketscomments) {
            this._context = context;
            this._ticketscomments = _ticketscomments;
            layoutInflater = LayoutInflater.from(context);

            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            utcZone = TimeZone.getTimeZone("UTC");
        }

        public ArrayList<displaymessagedata> get_ticketscomments() {
            return _ticketscomments;
        }

        public void set_ticketscomments(ArrayList<displaymessagedata> _ticketscomments) {
            this._ticketscomments = _ticketscomments;
        }

        @Override
        public int getCount() {
            return _ticketscomments.size();
        }

        @Override
        public Object getItem(int position) {
            return _ticketscomments.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.row_ticket_comment, null);
                holder = new ViewHolder();
                holder.imgprofile = (CircleImageView) convertView.findViewById(img_profile);
                holder.txttitle = (TextView) convertView.findViewById(R.id.txt_title);
                holder.txttitle.setTypeface(AppGlobal.getLatoBold(_context));
                holder.txttime = (TextView) convertView.findViewById(R.id.txt_time);
                holder.txttime.setTypeface(AppGlobal.getLatoRegular(_context));
                holder.txtdesc = (TextView) convertView.findViewById(R.id.txt_desc);
                holder.txtdesc.setTypeface(AppGlobal.getLatoRegular(_context));

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (_ticketscomments.get(position).getUserImages() != "") {
                Picasso.with(_context).load(_ticketscomments.get(position).getUserImages())
                        .placeholder(R.drawable.appimage).error(R.drawable.appimage).into(holder.imgprofile);
            } else {
                holder.imgprofile.setImageResource(R.drawable.appimage);
            }

            simpleDateFormat.setTimeZone(utcZone);
            Date myDate = null;

            try {
                myDate = simpleDateFormat.parse(_ticketscomments.get(position).getCreated_date());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Locale current = getResources().getConfiguration().locale;
            String time = new PrettyTime(current).format(myDate);
            holder.txttime.setText(time);

            holder.txttitle.setText(_ticketscomments.get(position).getFname() + " " + _ticketscomments.get(position).getLname());
            holder.txtdesc.setText(_ticketscomments.get(position).getMessage());

            holder.txttitle.setTag(position);

            return convertView;
        }

        public class ViewHolder {
            TextView txttitle, txttime, txtdesc;
            CircleImageView imgprofile;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.img_plus:

                callVerifyUserApi();

//                if (userobj.getStatus().equalsIgnoreCase("1")) {
//
//                    calladdticket();
//
//                } else {
//
//                    Toast.makeText(this, "Only verify user can create ticket", Toast.LENGTH_SHORT).show();
//                }
                break;

            case R.id.btn_send:
                chckvalidation();
                break;
        }
    }

    private void chckvalidation() {
        if (edtinputcomment.getText().toString().length() > 0 && edtinputcomment.getText().toString() != ""
                && edtinputcomment.getText().toString() != " ") {
            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

            callsendcommentws(edtinputcomment.getText().toString());
        } else {
            AppGlobal.showToast(TicketDetail.this, getString(R.string.str_enter_cimment));
        }
    }

    public void callVerifyUserApi() {

        if (AppGlobal.isNetwork(this)) {

            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(this, AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setUser_id(userobj.getId());
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            JSONObject jobj = new JSONObject();

            try {

                jobj.put("user_id", userobj.getId());
                jobj.put("secret_key", secretekey);
                jobj.put("access_key", new AES_Helper(globalPassword).encode(guid));

                new AsyncPostCall(this, WsConstant.WS_VERIFY_USER, WsConstant.Req_VerifyUser, jobj, true, "veryfing user..").execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else {

            if (userobj.getStatus().equalsIgnoreCase("1")) {

                calladdticket();

            } else {

                Toast.makeText(this, "Only verify user can create ticket", Toast.LENGTH_SHORT).show();
            }

            //AppGlobal.showToast(this, getString(R.string.str_no_internet));

        }


    }


    private void callsendcommentws(String message) {
        if (AppGlobal.isNetwork(this)) {
            String secretekey = tb.getString(AppConstant.PREF_SECRETE_KEY);
            String globalPassword = AppGlobal.getStringPreference(this, AppConstant.APP_PREF_GLOBAL_PASSWORD);
            String guid = tb.getString(AppConstant.PREF_GUID);

            Common cm = new Common();
            cm.setTicket_id(ojbticket.getTicketId());
            cm.setMessage(message);
            cm.setSent_from(userobj.getId());
            cm.setStatus("0");
            cm.setSecret_key(secretekey);
            cm.setAccess_key(new AES_Helper(globalPassword).encode(guid));

            try {
                new AsyncPostService(TicketDetail.this, "Adding Comment...", Req_SendMessage, cm, true, true)
                        .execute(WsConstant.WS_SENDMESSAGE);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            AppGlobal.showToast(this, getString(R.string.str_no_internet));
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (AppGlobal.isNetwork(this) && loadingMore == false) {
                increaseval = increaseval + pagelimit;
                callDisplayMessage(increaseval);
            }
        }
    }

    private void calladdticket() {
        Intent i = new Intent(TicketDetail.this, AddTicket.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
