package com.manazel.beacon;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanFilter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.manazel.BeaconDetailActivity;
import com.manazel.Constant.AppGlobal;
import com.manazel.MainActivity;
import com.manazel.R;
import com.manazel.modal.BeaconDataModel;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Case;
import io.realm.Realm;

/**
 * Created by C162 on 03/11/16.
 */

public class MyBeaconsService extends Service {

    private static final String TAG = MyBeaconsService.class.getSimpleName();
    public static final String BROADCAST_BEACON = "beacon";
    public static final String BEACON_EXITED = "beacon_exited";
    public static final String BEACON_ENTERED = "beacon_entered";
    public static final String BEACON_NEAR_BY = "beacon_near_by";
    public static final String BEACON_RANGE = "beacon_range";
    public static final String BEACON_MINOR = "beacon_minor";
    public static final String BEACON_MAJOR = "beacon_major";
    public static final String BEACON_TYPE = "is_close_approach";//is_close_approach = 0 for enter nd exit is_close_approach=1 for nearby
    public static final String BEACON_IMAGE = "beacon_image";
    public static final String BEACON_ID = "beacon_id";
    public static final String BEACON_NAME = "beacon_name";
    public static final String PLACE_ID = "place_id";
    public static final String BEACON_MESSAGE = "beacon_message";
    public static final String PLACE_IMAGE = "place_image";
    public static final String BEACON_ENTRY_TEXT = "beacon_entry_text";
    public static final String BEACON_EXIT_TEXT = "beacon_exit_text";
    public static final String BEACON_NEAR_BY_TEXT = "beacon_near_by_text";
    public static final String BEACON_UUDI = "beacon_uuid";
    public BeaconManager beaconManager;
    List<ScanFilter> filters = new ArrayList<ScanFilter>();
    private UUID mMyUuid = UUID.fromString("32769A6A-E884-4CCE-8D86-9A979E1B5ED5");
    private BluetoothLeScanner mBluetoothLeScanner;
    private boolean isServiceRunning;
    private int intentId = 0;
    private String placeid, placeImage, message, entered_message, exit_message, near_by_message;
    private String messageBeacon;
    PendingIntent pendingIntent;
    Intent intent;
    List<BeaconDataModel> beaconlist;
    Realm realm;
    final static int TYPE_ENTER = 0, TYPE_EXIT = 1, TYPE_RANGE = 2;

    public boolean isServiceRunning() {
        return isServiceRunning;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "onCreate");
        isServiceRunning = true;

        Realm.init(this);
        realm = Realm.getDefaultInstance();
        beaconManager = new BeaconManager(getApplicationContext());

        beaconlist = realm.where(BeaconDataModel.class).findAll();
        intent = new Intent(getApplicationContext(), BeaconDetailActivity.class);

//       beaconManager.setBackgroundScanPeriod(1, 10);
        beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
            @Override
            public void onEnteredRegion(Region region, List<Beacon> list) {

                Log.e(TAG, BEACON_ENTERED);
                Log.e("identifire", region.getIdentifier());

                intent.putExtra("uuid", region.getProximityUUID().toString());
                intent.putExtra("major", region.getMajor().toString());
                intent.putExtra("minor", region.getMinor().toString());
                pendingIntent = PendingIntent.getActivity(MyBeaconsService.this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

                AppGlobal.showToast(getApplicationContext(), BEACON_ENTERED);

                getbeaconDetails(region, TYPE_ENTER, 0);

//                BeaconDataModel beaconObj = realm.where(BeaconDataModel.class)
//                        .beginGroup()
//                        .contains("uuid", region.getProximityUUID().toString(), Case.INSENSITIVE)
//                        .contains("major", region.getMajor().toString(), Case.INSENSITIVE)
//                        .contains("minior", region.getMinor().toString(), Case.INSENSITIVE)
//                        .endGroup()
//                        .findFirst();
//
//                String name = "";
//                if (beaconObj != null) {
//
//                    name = beaconObj.getBeaconName();
//                    if (beaconObj.getIsCloseApproach().equalsIgnoreCase("1")) { // enrty-exit type. show notification
//
//                        generateNotification(getApplicationContext().getResources().getString(R.string.app_name), "Enter - " + name, pendingIntent);
//                    }
//
//                } else {
//
//                    name = region.getIdentifier();
//                }

                // showNotification(getApplicationContext().getResources().getString(R.string.app_name) + "Method", getEnteredMessage(region), BEACON_ENTERED, region.getIdentifier());

            }

            @Override
            public void onExitedRegion(Region region) {
                //beaconManager.stopRanging(region);
                Log.e(TAG, BEACON_EXITED);
                AppGlobal.showToast(getApplicationContext(), BEACON_EXITED);
                // showNotification(getApplicationContext().getResources().getString(R.string.app_name) + "Method", getExitedRegionMessage(region), BEACON_EXITED, region.getIdentifier());

                getbeaconDetails(region, TYPE_EXIT, 0);

//                BeaconDataModel beaconObj = realm.where(BeaconDataModel.class)
//                        .beginGroup()
//                        .contains("uuid", region.getProximityUUID().toString(), Case.INSENSITIVE)
//                        .contains("major", region.getMajor().toString(), Case.INSENSITIVE)
//                        .contains("minior", region.getMinor().toString(), Case.INSENSITIVE)
//                        .endGroup()
//                        .findFirst();
//
//                String name = "";
//                if (beaconObj != null) {
//
//                    name = beaconObj.getBeaconName();
//                    if (beaconObj.getIsCloseApproach().equalsIgnoreCase("1")) { // enrty-exit type. show notification
//
//                        generateNotification(getApplicationContext().getResources().getString(R.string.app_name), "Exit - " + name, pendingIntent);
//                    }
//
//                } else {
//
//                    name = region.getIdentifier();
//                }

            }
        });


        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
                                             @Override
                                             public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                                                 //  Log.e(TAG, "setRangingListener");


                                                 if (list != null && list.size() > 0) {
                                                     for (Beacon beacon : list) {

                                                         double range = calculateDistance(beacon.getMeasuredPower(), beacon.getRssi());

                                                         getbeaconDetails(region, TYPE_RANGE, range);

//                                                         AppGlobal.showToast(MyBeaconsService.this, "manzel distance : " + calculateDistance(beacon.getMeasuredPower(), beacon.getRssi()));
                                                         //  Log.e("Distanace manazel", calculateDistance(beacon.getMeasuredPower(), beacon.getRssi()) + "");

                                                         if (beacon.getProximityUUID().equals(UUID.fromString("fda50693-a4e2-4fb1-afcf-c6eb07647825"))) {

                                                         }
                                                         // AppGlobal.showToast(getApplicationContext(), BEACON_RANGE);
                                                         //  Log.e(TAG, BEACON_RANGE + region.getProximityUUID());


                                                     }
                                                 }
                                                 // UUID uuid = UUID.fromString("");

                                             }

                                         }

        );

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {

                                  @Override
                                  public void onServiceReady() {


//                                      beaconManager.startMonitoring(new Region("fda50693-a4e2-4fb1-afcf-c6eb07647825" + ":" + "10035" + ":" + "56498",
//                                              UUID.fromString("fda50693-a4e2-4fb1-afcf-c6eb07647825"), 10035, 56498));
                                      Log.e(TAG, "beacon connect");
//
//                                      beaconManager.startRanging(new Region("fda50693-a4e2-4fb1-afcf-c6eb07647825" + ":" + "10035" + ":" + "56498",
//                                              UUID.fromString("fda50693-a4e2-4fb1-afcf-c6eb07647825"), 10035, 56498));


                                      if (beaconlist != null && beaconlist.size() > 0) {

                                          try {

                                              for (BeaconDataModel beacon : beaconlist) {

                                                  beaconManager.startMonitoring(new Region(beacon.getUuid() + ":" + beacon.getMajor() + ":" + beacon.getMinior(),
                                                          UUID.fromString(beacon.getUuid()), Integer.parseInt(beacon.getMajor()), Integer.parseInt(beacon.getMinior())));

                                                  beaconManager.startRanging(new Region(beacon.getUuid() + ":" + beacon.getMajor() + ":" + beacon.getMinior(),
                                                          UUID.fromString(beacon.getUuid()), Integer.parseInt(beacon.getMajor()), Integer.parseInt(beacon.getMinior())));


                                              }
                                          } catch (Exception e) {
                                              e.printStackTrace();
                                          }
                                      }

                                      //showNotification("Beacon Region", "onServiceReady");
//                                      Set<String> stringSet = Preference.getStringSetPrefs("keyBeacons", getApplicationContext());
//                                      if (stringSet != null)
//                                      {
//                                          for (String str : stringSet)
//                                          {
//                                              try
//                                              {
//                                                  JSONObject jsonObject = new JSONObject(str);
//                                                  beaconManager.startMonitoring(new Region(jsonObject.getString("uuid") + ":" + jsonObject.getInt("major") + ":" + jsonObject.getInt("minor"),
//                                                          UUID.fromString(jsonObject.getString("uuid")), jsonObject.getInt("major"), jsonObject.getInt("minor")));
//                                                  //  else if (jsonObject.getString("is_close_approach").equals("1"))
////                                                  beaconManager.startRanging(new Region(jsonObject.getString("uuid") + ":" + jsonObject.getInt("major") + ":" + jsonObject.getInt("minor"),
////                                                          UUID.fromString(jsonObject.getString("uuid")), jsonObject.getInt("major"), jsonObject.getInt("minor")));
//                                              } catch (JSONException e) {
//                                                  e.printStackTrace();
//                                              }
//                                          }
//                                      }
//
                                  }

                              }

        );

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        isServiceRunning = false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG, "In Background");

        return null;
    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }

        } else {

            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


    public void getbeaconDetails(Region region, int type, double range) {

        BeaconDataModel beaconObj = realm.where(BeaconDataModel.class)
                .beginGroup()
                .contains("uuid", region.getProximityUUID().toString(), Case.INSENSITIVE)
                .contains("major", region.getMajor().toString(), Case.INSENSITIVE)
                .contains("minior", region.getMinor().toString(), Case.INSENSITIVE)
                .endGroup()
                .findFirst();

        if (beaconObj != null) {

            switch (type) {

                case TYPE_ENTER: {

                    if (beaconObj.getIsCloseApproach().equalsIgnoreCase("1")) { // enrty-exit type. show notification

                        generateNotification(getApplicationContext().getResources().getString(R.string.app_name), "Enter - " + beaconObj.getBeaconName(), pendingIntent);
                    }

                }
                break;
                case TYPE_EXIT: {

                    realm.beginTransaction();
                    beaconObj.setInRegion(false);
                    realm.commitTransaction();

                    if (beaconObj.getIsCloseApproach().equalsIgnoreCase("1")) { // enrty-exit type. show notification

                        generateNotification(getApplicationContext().getResources().getString(R.string.app_name), "Exit - " + beaconObj.getBeaconName(), pendingIntent);
                    }
                }
                break;
                case TYPE_RANGE: {

                    if (beaconObj.getIsCloseApproach().equalsIgnoreCase("0")) { // entry-exit type. show notification

                        int becoan_range = 1;

                        try {

                            becoan_range = Integer.parseInt(beaconObj.getRange());

                        } catch (Exception e) {

                            e.printStackTrace();
                        }

                        if (range < becoan_range) {

                            if (!beaconObj.isInRegion()) {

                                generateNotification(getApplicationContext().getResources().getString(R.string.app_name), "Range - " + beaconObj.getBeaconName(), pendingIntent);
                            }

                            realm.beginTransaction();
                            beaconObj.setInRegion(true);
                            realm.commitTransaction();

                        } else {

                            realm.beginTransaction();
                            beaconObj.setInRegion(false);
                            realm.commitTransaction();
                        }
                    }
                }
                break;
                default:
                    break;

            }


        }


    }

    public void showNotification(String title, String message, String type, String beacon) {
        if (isAppIsInBackground(getApplicationContext())) {
            AppGlobal.showToast(getApplicationContext(), "generate push");

//            Intent notifyIntent = new Intent(this,MainActivity.class);
//            notifyIntent.putExtra(BEACON_ENTRY_TEXT, entered_message);
//            notifyIntent.putExtra(BEACON_EXIT_TEXT, exit_message);
//            notifyIntent.putExtra(BEACON_NEAR_BY_TEXT, near_by_message);
//            notifyIntent.putExtra(BEACON_MESSAGE, messageBeacon);
//            notifyIntent.putExtra(PLACE_ID, placeid);
//            notifyIntent.putExtra("type", type);
//            notifyIntent.putExtra(BEACON_NAME, beacon);
//            notifyIntent.putExtra(PLACE_IMAGE, placeImage);
//            notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            PendingIntent pendingIntent = PendingIntent.getActivities(this, intentId++,
//                    new Intent[]{notifyIntent}, PendingIntent.FLAG_ONE_SHOT);
//            Notification notification = new Notification.Builder(this)
//                    .setSmallIcon(R.drawable.icon)
//                    .setContentTitle(title)
//                    .setContentText(message)
//                    .setAutoCancel(true)
//                    .setContentIntent(pendingIntent)
//                    .build();
//            notification.defaults |= Notification.DEFAULT_SOUND;
//            NotificationManager notificationManager =
//                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(intentId++, notification);
        } else {
            //Utils.toast(message);
            Intent broadCastIntent = new Intent(BROADCAST_BEACON);
//            Utils.Log(TAG, "broadcastReceiver for news feed -> " + type);
            // String[] strings = pushmsg.split(" ");
//            if (getEnteredMessage(beacon, 0).equals("")) {
//                return;
//            }
            broadCastIntent.putExtra(BEACON_ENTRY_TEXT, entered_message);
            broadCastIntent.putExtra(BEACON_EXIT_TEXT, exit_message);
            broadCastIntent.putExtra(BEACON_NEAR_BY_TEXT, near_by_message);
            broadCastIntent.putExtra(PLACE_ID, placeid);
            broadCastIntent.putExtra("type", type);
            broadCastIntent.putExtra(BEACON_NAME, beacon);
            broadCastIntent.putExtra(BEACON_MESSAGE, messageBeacon);
            broadCastIntent.putExtra(PLACE_IMAGE, placeImage);
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadCastIntent);
        }
    }

    public void generateNotification(String title, String messageBody, PendingIntent pendingIntent) {

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

//        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
//        inboxStyle.addLine("line1");
//        inboxStyle.addLine("line2");

        int NOTIFICATION_ID = 1;

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.appimage)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
//                .setStyle(inboxStyle)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID /* ID of notification */, notificationBuilder.build());
    }

    protected static double calculateDistance(int txPower, double rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine distance, return -1.
        }

        double ratio = rssi * 1.0 / txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio, 10);
        } else {
            double accuracy = (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
            return accuracy;
        }
    }

}
