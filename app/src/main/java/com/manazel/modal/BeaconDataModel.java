package com.manazel.modal;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by c136 on 03/01/17.
 */

@RealmClass
public class BeaconDataModel extends RealmObject {

    @PrimaryKey
    private String beaconsId;
    private String uuid;
    private String beaconName;
    private String major;
    private String minior;
    private String text;
    private String details;
    private String pushNotificationText;
    private String isCloseApproach;
    private String range;
    private String entryText;
    private String exitText;
    private String entryContent;
    private String exitContent;
    private String is_delete;
    private String created;
    private String modified;
    private boolean isInRegion;


    public String getBeaconsId() {
        return beaconsId;
    }

    public void setBeaconsId(String beaconsId) {
        this.beaconsId = beaconsId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getBeaconName() {
        return beaconName;
    }

    public void setBeaconName(String beaconName) {
        this.beaconName = beaconName;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getMinior() {
        return minior;
    }

    public void setMinior(String minior) {
        this.minior = minior;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getPushNotificationText() {
        return pushNotificationText;
    }

    public void setPushNotificationText(String pushNotificationText) {
        this.pushNotificationText = pushNotificationText;
    }

    public String getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getIsCloseApproach() {
        return isCloseApproach;
    }

    public void setIsCloseApproach(String isCloseApproach) {
        this.isCloseApproach = isCloseApproach;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getEntryText() {
        return entryText;
    }

    public void setEntryText(String entryText) {
        this.entryText = entryText;
    }

    public String getExitText() {
        return exitText;
    }

    public void setExitText(String exitText) {
        this.exitText = exitText;
    }

    public String getEntryContent() {
        return entryContent;
    }

    public void setEntryContent(String entryContent) {
        this.entryContent = entryContent;
    }

    public String getExitContent() {
        return exitContent;
    }

    public void setExitContent(String exitContent) {
        this.exitContent = exitContent;
    }

    public boolean isInRegion() {
        return isInRegion;
    }

    public void setInRegion(boolean inRegion) {
        isInRegion = inRegion;
    }
}
