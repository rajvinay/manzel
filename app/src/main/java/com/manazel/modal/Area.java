package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.json.*;


public class Area {
	
    private String minArea;
    private String maxArea;
    
    
	public Area () {
		
	}



    @JsonProperty("Min_area")
    public String getMinArea() {
        return this.minArea;
    }

    public void setMinArea(String minArea) {
        this.minArea = minArea;
    }

    @JsonProperty("Max_area")
    public String getMaxArea() {
        return this.maxArea;
    }

    public void setMaxArea(String maxArea) {
        this.maxArea = maxArea;
    }


    
}
