package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.json.*;


public class Contract {
	
    private String status;
    private String propertyCategoryName;
    private String propertyCategoryId;
    
    
	public Contract () {
		
	}	
        

    @JsonProperty("status")
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("property_category_name")
    public String getPropertyCategoryName() {
        return this.propertyCategoryName;
    }

    public void setPropertyCategoryName(String propertyCategoryName) {
        this.propertyCategoryName = propertyCategoryName;
    }

    @JsonProperty("property_category_id")
    public String getPropertyCategoryId() {
        return this.propertyCategoryId;
    }

    public void setPropertyCategoryId(String propertyCategoryId) {
        this.propertyCategoryId = propertyCategoryId;
    }


    
}
