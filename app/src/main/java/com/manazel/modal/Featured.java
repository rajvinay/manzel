package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.json.*;


public class Featured {

    private String locality;
    private String bedroomNo;
    private String contactEmail;
    private String status;
    private String title;
    private String availability;
    private String categoryName;
    private String isFeatured;
    private String discountType;
    private String country;
    private String createdDate;
    private String discountValue;
    private String images;
    private String latitude;
    private String dealDateTo;
    private String propertyCategoryId;
    private String amenities;
    private String contactNo;
    private String longitude;
    private String propertyTypeId;
    private String propertyListId;
    private String referenceNumber;
    private String typeName;
    private String rentType;
    private String area;
    private String bathroomNo;
    private String contactName;
    private String price;
    private String dealDateFrom;
    private String isOffer;
    private String address;
    private String description;
    private String is_like;

    @JsonProperty("is_like")
    public String getIs_like() {
        return is_like;
    }

    public void setIs_like(String is_like) {
        this.is_like = is_like;
    }

    public Featured() {

    }


    @JsonProperty("locality")
    public String getLocality() {
        return this.locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    @JsonProperty("bedroom_no")
    public String getBedroomNo() {
        return this.bedroomNo;
    }

    public void setBedroomNo(String bedroomNo) {
        this.bedroomNo = bedroomNo;
    }

    @JsonProperty("contact_email")
    public String getContactEmail() {
        return this.contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    @JsonProperty("status")
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("title")
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("availability")
    public String getAvailability() {
        return this.availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    @JsonProperty("category_name")
    public String getCategoryName() {
        return this.categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @JsonProperty("is_featured")
    public String getIsFeatured() {
        return this.isFeatured;
    }

    public void setIsFeatured(String isFeatured) {
        this.isFeatured = isFeatured;
    }

    @JsonProperty("discount_type")
    public String getDiscountType() {
        return this.discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    @JsonProperty("country")
    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("created_date")
    public String getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @JsonProperty("discount_value")
    public String getDiscountValue() {
        return this.discountValue;
    }

    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }

    @JsonProperty("images")
    public String getImages() {
        return this.images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    @JsonProperty("latitude")
    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("deal_date_to")
    public String getDealDateTo() {
        return this.dealDateTo;
    }

    public void setDealDateTo(String dealDateTo) {
        this.dealDateTo = dealDateTo;
    }

    @JsonProperty("property_category_id")
    public String getPropertyCategoryId() {
        return this.propertyCategoryId;
    }

    public void setPropertyCategoryId(String propertyCategoryId) {
        this.propertyCategoryId = propertyCategoryId;
    }

    @JsonProperty("amenities")
    public String getAmenities() {
        return this.amenities;
    }

    public void setAmenities(String amenities) {
        this.amenities = amenities;
    }

    @JsonProperty("contact_no")
    public String getContactNo() {
        return this.contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    @JsonProperty("longitude")
    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("property_type_id")
    public String getPropertyTypeId() {
        return this.propertyTypeId;
    }

    public void setPropertyTypeId(String propertyTypeId) {
        this.propertyTypeId = propertyTypeId;
    }

    @JsonProperty("property_list_id")
    public String getPropertyListId() {
        return this.propertyListId;
    }

    public void setPropertyListId(String propertyListId) {
        this.propertyListId = propertyListId;
    }

    @JsonProperty("reference_number")
    public String getReferenceNumber() {
        return this.referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    @JsonProperty("type_name")
    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @JsonProperty("rent_type")
    public String getRentType() {
        return this.rentType;
    }

    public void setRentType(String rentType) {
        this.rentType = rentType;
    }

    @JsonProperty("area")
    public String getArea() {
        return this.area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @JsonProperty("bathroom_no")
    public String getBathroomNo() {
        return this.bathroomNo;
    }

    public void setBathroomNo(String bathroomNo) {
        this.bathroomNo = bathroomNo;
    }

    @JsonProperty("contact_name")
    public String getContactName() {
        return this.contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @JsonProperty("price")
    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @JsonProperty("deal_date_from")
    public String getDealDateFrom() {
        return this.dealDateFrom;
    }

    public void setDealDateFrom(String dealDateFrom) {
        this.dealDateFrom = dealDateFrom;
    }

    @JsonProperty("is_offer")
    public String getIsOffer() {
        return this.isOffer;
    }

    public void setIsOffer(String isOffer) {
        this.isOffer = isOffer;
    }

    @JsonProperty("address")
    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("description")
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}


