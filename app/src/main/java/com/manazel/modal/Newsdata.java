package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by c119 on 03/12/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Newsdata
{
    private String newsannouncementsId;
    private String title;
    private String slug;
    private String descriptions;
    private String newsannouncementsImages;
    private String userId;
    private String is_news;
    private String is_delete;
    private String created_date;
    private String modified_date;

    @JsonProperty("newsannouncementsId")
    public String getNewsannouncementsId() {
        return newsannouncementsId;
    }

    public void setNewsannouncementsId(String newsannouncementsId) {
        this.newsannouncementsId = newsannouncementsId;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("slug")
    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    @JsonProperty("descriptions")
    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    @JsonProperty("newsannouncementsImages")
    public String getNewsannouncementsImages() {
        return newsannouncementsImages;
    }

    public void setNewsannouncementsImages(String newsannouncementsImages) {
        this.newsannouncementsImages = newsannouncementsImages;
    }

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("is_news")
    public String getIs_news() {
        return is_news;
    }

    public void setIs_news(String is_news) {
        this.is_news = is_news;
    }

    @JsonProperty("is_delete")
    public String getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }

    @JsonProperty("created_date")
    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    @JsonProperty("modified_date")
    public String getModified_date() {
        return modified_date;
    }

    public void setModified_date(String modified_date) {
        this.modified_date = modified_date;
    }
}
