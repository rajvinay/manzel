package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by c119 on 01/12/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ticketsdata
{
    private String ticketId;
    private String series_no;
    private String title;
    private String deptId;
    private String departmentName;
    private String tickettypeId;
    private String tickettypeName;
    private String priorityId;
    private String ticketPriority;
    private String statusId;
    private String ticketStatus;
    private String userId;
    private String is_read;
    private String descriptions;
    private String is_delete;
    private String modified;
    private String assignTofname;
    private String assignTolname;
    private String ticketImages;


    @JsonProperty("ticketId")
    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    @JsonProperty("series_no")
    public String getSeries_no() {
        return series_no;
    }

    public void setSeries_no(String series_no) {
        this.series_no = series_no;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("deptId")
    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    @JsonProperty("departmentName")
    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    @JsonProperty("tickettypeId")
    public String getTickettypeId() {
        return tickettypeId;
    }

    public void setTickettypeId(String tickettypeId) {
        this.tickettypeId = tickettypeId;
    }

    @JsonProperty("tickettypeName")
    public String getTickettypeName() {
        return tickettypeName;
    }

    public void setTickettypeName(String tickettypeName) {
        this.tickettypeName = tickettypeName;
    }

    @JsonProperty("priorityId")
    public String getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(String priorityId) {
        this.priorityId = priorityId;
    }

    @JsonProperty("ticketPriority")
    public String getTicketPriority() {
        return ticketPriority;
    }

    public void setTicketPriority(String ticketPriority) {
        this.ticketPriority = ticketPriority;
    }

    @JsonProperty("statusId")
    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    @JsonProperty("ticketStatus")
    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("is_read")
    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    @JsonProperty("descriptions")
    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    @JsonProperty("is_delete")
    public String getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }

    @JsonProperty("modified")
    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    @JsonProperty("assignTofname")
    public String getAssignTofname() {
        return assignTofname;
    }

    public void setAssignTofname(String assignTofname) {
        this.assignTofname = assignTofname;
    }

    @JsonProperty("assignTolname")
    public String getAssignTolname() {
        return assignTolname;
    }

    public void setAssignTolname(String assignTolname) {
        this.assignTolname = assignTolname;
    }

    @JsonProperty("ticketImages")
    public String getTicketImages() {
        return ticketImages;
    }

    public void setTicketImages(String ticketImages) {
        this.ticketImages = ticketImages;
    }

}
