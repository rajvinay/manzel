package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 *  Created by c119 on 09/12/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Articledata
{
    private String categoryId;
    private String categoryName;
    private ArrayList<articlesarr> arrarticlechild;

    @JsonProperty("category_id")
    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @JsonProperty("categoryName")
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @JsonProperty("articlesarr")
    public ArrayList<articlesarr> getArrarticlechild() {
        return arrarticlechild;
    }

    public void setArrarticlechild(ArrayList<articlesarr> arrarticlechild) {
        this.arrarticlechild = arrarticlechild;
    }
}
