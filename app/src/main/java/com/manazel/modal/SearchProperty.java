package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.json.*;


public class SearchProperty {

    private String locality;
    private String bedroomNo;
    private String contactEmail;
    private String status;
    private String title;
    private String availability;
    private String categoryName;
    private String isFeatured;
    private String discountType;
    private String country;
    private String discountValue;
    private String images;
    private String latitude;
    private String isLike;
    private String propertyCategoryId;
    private String amenities;
    private String contactNo;
    private String longitude;
    private String propertyTypeId;
    private String referenceNumber;
    private String typeName;
    private String rentType;
    private String area;
    private String price;
    private String isOffer;
    private String address;
    private String description;
    private String property_list_id;
    private String reference_number;
    private String contact_no;
    private String contact_name;
    private String property_category_id;
    private String property_type_id;
    private String type_name;
    private String rent_type;
    private String bathroom_no;
    private String is_offer;
    private String deal_date_from;
    private String deal_date_to;
    private String discount_value;
    private String is_like;
    private String SortedBy;

    @JsonProperty("SortedBy")
    public String getSortedBy() {
        return SortedBy;
    }

    public void setSortedBy(String sortedBy) {
        SortedBy = sortedBy;
    }

    @JsonProperty("is_like")
    public String getIs_like() {
        return is_like;
    }

    public void setIs_like(String is_like) {
        this.is_like = is_like;
    }

    @JsonProperty("discount_value")
    public String getDiscount_value() {
        return discount_value;
    }

    public void setDiscount_value(String discount_value) {
        this.discount_value = discount_value;
    }

    @JsonProperty("deal_date_to")
    public String getDeal_date_to() {
        return deal_date_to;
    }

    public void setDeal_date_to(String deal_date_to) {
        this.deal_date_to = deal_date_to;
    }

    @JsonProperty("deal_date_from")
    public String getDeal_date_from() {
        return deal_date_from;
    }

    public void setDeal_date_from(String deal_date_from) {
        this.deal_date_from = deal_date_from;
    }

    @JsonProperty("is_offer")
    public String getIs_offer() {
        return is_offer;
    }

    public void setIs_offer(String is_offer) {
        this.is_offer = is_offer;
    }

    @JsonProperty("bathroom_no")
    public String getBathroom_no() {
        return bathroom_no;
    }

    public void setBathroom_no(String bathroom_no) {
        this.bathroom_no = bathroom_no;
    }

    @JsonProperty("rent_type")
    public String getRent_type() {
        return rent_type;
    }

    public void setRent_type(String rent_type) {
        this.rent_type = rent_type;
    }

    @JsonProperty("type_name")
    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    @JsonProperty("property_type_id")
    public String getProperty_type_id() {
        return property_type_id;
    }

    public void setProperty_type_id(String property_type_id) {
        this.property_type_id = property_type_id;
    }

    @JsonProperty("property_category_id")
    public String getProperty_category_id() {
        return property_category_id;
    }

    public void setProperty_category_id(String property_category_id) {
        this.property_category_id = property_category_id;
    }

    @JsonProperty("contact_name")
    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    @JsonProperty("contact_no")
    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    @JsonProperty("reference_number")
    public String getReference_number() {
        return reference_number;
    }

    public void setReference_number(String reference_number) {
        this.reference_number = reference_number;
    }

    @JsonProperty("property_list_id")
    public String getProperty_list_id() {
        return property_list_id;
    }

    public void setProperty_list_id(String property_list_id) {
        this.property_list_id = property_list_id;
    }

    public SearchProperty() {

    }

    @JsonProperty("locality")
    public String getLocality() {
        return this.locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    @JsonProperty("bedroom_no")
    public String getBedroomNo() {
        return this.bedroomNo;
    }

    public void setBedroomNo(String bedroomNo) {
        this.bedroomNo = bedroomNo;
    }

    @JsonProperty("contact_email")
    public String getContactEmail() {
        return this.contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    @JsonProperty("status")
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("title")
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("availability")
    public String getAvailability() {
        return this.availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    @JsonProperty("category_name")
    public String getCategoryName() {
        return this.categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @JsonProperty("is_featured")
    public String getIsFeatured() {
        return this.isFeatured;
    }

    public void setIsFeatured(String isFeatured) {
        this.isFeatured = isFeatured;
    }

    @JsonProperty("discount_type")
    public String getDiscountType() {
        return this.discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    @JsonProperty("country")
    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("images")
    public String getImages() {
        return this.images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    @JsonProperty("latitude")
    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("amenities")
    public String getAmenities() {
        return this.amenities;
    }

    public void setAmenities(String amenities) {
        this.amenities = amenities;
    }

    @JsonProperty("longitude")
    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("area")
    public String getArea() {
        return this.area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @JsonProperty("price")
    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @JsonProperty("address")
    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("description")
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
