package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.realm.RealmObject;

/**
 * Created by c119 on 07/12/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class tickettypes extends RealmObject
{
    private String tickettypeId;
    private String tickettypeName;
    private String is_delete;

    @JsonProperty("tickettypeId")
    public String getTickettypeId() {
        return tickettypeId;
    }

    public void setTickettypeId(String tickettypeId) {
        this.tickettypeId = tickettypeId;
    }

    @JsonProperty("tickettypeName")
    public String getTickettypeName() {
        return tickettypeName;
    }

    public void setTickettypeName(String tickettypeName) {
        this.tickettypeName = tickettypeName;
    }

    @JsonProperty("is_delete")
    public String getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }
}
