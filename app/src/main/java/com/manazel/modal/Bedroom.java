package com.manazel.modal;

/**
 * Created by c217 on 23/05/17.
 */

public class Bedroom {

    private String bedroom_Count;
    private String bedroom_Count_Name;

    public String getBedroom_Count() {
        return bedroom_Count;
    }

    public void setBedroom_Count(String bedroom_Count) {
        this.bedroom_Count = bedroom_Count;
    }

    public String getBedroom_Count_Name() {
        return bedroom_Count_Name;
    }

    public void setBedroom_Count_Name(String bedroom_Count_Name) {
        this.bedroom_Count_Name = bedroom_Count_Name;
    }
}
