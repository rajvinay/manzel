package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by c217 on 19/05/17.
 */

public class Property {

    private ArrayList<Offer> offer;
    private ArrayList<Featured> featured;
    private ArrayList<Recent> recent;
    private ArrayList<Wishlist> wishlists;
    private ArrayList<Contract> contracts;
    private ArrayList<Category> categories;
    private ArrayList<Location> locations;
    private ArrayList<Area> areas;
    private ArrayList<Price> prices;


    @JsonProperty("wishlist")
    public ArrayList<Wishlist> getWishlists() {
        return wishlists;
    }

    public void setWishlists(ArrayList<Wishlist> wishlists) {
        this.wishlists = wishlists;
    }

    @JsonProperty("contract")
    public ArrayList<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(ArrayList<Contract> contracts) {
        this.contracts = contracts;
    }

    @JsonProperty("category")
    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    @JsonProperty("location")
    public ArrayList<Location> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<Location> locations) {
        this.locations = locations;
    }

    @JsonProperty("area")
    public ArrayList<Area> getAreas() {
        return areas;
    }

    public void setAreas(ArrayList<Area> areas) {
        this.areas = areas;
    }

    @JsonProperty("price")
    public ArrayList<Price> getPrices() {
        return prices;
    }

    public void setPrices(ArrayList<Price> prices) {
        this.prices = prices;
    }

    public Property() {

    }

    @JsonProperty("offer")
    public ArrayList<Offer> getOffer() {
        return this.offer;
    }

    public void setOffer(ArrayList<Offer> offer) {
        this.offer = offer;
    }

    @JsonProperty("featured")
    public ArrayList<Featured> getFeatured() {
        return this.featured;
    }

    public void setFeatured(ArrayList<Featured> featured) {
        this.featured = featured;
    }

    @JsonProperty("recent")
    public ArrayList<Recent> getRecent() {
        return this.recent;
    }

    public void setRecent(ArrayList<Recent> recent) {
        this.recent = recent;
    }


}
