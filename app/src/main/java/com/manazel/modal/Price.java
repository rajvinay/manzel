package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.json.*;


public class Price {
	
    private String minPrice;
    private String maxPrice;
    
    
	public Price () {
		
	}


    @JsonProperty("Min_price")
    public String getMinPrice() {
        return this.minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    @JsonProperty("Max_price")
    public String getMaxPrice() {
        return this.maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }


    
}
