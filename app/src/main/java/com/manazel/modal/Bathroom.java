package com.manazel.modal;

/**
 * Created by c217 on 23/05/17.
 */

public class Bathroom {

    private String bathroom_Count;
    private String bathroom_Count_Name;

    public String getBathroom_Count() {
        return bathroom_Count;
    }

    public void setBathroom_Count(String bathroom_Count) {
        this.bathroom_Count = bathroom_Count;
    }

    public String getBathroom_Count_Name() {
        return bathroom_Count_Name;
    }

    public void setBathroom_Count_Name(String bathroom_Count_Name) {
        this.bathroom_Count_Name = bathroom_Count_Name;
    }
}
