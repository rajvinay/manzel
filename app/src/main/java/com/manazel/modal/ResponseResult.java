package com.manazel.modal;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseResult implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private ResponseObject Result;

    private String status;

    private String message;

    private String UserToken;

    private Property property;

    private ArrayList<SearchProperty> searchProperties;

    private ArrayList<FaqData> arrfaqdata;

    private ArrayList<Ticketsdata> arrticket;

    private ArrayList<Newsdata> arrnews;

    private ArrayList<Announcementsdata> arrannouncements;

    private ArrayList<TicketDeptData> arrticketdept;

    private ArrayList<Registerdata> arrregisterdata;

    private ArrayList<Logindata> arrlogindata;

    private ArrayList<Articledata> arrarticledata;

    private ArrayList<displaymessagedata> arrticketcomments;

    private ArrayList<Logindata> user_info;

    private ArrayList<String> section;

    @JsonProperty("section")
    public ArrayList<String> getSection() {
        return section;
    }

    public void setSection(ArrayList<String> section) {
        this.section = section;
    }

    @JsonProperty("Search_Property")
    public ArrayList<SearchProperty> getSearchProperties() {
        return searchProperties;
    }

    public void setSearchProperties(ArrayList<SearchProperty> searchProperties) {
        this.searchProperties = searchProperties;
    }

    @JsonProperty("Property")
    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    @JsonProperty("Articledata")
    public ArrayList<Articledata> getArrarticledata() {
        return arrarticledata;
    }

    public void setArrarticledata(ArrayList<Articledata> arrarticledata) {
        this.arrarticledata = arrarticledata;
    }

    @JsonProperty("UserToken")
    public String getUserToken() {
        return UserToken;
    }

    public void setUserToken(String userToken) {
        UserToken = userToken;
    }

    @JsonProperty("Logindata")
    public ArrayList<Logindata> getArrlogindata() {
        return arrlogindata;
    }

    public void setArrlogindata(ArrayList<Logindata> arrlogindata) {
        this.arrlogindata = arrlogindata;
    }

    @JsonProperty("Registerdata")
    public ArrayList<Registerdata> getArrregisterdata() {
        return arrregisterdata;
    }

    public void setArrregisterdata(ArrayList<Registerdata> arrregisterdata) {
        this.arrregisterdata = arrregisterdata;
    }

    @JsonProperty("Result")
    public ResponseObject getResult() {
        return Result;
    }

    public void setResult(ResponseObject result) {
        Result = result;
    }

    @JsonProperty("ticketdeptdata")
    public ArrayList<TicketDeptData> getArrticketdept() {
        return arrticketdept;
    }

    public void setArrticketdept(ArrayList<TicketDeptData> arrticketdept) {
        this.arrticketdept = arrticketdept;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("FAQdata")
    public ArrayList<FaqData> getArrfaqdata() {
        return arrfaqdata;
    }

    public void setArrfaqdata(ArrayList<FaqData> arrfaqdata) {
        this.arrfaqdata = arrfaqdata;
    }

    @JsonProperty("Ticketsdata")
    public ArrayList<Ticketsdata> getArrticket() {
        return arrticket;
    }

    public void setArrticket(ArrayList<Ticketsdata> arrticket) {
        this.arrticket = arrticket;
    }

    @JsonProperty("Newsdata")
    public ArrayList<Newsdata> getArrnews() {
        return arrnews;
    }

    public void setArrnews(ArrayList<Newsdata> arrnews) {
        this.arrnews = arrnews;
    }

    @JsonProperty("Announcementsdata")
    public ArrayList<Announcementsdata> getArrannouncements() {
        return arrannouncements;
    }

    public void setArrannouncements(ArrayList<Announcementsdata> arrannouncements) {
        this.arrannouncements = arrannouncements;
    }

    @JsonProperty("displaymessagedata")
    public ArrayList<displaymessagedata> getArrticketcomments() {
        return arrticketcomments;
    }

    public void setArrticketcomments(ArrayList<displaymessagedata> arrticketcomments) {
        this.arrticketcomments = arrticketcomments;
    }

    @JsonProperty("data")
    public ArrayList<Logindata> getUser_info() {
        return user_info;
    }

    public void setUser_info(ArrayList<Logindata> user_info) {
        this.user_info = user_info;
    }


}
