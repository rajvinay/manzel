package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by c119 on 09/12/16.
 */


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class ArticledataPush {

    private String category_id;
    private String categoryName;
    private articlesarr articlesarr;

    @JsonProperty("category_id")
    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    @JsonProperty("categoryName")
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @JsonProperty("articlesarr")
    public com.manazel.modal.articlesarr getArticlesarr() {
        return articlesarr;
    }

    public void setArticlesarr(com.manazel.modal.articlesarr articlesarr) {
        this.articlesarr = articlesarr;
    }
}
