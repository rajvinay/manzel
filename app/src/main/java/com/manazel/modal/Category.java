package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.json.*;


public class Category {

    private String propertyTypeId;
    private String propertyTypeName;
    private String status;


    public Category() {

    }

    @JsonProperty("property_type_id")
    public String getPropertyTypeId() {
        return this.propertyTypeId;
    }

    public void setPropertyTypeId(String propertyTypeId) {
        this.propertyTypeId = propertyTypeId;
    }

    @JsonProperty("property_type_name")
    public String getPropertyTypeName() {
        return this.propertyTypeName;
    }

    public void setPropertyTypeName(String propertyTypeName) {
        this.propertyTypeName = propertyTypeName;
    }

    @JsonProperty("status")
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
