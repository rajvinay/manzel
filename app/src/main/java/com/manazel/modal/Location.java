package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.json.*;


public class Location {

    private String count;
    private String city;


    public Location() {

    }


    @JsonProperty("count")
    public String getCount() {
        return this.count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @JsonProperty("city")
    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }


}
