package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by c119 on 07/12/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TicketDeptData extends RealmObject
{
    private RealmList<departments> arrdepartment;

    private RealmList<tickettypes> arrtickettypes;

    private RealmList<ticketpriorities> arrticketpriorities;

    @JsonProperty("tickettypes")
    public RealmList<tickettypes> getArrtickettypes() {
        return arrtickettypes;
    }

    public void setArrtickettypes(RealmList<tickettypes> arrtickettypes) {
        this.arrtickettypes = arrtickettypes;
    }

    @JsonProperty("ticketpriorities")
    public RealmList<ticketpriorities> getArrticketpriorities() {
        return arrticketpriorities;
    }

    public void setArrticketpriorities(RealmList<ticketpriorities> arrticketpriorities) {
        this.arrticketpriorities = arrticketpriorities;
    }

    @JsonProperty("departments")
    public RealmList<departments> getArrdepartment() {
        return arrdepartment;
    }

    public void setArrdepartment(RealmList<departments> arrdepartment) {
        this.arrdepartment = arrdepartment;
    }
}
