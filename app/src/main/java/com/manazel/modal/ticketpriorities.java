package com.manazel.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.realm.RealmObject;

/**
 * Created by c119 on 07/12/16.
 */

public class ticketpriorities extends RealmObject
{
    private String ticketpriorityId;
    private String ticketPriority;
    private String is_delete;

    @JsonProperty("ticketpriorityId")
    public String getTicketpriorityId() {
        return ticketpriorityId;
    }

    public void setTicketpriorityId(String ticketpriorityId) {
        this.ticketpriorityId = ticketpriorityId;
    }

    @JsonProperty("ticketPriority")
    public String getTicketPriority() {
        return ticketPriority;
    }

    public void setTicketPriority(String ticketPriority) {
        this.ticketPriority = ticketPriority;
    }

    @JsonProperty("is_delete")
    public String getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }
}
