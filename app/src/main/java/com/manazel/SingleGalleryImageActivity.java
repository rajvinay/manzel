package com.manazel;

import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.manazel.Constant.AppGlobal;
import com.manazel.Fragment.SingleImageGallery;

import java.util.ArrayList;
import java.util.List;

public class SingleGalleryImageActivity extends FragmentActivity implements View.OnClickListener {

    private ViewPager viewPager;
    private Bundle b;
    private int selected_pos;
    private ArrayList<String> imgList = new ArrayList<>();
    private ImageView iv_Back;
    private ViewPagerAdapter adapter;
    private SingleImageGallery singleImageGallery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_gallery_image);

        setupControls();
        initGlobal();
    }

    private void setupControls() {
        b = getIntent().getExtras();
        selected_pos = b.getInt("selected_pos");
        imgList = b.getStringArrayList("full_array");
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        iv_Back = (ImageView) findViewById(R.id.iv_Back);

    }

    private void initGlobal() {
        setupViewPager(viewPager);
        iv_Back.setOnClickListener(this);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (int i = 0; i < imgList.size(); i++) {

            singleImageGallery = new SingleImageGallery(imgList.get(i), i);
            adapter.addFragment(singleImageGallery);
        }
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(selected_pos);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_Back:
                onBackPressed();
                SingleGalleryImageActivity.this.finish();
                break;
        }
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
//        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
//            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
//            return mFragmentTitleList.get(position);
            return "";
        }
    }


}
