package com.manazel.service;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.manazel.AnnouncementDetail;
import com.manazel.ArticleDetail;
import com.manazel.Constant.AppConstant;
import com.manazel.Constant.TinyDB;
import com.manazel.MainActivity;
import com.manazel.NewsDetail;
import com.manazel.app.Config;
import com.manazel.modal.Announcementsdata;
import com.manazel.modal.Articledata;
import com.manazel.modal.ArticledataPush;
import com.manazel.modal.Newsdata;
import com.manazel.utils.NotificationUtils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by c119 on 16/11/16.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private NotificationUtils notificationUtils;
    SimpleDateFormat sdf;
    TinyDB tinydb;
    private String notification_title = "", notification_message = "", notification_image = null, timestemp = null, timestamp = "0";
    boolean isBackground;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // Log.e(TAG, "From: " + remoteMessage.getFrom());
        Map data = remoteMessage.getData();
        RemoteMessage.Notification notification = remoteMessage.getNotification();
        tinydb = new TinyDB(getApplicationContext());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {

            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            //Log.e(TAG, "Data Payload: " + remoteMessage.getData().get("notification_for"));
            String push_type = remoteMessage.getData().get("notification_for");
            Gson gson = new Gson();

            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = null;
            String tempDate = "";
            Intent resultIntent = null;
            String type = "";

            if (push_type.equalsIgnoreCase(AppConstant.PUSH_TYPE_NEWS)) {

                try {

                    JSONObject jobj = new JSONObject(remoteMessage.getData().get("data"));
                    if (jobj.has("is_news")) {

                        if (jobj.optString("is_news").equalsIgnoreCase("1")) {

                            //Type news
                            type = Newsdata.class.getName();
                            tinydb.putString("newsdetailobj", remoteMessage.getData().get("data"));
                            Newsdata obj = gson.fromJson(remoteMessage.getData().get("data"), Newsdata.class);
                            notification_title = obj.getTitle();
                            notification_message = obj.getDescriptions();
                            tempDate = obj.getModified_date();
                            resultIntent = new Intent(getApplicationContext(), NewsDetail.class);

                        } else {

                            //Type announcement
                            type = Announcementsdata.class.getName();
                            tinydb.putString("announcedetailobj", remoteMessage.getData().get("data"));
                            Announcementsdata obj = gson.fromJson(remoteMessage.getData().get("data"), Announcementsdata.class);
                            notification_title = obj.getTitle();
                            notification_message = obj.getDescriptions();
                            tempDate = obj.getModified_date();
                            resultIntent = new Intent(getApplicationContext(), AnnouncementDetail.class);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (push_type.equalsIgnoreCase(AppConstant.PUSH_TYPE_ARTICLE)) {

                //Article type push

                type = Announcementsdata.class.getName();
                ArticledataPush obj = gson.fromJson(remoteMessage.getData().get("data"), ArticledataPush.class);

                String json = gson.toJson(obj.getArticlesarr());
                tinydb.putString("articledetailobj", json);

                resultIntent = new Intent(getApplicationContext(), ArticleDetail.class);

                notification_title = obj.getArticlesarr().getTitle();
                notification_message = obj.getArticlesarr().getDescriptions();
                tempDate = obj.getArticlesarr().getModified_date();

            }

            isBackground = false;
            notification_image = null;

            try {

                date = sdf.parse(tempDate);
                timestamp = String.valueOf(date.getTime());

            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (resultIntent != null) {

                resultIntent.putExtra("message", notification_message);
                handleDataMessage2(resultIntent, type);
            }

        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);


            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();

//            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
//            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            resultIntent.putExtra("message", message);
//
//            notificationUtils.showNotificationMessage(message, "", "", resultIntent);
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {

            JSONObject data = json.getJSONObject("data");

            String title = data.getString("title");
            String message = data.getString("message");
            boolean isBackground = data.getBoolean("is_background");
            String imageUrl = data.getString("image");
            String timestamp = data.getString("timestamp");
            JSONObject payload = data.getJSONObject("payload");

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "isBackground: " + isBackground);
            Log.e(TAG, "payload: " + payload.toString());
            Log.e(TAG, "imageUrl: " + imageUrl);
            Log.e(TAG, "timestamp: " + timestamp);


            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();
            } else {
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                resultIntent.putExtra("message", message);

                //check for image attachment
                if (TextUtils.isEmpty(imageUrl)) {

                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);

                } else {

                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                }
            }

        } catch (JSONException e) {

            Log.e(TAG, "Json Exception: " + e.getMessage());

        } catch (Exception e) {

            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    private void handleDataMessage2(Intent intent, String type) {


        try {


//            Log.e(TAG, "title: " + notification_title);
//            Log.e(TAG, "message: " + notification_message);
//            Log.e(TAG, "isBackground: " + isBackground);
//            Log.e(TAG, "payload: " + payload.toString());
//            Log.e(TAG, "imageUrl: " + notification_image);
//            Log.e(TAG, "timestamp: " + timestamp);


            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

                //app is in foreground, broadcast the push message
//                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
//                pushNotification.putExtra("message", notification_message);
//                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                EventBus.getDefault().post(type);


                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();

            } else {

                // app is in background, show the notification in notification tray
                // check for image attachment
                if (TextUtils.isEmpty(notification_image)) {

                    showNotificationMessage(getApplicationContext(), notification_title, notification_message, timestamp, intent);

                } else {

                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), notification_title, notification_message, timestamp, intent, notification_image);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }


    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */

    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}