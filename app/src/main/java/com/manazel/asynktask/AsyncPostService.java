package com.manazel.asynktask;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import com.google.gson.Gson;
import com.manazel.Constant.AppGlobal;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.Common;
import com.manazel.modal.ResponseResult;
import com.manazel.ws.RequestWs;

import org.json.JSONObject;


@SuppressWarnings("deprecation")
public class AsyncPostService extends AsyncTask<String, Void, Object> {

	Context mContext;
	String serviceType;
	String loadingmsg;

	WsResponseListener wsResponseListener;

	boolean isloaderEnable;
	boolean passObject;
	Common commonObj;

	Exception error = null;

	public AsyncPostService(Context mContext, String loadingmsg, String serviceType, boolean isloaderEnable, boolean passObject)
	{

		this.mContext = mContext;
		this.loadingmsg = loadingmsg;
		this.serviceType = serviceType;
		this.isloaderEnable = isloaderEnable;
		this.passObject = passObject;
		this.passObject = passObject;

		wsResponseListener = (WsResponseListener) mContext;

	}

	public AsyncPostService(Context mContext, String loadingmsg, String serviceType, Common commonObj, boolean isloaderEnable, boolean passObject)
	{

		this.mContext = mContext;
		this.loadingmsg = loadingmsg;
		this.serviceType = serviceType;
		this.commonObj = commonObj;
		this.isloaderEnable = isloaderEnable;
		this.passObject = passObject;

		wsResponseListener = (WsResponseListener) mContext;

	}

	public AsyncPostService(Context mContext, String loadingmsg, Fragment fragment, String serviceType, Common commonObj, boolean isloaderEnable, boolean passObject)
	{

		this.mContext = mContext;
		this.loadingmsg = loadingmsg;
		this.serviceType = serviceType;
		this.commonObj = commonObj;
		this.isloaderEnable = isloaderEnable;
		this.passObject = passObject;

		wsResponseListener = (WsResponseListener) fragment;

	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		if (isloaderEnable)
			AppGlobal.showProgressDialog(mContext, loadingmsg);
	}

	@Override
	protected Object doInBackground(String... params)
	{
 		try
		{
			if(passObject)
			{
				Gson gson = new Gson();
				String json = gson.toJson(commonObj);
				JSONObject obj = new JSONObject(json);
				return new RequestWs().getPostRequest(params[0],obj,ResponseResult.class);

			}
			else
			{
				return new RequestWs().getPostRequest(params[0],ResponseResult.class);
			}

		}
		catch (Exception e)
		{
			error = e;
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);
		if (isloaderEnable)
			AppGlobal.hideProgressDialog(mContext);
		wsResponseListener.onDelieverResponse(serviceType, result, error);
	}

}
