package com.manazel.asynktask;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.manazel.Constant.AppGlobal;
import com.manazel.R;
import com.manazel.interfaces.WsResponseListener;
import com.manazel.modal.ResponseResult;
import com.manazel.ws.RequestWs;

public class AsyncGetService extends AsyncTask<String, Void, Object> {

	private final String LOG_TAG = "AsyncGetService";

	Exception error = null;

	Context mContext;

	WsResponseListener wsResponseListener;

	String serviceType;

	boolean isLoader;


	public AsyncGetService(Context mContext, String serviceType, boolean isLoader)
	{

		this.mContext = mContext;
		this.serviceType = serviceType;
		this.isLoader = isLoader;

		wsResponseListener = (WsResponseListener) mContext;
	}

	public AsyncGetService(Context mContext, Fragment fragment, String serviceType, boolean isLoader)
	{
		this.mContext = mContext;
		this.serviceType = serviceType;
		this.isLoader = isLoader;

		wsResponseListener = (WsResponseListener) fragment;
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();

		if (isLoader)
			AppGlobal.showProgressDialog(mContext,mContext.getResources().getString(R.string.Please_wait));
	}

	@Override
	protected Object doInBackground(String... params)
	{
		try
		{
			return new RequestWs().getGetRequest(params[0],serviceType,ResponseResult.class);
		}
		catch (Exception e)
		{
			this.error = e;
			Log.e(LOG_TAG, e.getMessage());
		}

		return null;

	}

	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);

		if (isLoader)
			AppGlobal.hideProgressDialog(mContext);
		wsResponseListener.onDelieverResponse(serviceType, result, error);
	}

}
